package com.cs.dajen.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 25-05-2017.
 */

public class MyProfile extends Activity implements View.OnClickListener{

    EditText name,mobile,email;
    RelativeLayout manageAddress, changePassword, messages;
    LinearLayout logout;
    TextView editProfile;
    ImageView back_btn;
    SharedPreferences userPrefs;
    SharedPreferences.Editor  userPrefEditor;
    String response;
    String mLoginStatus;
    TextView logoutText;
    private DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.profile_activity);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.profile_activity_ar);
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 4;
        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar4);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar1);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        mLoginStatus = userPrefs.getString("login_status", "");
//        userId = userPrefs.getString("userId", null);

        manageAddress = (RelativeLayout) findViewById(R.id.manage_address);
        changePassword = (RelativeLayout) findViewById(R.id.change_password);
        messages = (RelativeLayout) findViewById(R.id.messages);
        logout = (LinearLayout) findViewById(R.id.logout_button);
        editProfile = (TextView) findViewById(R.id.edit_profile);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        logoutText = (TextView) findViewById(R.id.logout_text);

        name = (EditText) findViewById(R.id.register_name);
        mobile = (EditText) findViewById(R.id.mobile_number);
        email = (EditText) findViewById(R.id.email);

        editProfile.setOnClickListener(this);
        changePassword.setOnClickListener(this);
        messages.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        logout.setOnClickListener(this);
        manageAddress.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.back_btn:
                startActivity(new Intent(MyProfile.this,MoreActivity.class));
                finish();
                break;

            case R.id.change_password:
                startActivity(new Intent(MyProfile.this,ChangePassword.class));
                break;

            case R.id.edit_profile:
                startActivity(new Intent(MyProfile.this,EditProfile.class));
                break;

            case R.id.messages:
                startActivity(new Intent(MyProfile.this,MessagesActivity.class));
                break;

            case R.id.logout_button:
                userPrefEditor.clear();
                userPrefEditor.commit();
//                logoutText.setText("Login");
                onBackPressed();
                break;

            case R.id.manage_address:
                Intent i = new Intent(MyProfile.this, AddressActivity.class);
                startActivity(i);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
        Constants.CurrentMoreActivity = "profile";
        response = userPrefs.getString("user_profile", null);
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                name.setText(userObjuect.getString("fullName"));
                mobile.setText("+"+userObjuect.getString("mobile"));
                email.setText(userObjuect.getString("email"));
            } catch (JSONException e) {
                Log.d("TAG", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(MyProfile.this,MoreActivity.class));
        finish();
    }

}
