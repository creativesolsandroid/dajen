package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Adapters.CheckoutAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.Order;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.cs.dajen.SplashScreenActivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.cs.dajen.R.id.footer_amount;

/**
 * Created by CS on 04-05-2017.
 */

public class CheckoutActivity extends Activity implements View.OnClickListener{

    TextView footer_addmore, footer_ordernow;
    public static TextView footer_qty, footer_amt, netTotal, vatAmount, amount;

    RelativeLayout recieptLayout;
    LinearLayout  vatLayout;
    TextView vatPercent,receipt_close;
    ImageView minvoice;
    float vat=5;;
    ListView checkoutList;
    private DataBaseHelper myDbHelper;
    ArrayList<Order> orderList = new ArrayList<>();
    CheckoutAdapter mAdapter;
    ImageView cart;
    AlertDialog customDialog;
    ImageView back_btn;
    String screen;
    SharedPreferences userPrefs;
    AlertDialog alertDialog;
    String favNameTxt;
    public static int CHECKOUT_REQUEST = 1;
    String userId;

    SharedPreferences languagePrefs;
    String language;

    LinearLayout amount_layout;
    ScrollView sv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.checkout);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.checkout_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "-1");

        Constants.CurrentOrderActivity = "checkout";
        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 2;

        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        try {
            screen = getIntent().getStringExtra("class");
        } catch (Exception e) {
            e.printStackTrace();
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());
        orderList = myDbHelper.getOrderInfo();

        footer_addmore = (TextView) findViewById(R.id.footer_addmore);
        footer_qty = (TextView) findViewById(R.id.footer_qty);
        footer_amt = (TextView) findViewById(footer_amount);
        footer_ordernow = (TextView) findViewById(R.id.footer_checkout);
        cart = (ImageView) findViewById(R.id.cart);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        recieptLayout = (RelativeLayout) findViewById(R.id.receipt_layout);
        receipt_close = (TextView) findViewById(R.id.receipt_close);
        amount = (TextView) findViewById(R.id.amount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        vatPercent = (TextView) findViewById(R.id.vatPercent);
        netTotal = (TextView) findViewById(R.id.net_total);
        minvoice= (ImageView) findViewById(R.id.invoice);

//        amount_layout = (LinearLayout) findViewById(R.id.amount_layout);
//        sv = (ScrollView) findViewById(R.id.sv);

        checkoutList = (ListView) findViewById(R.id.checkout_list);

        mAdapter = new CheckoutAdapter(CheckoutActivity.this, orderList, language);
        checkoutList.setAdapter(mAdapter);

//        amount_layout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            @Override
//            public boolean onPreDraw() {
//                sv.getViewTreeObserver().removeOnPreDrawListener(this);
//                sv.scrollTo(0,2000);
//                return false;
//            }
//        });

        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieptLayout.setVisibility(View.GONE);
            }
        });


        minvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recieptLayout.setVisibility(View.VISIBLE);
            }
        });

        cart.setOnClickListener(this);
        footer_addmore.setOnClickListener(this);
        footer_ordernow.setOnClickListener(this);
        back_btn.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        amount.setText(""+Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
        vatAmount.setText(""+Constants.decimalFormat.format(tax));
        netTotal.setText(""+Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()+tax));

        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
        footer_qty.setText(""+myDbHelper.getTotalOrderQty());
        footer_amt.setText(""+Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()+tax));
    }

    @Override
    public void onBackPressed() {
        if(screen!=null && screen.equals("confirm")){
            if (userPrefs.getString("menu", "main").equals("main")) {
                Intent intent = new Intent(CheckoutActivity.this, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
            else if (userPrefs.getString("menu", "main").equals("bbq")) {
                Intent intent = new Intent(CheckoutActivity.this, BBQatHome.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }
        else if(screen!=null && screen.equals("product")){
            finish();
        }
        else {
            if (Constants.MENU_TYPE.equals("main")) {
                Intent intent = new Intent(CheckoutActivity.this, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
            else if (Constants.MENU_TYPE.equals("bbq")) {
                Intent intent = new Intent(CheckoutActivity.this, BBQatHome.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back_btn:
                if(screen!=null && screen.equals("confirm")){
                    if (userPrefs.getString("menu", "main").equals("main")) {
                        Intent intent = new Intent(CheckoutActivity.this, MenuActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                    else if (userPrefs.getString("menu", "main").equals("bbq")) {
                        Intent intent = new Intent(CheckoutActivity.this, BBQatHome.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                }
                else if(screen!=null && screen.equals("product")){
                    finish();
                }
                else {
                    if (userPrefs.getString("menu", "main").equals("main")) {
                        Intent intent = new Intent(CheckoutActivity.this, MenuActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                    else if (userPrefs.getString("menu", "main").equals("bbq")) {
                        Intent intent = new Intent(CheckoutActivity.this, BBQatHome.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                }
                break;

            case R.id.footer_checkout:
                startActivity(new Intent(CheckoutActivity.this,OrderTypeActivity.class));
                break;

            case R.id.cart:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CheckoutActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = null;
                if(language.equalsIgnoreCase("En")){
                    dialogView = inflater.inflate(R.layout.dialog_save_order, null);
                }else if(language.equalsIgnoreCase("Ar")){
                    dialogView = inflater.inflate(R.layout.dialog_save_order_ar, null);
                }
//                View dialogView = inflater.inflate(R.layout.cancel_order_dialog, null);
                dialogBuilder.setView(dialogView);

                final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
                TextView cancelBtn = (TextView) dialogView.findViewById(R.id.cancel);
                final TextView clearOrder = (TextView) dialogView.findViewById(R.id.order_clear);
                final TextView saveOrder = (TextView) dialogView.findViewById(R.id.save_order);
                TextView cancelAlertText = (TextView) dialogView.findViewById(R.id.cancel_alert_text);
                if(language.equalsIgnoreCase("En")){
                    cancelAlertText.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                }else if(language.equalsIgnoreCase("Ar")){
                    cancelAlertText.setText(" منتج في الحقيبة ، من خلال هذا الإجراء ستصبح حقيبتك خالية من المنتجا ت" +myDbHelper.getTotalOrderQty()+"  لديك  ");
                }

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                clearOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDbHelper.deleteOrderTable();
                        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());

                        Intent intent = new Intent(CheckoutActivity.this, MenuType.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

                saveOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        favNameTxt = favName.getText().toString();
                        if(favNameTxt.length()>0){
                            alertDialog.dismiss();
                            if(userId.equals("-1")) {
                                Intent intent = new Intent(CheckoutActivity.this, LoginActivity.class);
                                startActivityForResult(intent, CHECKOUT_REQUEST);
                            }
                            else {
                                new InsertOrder().execute();
                            }
                        }
                    }
                });

                favName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        favNameTxt = favName.getText().toString();
                        if(favNameTxt.length()>0){
                            saveOrder.setTextColor(Color.parseColor("#2A8AEE"));
                        }
                        else{
                            saveOrder.setTextColor(Color.parseColor("#555555"));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

//                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CheckoutActivity.this);
//                // ...Irrelevant code for customizing the buttons and title
//                LayoutInflater inflater = getLayoutInflater();
//                int layout = R.layout.alert_dialog;
//                View dialogView = inflater.inflate(layout, null);
//                dialogBuilder.setView(dialogView);
//                dialogBuilder.setCancelable(false);
//
//                TextView title = (TextView) dialogView.findViewById(R.id.title);
//                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//                View vert = (View) dialogView.findViewById(R.id.vert_line);
//
//                if(language.equalsIgnoreCase("En")) {
//                    title.setText(getResources().getString(R.string.dajen));
//                    no.setText(getResources().getString(R.string.no));
//                    yes.setText(getResources().getString(R.string.cart_clear));
//                    desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
//                }
//                else{
//                    title.setText(getResources().getString(R.string.dajen_ar));
//                    no.setText(getResources().getString(R.string.no_ar));
//                    yes.setText(getResources().getString(R.string.cart_clear_ar));
//                    desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
//                }
//
//                yes.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        customDialog.dismiss();
//                        myDbHelper.deleteOrderTable();
//                        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
//
//                        Intent intent = new Intent(CheckoutActivity.this, MenuType.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
//                        finish();
//                    }
//                });
//
//                no.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        customDialog.dismiss();
//                    }
//                });
//
//                customDialog = dialogBuilder.create();
//                customDialog.show();
//                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                Window window = customDialog.getWindow();
//                lp.copyFrom(window.getAttributes());
//                //This makes the dialog take up the full width
//                Display display = getWindowManager().getDefaultDisplay();
//                Point size = new Point();
//                display.getSize(size);
//                int screenWidth = size.x;
//
//                double d = screenWidth*0.85;
//                lp.width = (int) d;
//                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                window.setAttributes(lp);
                break;

            case R.id.footer_addmore:
                if (userPrefs.getString("menu", "main").equals("main")) {
                    Intent intent = new Intent(CheckoutActivity.this, MenuActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                else if (userPrefs.getString("menu", "main").equals("bbq")) {
                    Intent intent = new Intent(CheckoutActivity.this, BBQatHome.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }

                break;
        }
    }

    public class InsertOrder extends AsyncTask<String, String, String> {
        URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        String currentTime, expectedtime;
        String kitchenStart;
        String estimatedTime, total_amt;
        JSONObject parent = new JSONObject();
        InputStream inputStream = null;
        String result = "";
        MaterialDialog dialog;
        String mamount, mvatamount;

        String itemId = "", qty = "", comments = "", total_price = null, item_price = "", sizes = "", additionals = "", additionalsPices = "";
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat df6 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(CheckoutActivity.this);
            dialog = new MaterialDialog.Builder(CheckoutActivity.this)
                    .title(R.string.app_name)
                    .content("Please wait...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();

            Calendar c = Calendar.getInstance();
            SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
            dateFormat1.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
            df3.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));

            currentTime = df3.format(c.getTime()).replace("a.m", "AM")
                    .replace("p.m", "PM").replace("am", "AM")
                    .replace("pm", "PM").replace("AM.", "AM")
                    .replace("PM.", "PM");
            estimatedTime = dateFormat1.format(c.getTime()).replace("a.m", "AM")
                    .replace("p.m", "PM").replace("am", "AM")
                    .replace("pm", "PM").replace("AM.", "AM")
                    .replace("PM.", "PM");

            total_amt = netTotal.getText().toString().replace(" SR", "");
            mamount = Constants.decimalFormat.format(Float.parseFloat(String.valueOf(myDbHelper.getTotalOrderPrice()))).replace("0,00", "0.00");

            float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
            mvatamount = Constants.decimalFormat.format(tax);

            String MenuType = userPrefs.getString("menu", "main");
            if (MenuType.equalsIgnoreCase("main")) {
                MenuType = "1";
            } else if (MenuType.equalsIgnoreCase("bbq")) {
                MenuType = "2";
            }

            try {
                JSONArray mainItem = new JSONArray();
                JSONArray subItem = new JSONArray();
                JSONArray subItem1 = new JSONArray();
                JSONArray promoItem = new JSONArray();

                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                String version = pInfo.versionName;

                JSONObject mainObj = new JSONObject();
                mainObj.put("StoreId", "11");
                mainObj.put("IsFavorite", true);
                mainObj.put("FavoriteName", favNameTxt);
                mainObj.put("UserId", userId);
                mainObj.put("MenuType", MenuType);
                mainObj.put("OrderDate", currentTime);
                mainObj.put("PaymentMode", "0");
                mainObj.put("ExpectedTime", estimatedTime);
                mainObj.put("Device_token", SplashScreenActivity.regId);
                mainObj.put("OrderStatus", "Close");
                mainObj.put("Total_Price", total_amt);
                mainObj.put("Comments", "Android v" + version);
                mainObj.put("OrderType", Constants.ORDER_TYPE);
                mainObj.put("VatPercentage", "5");
                mainObj.put("SubTotal", Constants.convertToArabic(mamount));
                mainObj.put("VatCharges", Constants.convertToArabic(mvatamount));

                mainItem.put(mainObj);

                for (Order order : orderList) {
                    JSONObject subObj = new JSONObject();

                    JSONArray subItem2 = new JSONArray();
                    subObj.put("ItemPrice", Constants.convertToArabic(Constants.decimalFormat.format(Float.parseFloat(order.getItemPrice()))));
                    subObj.put("Qty", order.getQty());
                    subObj.put("Comments", order.getComment());
                    if (order.getAdditionalId() != null && !order.getAdditionalId().equals("")&& !order.getAdditionalPrice().equals("")) {

                        String ids = order.getAdditionalId();
                        String prices = order.getAdditionalPrice();
                        String qty = order.getSingleprice();
                        List<String> idsList = Arrays.asList(ids.split(","));
                        List<String> pricesList = Arrays.asList(prices.split(","));
                        List<String> qtyList = Arrays.asList(qty.split(","));
                        for (int i = 0; i < idsList.size(); i++) {
                            JSONObject subObj1 = new JSONObject();
                            subObj1.put("AdditionalID", idsList.get(i));
                            subObj1.put("AdditionalPrice", Constants.convertToArabic(Constants.decimalFormat.format(Float.parseFloat(pricesList.get(i)))));
                            subObj1.put("AddQty", qtyList.get(i));
                            subItem2.put(subObj1);
                        }
                    } else {
                        JSONObject subObj1 = new JSONObject();

                        subObj1.put("AdditionalID", "0");
                        subObj1.put("AdditionalPrice", "0.00");
                        subObj1.put("AddQty", "0");
                        subItem2.put(subObj1);
                    }

                    subObj.put("Additionals", subItem2);
                    subObj.put("ItemId", order.getItemId());
                    subObj.put("Size", order.getItemTypeId());

                    subItem1.put(subObj);
                }

                parent.put("SubItem", subItem1);
                parent.put("MainItem", mainItem);
                Log.i("TAG", parent.toString());
            } catch (JSONException je) {
                je.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.INSERT_ORDER_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(parent.toString());

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            result = convertInputStreamToString(inputStream);
                            Log.e("Responce", "" + result);
                            return result;
                        }

                    } catch (Exception e) {
                        Log.e("InputStream", e.getLocalizedMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                return "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                if (result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CheckoutActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if (language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.connection_error));
                    } else {
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.connection_error_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d1 = screenWidth * 0.85;
                    lp.width = (int) d1;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                } else {

                    String order_number = "";
                    try {
                        JSONObject jo = new JSONObject(result);
                        myDbHelper.deleteOrderTable();
                        Intent intent = new Intent(CheckoutActivity.this, MenuType.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();

                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                }
            }
            dialog.dismiss();
            super.onPostExecute(result1);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
