package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Constants;
import com.cs.dajen.JSONParser;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

/**
 * Created by CS on 29-12-2016.
 */
public class ChangePassword extends Activity {
    private String response12 = null;
    ProgressDialog dialog;

    EditText oldPassword, newPassword, confirmPassword;
    LinearLayout submit;
    String response, userId, phoneNumber;
    Toolbar toolbar;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    String language;
    AlertDialog customDialog;
    ImageView showPassword,showPassword1,showPassword2,back_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.change_password);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.change_password_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        oldPassword = (EditText) findViewById(R.id.old_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        submit = (LinearLayout) findViewById(R.id.submit_button);
        showPassword = (ImageView) findViewById(R.id.show_password);
        showPassword1 = (ImageView) findViewById(R.id.show_password1);
        showPassword2 = (ImageView) findViewById(R.id.show_password2);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPwd = oldPassword.getText().toString();
                String newPwd = newPassword.getText().toString();
                String confirmPwd = confirmPassword.getText().toString();
                if(oldPwd.length() == 0){
                    if (language.equalsIgnoreCase("En")) {
                        newPassword.setError(getResources().getString(R.string.pls_enter_old_pwd));
                    }
                    else{
                        newPassword.setError(getResources().getString(R.string.pls_enter_old_pwd_ar));
                    }
                }
                else if (newPwd.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        newPassword.setError(getResources().getString(R.string.pls_enter_new_pwd));
                    }
                    else{
                        newPassword.setError(getResources().getString(R.string.pls_enter_new_pwd_ar));
                    }
                } else if (newPwd.length() < 8) {
                    if (language.equalsIgnoreCase("En")) {
                        newPassword.setError(getResources().getString(R.string.pwd_8_chars));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        newPassword.setError(getResources().getString(R.string.pwd_8_chars_ar));
                    }
                } else if (confirmPwd.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        confirmPassword.setError(getResources().getString(R.string.pls_retype_pwd));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        confirmPassword.setError(getResources().getString(R.string.pls_retype_pwd_ar));
                    }
                } else if (!newPwd.equals(confirmPwd)) {
                    if (language.equalsIgnoreCase("En")) {
                        confirmPassword.setError(getResources().getString(R.string.pwd_not_match));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        confirmPassword.setError(getResources().getString(R.string.pwd_not_match_ar));
                    }
                } else {
                    new ChangePasswordResponse().execute(Constants.CHANGE_PASSWORD+userId+"?OldPsw="+oldPwd+"&NewPsw="+newPwd);
                }
            }
        });

        showPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(oldPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD){
                    oldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                else{
                    oldPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                oldPassword.setSelection(oldPassword.length());
            }
        });

        showPassword1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD){
                    newPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                else{
                    newPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                newPassword.setSelection(newPassword.length());
            }
        });

        showPassword2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(confirmPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD){
                    confirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                else{
                    confirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                confirmPassword.setSelection(confirmPassword.length());
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public class ChangePasswordResponse extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(ChangePassword.this);
            dialog = new MaterialDialog.Builder(ChangePassword.this)
                    .title(R.string.app_name)
                    .content("Changing Password...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }
        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if (result.equals("")) {
                        Toast.makeText(ChangePassword.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            if (result.contains("false")) {
                                dialog.dismiss();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChangePassword.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.dajen));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(getResources().getString(R.string.invalid_old_pwd));
                                   }
                                else{
                                    title.setText(getResources().getString(R.string.dajen_ar));
                                    yes.setText(getResources().getString(R.string.ok_ar));
                                    desc.setText(getResources().getString(R.string.invalid_old_pwd_ar));
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            } else if(result.contains("true")){
                                dialog.dismiss();
                                Toast.makeText(ChangePassword.this, getResources().getString(R.string.pwd_change_successful), Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }else {
                Toast.makeText(ChangePassword.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
}