package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.R;

/**
 * Created by CS on 24-05-2017.
 */

public class MenuType extends Activity implements View.OnClickListener{

    private DataBaseHelper myDbHelper;
    TextView cart_count;
    FrameLayout cart;
    AlertDialog customDialog;
    ImageView back_btn;
    RelativeLayout DajenMenu;
    LinearLayout BBQatHome, TestingMenu;
    SharedPreferences userPrefs;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.menu_type);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.menu_type_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        Constants.CurrentOrderActivity = "menutype";
        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 2;
        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        cart = (FrameLayout) findViewById(R.id.cart);
        cart_count = (TextView) findViewById(R.id.cart_count);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        DajenMenu = (RelativeLayout) findViewById(R.id.dajen_menu);
        BBQatHome = (LinearLayout) findViewById(R.id.bbqathome);
        TestingMenu = (LinearLayout) findViewById(R.id.testing_menu);

        cart.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        DajenMenu.setOnClickListener(this);
        BBQatHome.setOnClickListener(this);
        TestingMenu.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MenuType.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.yes));
                        no.setText(getResources().getString(R.string.no));
                        desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.yes_ar));
                        no.setText(getResources().getString(R.string.no_ar));
                        desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            Intent intent = new Intent(MenuType.this, OrderActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else{
                    Intent intent = new Intent(MenuType.this, OrderActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;
            case R.id.dajen_menu:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    if(userPrefs.getString("menu","").equals("bbq")){

                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MenuType.this);
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getLayoutInflater();
                            int layout = R.layout.alert_dialog;
                            View dialogView = inflater.inflate(layout, null);
                            dialogBuilder.setView(dialogView);
                            dialogBuilder.setCancelable(false);

                            TextView title = (TextView) dialogView.findViewById(R.id.title);
                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                            if(language.equalsIgnoreCase("En")) {
                                title.setText(getResources().getString(R.string.dajen));
                                yes.setText(getResources().getString(R.string.yes));
                                no.setText(getResources().getString(R.string.no));
                                desc.setText("You have " + myDbHelper.getTotalOrderQty() + " BBQ item(s) in your bag, by this action all items get clear. Do you want to continue with Main Menu?");
                            }
                            else{
                                title.setText(getResources().getString(R.string.dajen_ar));
                                yes.setText(getResources().getString(R.string.yes_ar));
                                no.setText(getResources().getString(R.string.no_ar));
                                desc.setText(" لديك" + myDbHelper.getTotalOrderQty() + " طلبات من قائمة ال BBQ في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع القائمة الرئيسية ؟");
                            }

                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    customDialog.dismiss();
                                    myDbHelper.deleteOrderTable();
                                    Constants.MENU_TYPE = "main";
                                    startActivity(new Intent(MenuType.this, MenuActivity.class));
                                }
                            });

                            no.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    customDialog.dismiss();
                                }
                            });

                            customDialog = dialogBuilder.create();
                            customDialog.show();
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = customDialog.getWindow();
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int screenWidth = size.x;

                            double d = screenWidth*0.85;
                            lp.width = (int) d;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                    }
                    else {
                        Constants.MENU_TYPE = "main";
                        startActivity(new Intent(MenuType.this, MenuActivity.class));
                    }
                }
                else {
                    Constants.MENU_TYPE = "main";
                    startActivity(new Intent(MenuType.this, MenuActivity.class));
                }
                break;

            case  R.id.bbqathome:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    if(userPrefs.getString("menu","").equals("main")){

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MenuType.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.dajen));
                            yes.setText(getResources().getString(R.string.yes));
                            no.setText(getResources().getString(R.string.no));
                            desc.setText("You have " + myDbHelper.getTotalOrderQty() + " Main Menu item(s) in your bag, by this action all items get clear. Do you want to continue with BBQ Menu?");
                        }
                        else{
                            title.setText(getResources().getString(R.string.dajen_ar));
                            yes.setText(getResources().getString(R.string.yes_ar));
                            no.setText(getResources().getString(R.string.no_ar));
                            desc.setText(" لديك" + myDbHelper.getTotalOrderQty() + " طلبات من القائمة الرئيسية  في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع قائمة ال BBQ ؟");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                                myDbHelper.deleteOrderTable();
                                Constants.MENU_TYPE = "bbq";
                                startActivity(new Intent(MenuType.this, BBQatHome.class));
                            }
                        });

                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

                    }
                    else{
                        Constants.MENU_TYPE = "bbq";
                        startActivity(new Intent(MenuType.this, BBQatHome.class));
                    }
                }
                else {
                    Constants.MENU_TYPE = "bbq";
                    startActivity(new Intent(MenuType.this, BBQatHome.class));
                }

                break;

            case R.id.testing_menu:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    if(userPrefs.getString("menu","").equals("bbq")){

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MenuType.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.dajen));
                            yes.setText(getResources().getString(R.string.yes));
                            no.setText(getResources().getString(R.string.no));
                            desc.setText("You have " + myDbHelper.getTotalOrderQty() + " BBQ item(s) in your bag, by this action all items get clear. Do you want to continue with Main Menu?");
                        }
                        else{
                            title.setText(getResources().getString(R.string.dajen_ar));
                            yes.setText(getResources().getString(R.string.yes_ar));
                            no.setText(getResources().getString(R.string.no_ar));
                            desc.setText(" لديك" + myDbHelper.getTotalOrderQty() + " طلبات من قائمة ال BBQ في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع القائمة الرئيسية ؟");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                                myDbHelper.deleteOrderTable();
                                Constants.MENU_TYPE = "main";
                                Constants.Itemid = 10;
                                Intent crispIntent = new Intent(MenuType.this,TestingMenuActivity.class);
                                startActivity(crispIntent);
                                finish();
                            }
                        });

                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else {
                        Constants.MENU_TYPE = "main";
                        Constants.Itemid = 10;
                        Intent crispIntent = new Intent(MenuType.this,TestingMenuActivity.class);
                        startActivity(crispIntent);
                        finish();
                    }
                }
                else {
                    Constants.MENU_TYPE = "main";
                    Constants.Itemid = 10;
                    Intent crispIntent = new Intent(MenuType.this,TestingMenuActivity.class);
                    startActivity(crispIntent);
                    finish();
                }
                break;

            case R.id.cart:
                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MenuType.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.cart_noitems));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.cart_noitems_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                } else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MenuType.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.cart_clear));
                        no.setText(getResources().getString(R.string.checkout_title));
                        desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.cart_clear_ar));
                        no.setText(getResources().getString(R.string.checkout_title_ar));
                        desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
                            cart_count.setText(""+myDbHelper.getTotalOrderQty());
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            Intent checkoutIntent = new Intent(MenuType.this, CheckoutActivity.class);
                            startActivity(checkoutIntent);
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                    break;
                }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MenuType.this, OrderActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
        cart_count.setText(""+myDbHelper.getTotalOrderQty());
    }
}
