package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.cs.dajen.Adapters.BreadAdapter;
import com.cs.dajen.Adapters.SauceAdapter;
import com.cs.dajen.Adapters.SoftDrinksAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.AdditionalPrices;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.Models.MenuItems;
import com.cs.dajen.Models.Modifiers;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.cs.dajen.widgets.CustomGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by CS on 28-04-2017.
 */

public class TestingMenuDetails extends Activity implements View.OnClickListener{

    ImageView back_btn, grid_image, comment, footer_plus, footer_minus;
    TextView title, grid_title, grid_desc, grid_price, breadTitle, sauceTitle;
    TextView jr_price, reg_price, spice_price;
    TextView footer_add, footer_quantity;
    public static TextView footer_price;
    LinearLayout junior_layout, regular_layout, spice_layout;
    LinearLayout medium_layout, large_layout ,small_layout;
//    LinearLayout fanta_layout, coke_layout, sprite_layout;
    ImageView medium_tick, large_tick, small_tick;
    int position, price_count=0;
    View line, line_junior, line_spice, pasta_line, sauceLine, breadLine;
    ArrayList<MenuItems> menuItems = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    String itemTypeId, itemTypeName;
    LinearLayout prices_layout, softDrinks_layout, size_layout;
    View prices_line, line_hor, line_sizes, line_hor3;
    TextView medium_text, large_text, medium_price, large_price;
    ImageView medium_glass, large_glass;
    private ArrayList<Modifiers> SizesList = new ArrayList<>();
    private ArrayList<Modifiers> SoftDrinksList = new ArrayList<>();
    private ArrayList<Modifiers> SauceList = new ArrayList<>();
    private ArrayList<Modifiers> BreadsList = new ArrayList<>();
    ListView SoftDrinks_listview;
    SoftDrinksAdapter mDrinksAdapter;
    public static int drinks_pos, refreshCount;
    int size_pos;
    String screen;
    AlertDialog alertDialog, customDialog;
    Context context;
    Boolean isItemTypesAvailable = true;
    CustomGridView sauceGrid,breadGrid;
    SauceAdapter mSauceAdapter;
    BreadAdapter mBreadAdapter;
    RelativeLayout extrasLayout;
    public static float total_price, sauce_price, bread_price, cooldrinkprice, price;
    public static int quantity;
    public static ArrayList<String> sauce_pos = new ArrayList<>();
    public static ArrayList<String> bread_pos = new ArrayList<>();

    SharedPreferences languagePrefs;
    String language;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.inside_product);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.inside_product_ar);
        }

        context = this;

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        Constants.CurrentOrderActivity = "inside";
        FooterActivity.tabBarPosition = 2;

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        drinks_pos = 0;
        refreshCount = 0;
        size_pos = 0;
        total_price = 0;
        cooldrinkprice = 0;
        quantity = 0;
        price = 0;
        sauce_price = 0;
        bread_price = 0;

        try {
            sauce_pos.clear();
            bread_pos.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        position = getIntent().getIntExtra("position",0);
        menuItems = (ArrayList<MenuItems>) getIntent().getSerializableExtra("arraylist");
        try {
            screen = getIntent().getStringExtra("screen");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Constants.ItemPosition = position;

        if(screen!=null && screen.equalsIgnoreCase("chicken")){
            menuItems = Constants.ChickenMenu;
        }
        else{
            menuItems = Constants.menuItems;
        }

        back_btn = (ImageView) findViewById(R.id.back_btn);
        grid_image = (ImageView) findViewById(R.id.grid_image);
        comment = (ImageView) findViewById(R.id.comment);
        line = (View) findViewById(R.id.prices_line);
        line_junior = (View) findViewById(R.id.line_junior);
        line_spice = (View) findViewById(R.id.line_spice);
        pasta_line = (View) findViewById(R.id.pasta_line);
        sauceLine = (View) findViewById(R.id.sauce_line);
        breadLine = (View) findViewById(R.id.bread_line);

        title = (TextView) findViewById(R.id.title);
        grid_title = (TextView) findViewById(R.id.grid_title);
        grid_desc = (TextView) findViewById(R.id.grid_desc);
        grid_price = (TextView) findViewById(R.id.grid_price);
        jr_price = (TextView) findViewById(R.id.junior_price);
        reg_price = (TextView) findViewById(R.id.regular_price);
        spice_price = (TextView) findViewById(R.id.spice_price);
        sauceTitle = (TextView) findViewById(R.id.sauce_title);
        breadTitle = (TextView) findViewById(R.id.bread_title);

        footer_add = (TextView) findViewById(R.id.footer_addmore);
        footer_price = (TextView) findViewById(R.id.footer_amount);
        footer_quantity = (TextView) findViewById(R.id.footer_qty);
        footer_plus = (ImageView) findViewById(R.id.footer_plus);
        footer_minus = (ImageView) findViewById(R.id.footer_minus);

        medium_text = (TextView) findViewById(R.id.med_text);
        large_text = (TextView) findViewById(R.id.large_text);
        medium_price = (TextView) findViewById(R.id.med_sr);
        large_price = (TextView) findViewById(R.id.large_sr);
        medium_glass = (ImageView) findViewById(R.id.med_glass);
        large_glass = (ImageView) findViewById(R.id.large_glass);

        prices_layout = (LinearLayout) findViewById(R.id.prices_layout);
        junior_layout = (LinearLayout) findViewById(R.id.junior_layout);
        regular_layout = (LinearLayout) findViewById(R.id.regular_layout);
        spice_layout = (LinearLayout) findViewById(R.id.spice_layout);

        softDrinks_layout = (LinearLayout) findViewById(R.id.softdrinks_layout);
        size_layout = (LinearLayout) findViewById(R.id.size_layout);
        extrasLayout = (RelativeLayout) findViewById(R.id.extra_layout);

//        fanta_layout = (LinearLayout) findViewById(R.id.fanta_layout);
//        coke_layout = (LinearLayout) findViewById(R.id.coke_layout);
//        sprite_layout = (LinearLayout) findViewById(R.id.sprite_layout);

        prices_line = (View) findViewById(R.id.prices_line);
        line_hor = (View) findViewById(R.id.line_hor);
        line_sizes = (View) findViewById(R.id.line_sizes);
        line_hor3 = (View) findViewById(R.id.line_hor3);

        medium_layout = (LinearLayout) findViewById(R.id.medium_layout);
        large_layout = (LinearLayout) findViewById(R.id.large_layout);
        small_layout = (LinearLayout) findViewById(R.id.small_layout);

        medium_tick = (ImageView) findViewById(R.id.med_tick);
        large_tick = (ImageView) findViewById(R.id.large_tick);
        small_tick = (ImageView) findViewById(R.id.small_tick);

        SoftDrinks_listview = (ListView) findViewById(R.id.soft_drinks_listview);
        sauceGrid = (CustomGridView) findViewById(R.id.extras_grid);
        breadGrid = (CustomGridView) findViewById(R.id.bread_grid);

//        fanta_tick = (ImageView) findViewById(R.id.fanta_tick);
//        coke_tick = (ImageView) findViewById(R.id.coke_tick);
//        sprite_tick = (ImageView) findViewById(R.id.sprite_tick);

        Constants.COMMENTS = "";

        SoftDrinks_listview.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + menuItems.get(position).getImage()).into(grid_image);
        if(language.equalsIgnoreCase("En")) {
            title.setText("" + menuItems.get(position).getItemName());
            grid_title.setText("" + menuItems.get(position).getItemName());
            grid_desc.setText("" + menuItems.get(position).getDesc());
        }
        else{
            title.setText("" + menuItems.get(position).getItemName_ar());
            grid_title.setText("" + menuItems.get(position).getItemName_ar());
            grid_desc.setText("" + menuItems.get(position).getDesc_ar());
        }

        if(!menuItems.get(position).getTwoCount()) {
            grid_price.setText("" + Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice1())));
            footer_price.setText("" + Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice1())));
            price = Float.parseFloat(menuItems.get(position).getPrice1());
            prices_layout.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
            isItemTypesAvailable = false;
            quantity = 1;
//            itemTypeId = "1";
//            itemTypeName = "REGULAR";
        }
        else{
            isItemTypesAvailable = true;
            footer_price.setText("0.00");
            grid_price.setText("0.00");
            footer_quantity.setText("0");
            if(!menuItems.get(position).getPrice2().equals("-1")){
                spice_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice2())));
//                grid_price.setText("" + menuItems.get(position).getPrice2()+" SR");
//                footer_price.setText("" + menuItems.get(position).getPrice2()+" SR");
//                price = Integer.parseInt(menuItems.get(position).getPrice2());
                price_count = price_count+1;

//                itemTypeId = "2";
//                itemTypeName = "SPICY";
            }
            if(price_count==0){
                spice_layout.setVisibility(View.GONE);
                junior_layout.setVisibility(View.VISIBLE);
                line_spice.setVisibility(View.GONE);
                line_junior.setVisibility(View.VISIBLE);

                if(language.equalsIgnoreCase("En")) {
                    showItemTypeDialog1(getResources().getString(R.string.regular), getResources().getString(R.string.junior));
                }
                else{
                    showItemTypeDialog1(getResources().getString(R.string.regular_ar), getResources().getString(R.string.junior_ar));
                }
            }
            else{
                if(language.equalsIgnoreCase("En")) {
                    showItemTypeDialog(getResources().getString(R.string.regular), getResources().getString(R.string.spice));
                }
                else{
                    showItemTypeDialog(getResources().getString(R.string.regular_ar), getResources().getString(R.string.spice_ar));
                }
            }
            if(!menuItems.get(position).getPrice1().equals("-1")){
                reg_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice1())));
                price_count = price_count+1;
                if(price_count==0){
//                    grid_price.setText("" + menuItems.get(position).getPrice1()+" SR");
//                    footer_price.setText("" + menuItems.get(position).getPrice1()+" SR");

//                    itemTypeId = "1";
//                    itemTypeName = "REGULAR";
                }
            }

            if(!menuItems.get(position).getPrice3().equals("-1")){
                jr_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice3())));
                price_count = price_count+1;
            }

            if(!menuItems.get(position).getPrice4().equals("-1")){
                reg_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice4())));
                price_count = price_count+1;
                if(price_count==2){
//                    grid_price.setText("" + menuItems.get(position).getPrice4()+" SR");
//                    footer_price.setText("" + menuItems.get(position).getPrice4()+" SR");
                    price = Float.parseFloat(menuItems.get(position).getPrice4());

//                    itemTypeId = "4";
//                    itemTypeName = "REGULAR";
                }
            }

            if(!menuItems.get(position).getPrice5().equals("-1")){
            }

            if(!menuItems.get(position).getPrice6().equals("-1")){
            }

            if(!menuItems.get(position).getPrice7().equals("-1")){
            }
        }

        if(!menuItems.get(position).getAdditionalId().equals("null")) {
            new GetAdditionals().execute(Constants.ADDITIONALS_URL + menuItems.get(position).getAdditionalId());
           }
        else{
            extrasLayout.setVisibility(View.GONE);
//            prices_layout.setVisibility(View.GONE);
            size_layout.setVisibility(View.GONE);
            softDrinks_layout.setVisibility(View.GONE);
            line_hor.setVisibility(View.GONE);
//            prices_line.setVisibility(View.GONE);
            line_sizes.setVisibility(View.GONE);
            pasta_line.setVisibility(View.GONE);
        }

        grid_image.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        junior_layout.setOnClickListener(this);
        regular_layout.setOnClickListener(this);
        spice_layout.setOnClickListener(this);
        medium_layout.setOnClickListener(this);
        large_layout.setOnClickListener(this);
        small_layout.setOnClickListener(this);
        footer_plus.setOnClickListener(this);
        footer_minus.setOnClickListener(this);
        footer_add.setOnClickListener(this);
        comment.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.back_btn:
                Intent sideItemsIntent = new Intent(TestingMenuDetails.this,TestingMenuActivity.class);
                startActivity(sideItemsIntent);
                finish();
                break;

            case R.id.comment:
                Intent commentIntent = new Intent(TestingMenuDetails.this, CommentActivity.class);
                if(language.equalsIgnoreCase("En")) {
                    commentIntent.putExtra("name",menuItems.get(position).getItemName());
                }else if(language.equalsIgnoreCase("Ar")){
                    commentIntent.putExtra("name",menuItems.get(position).getItemName_ar());
                }
                commentIntent.putExtra("image",menuItems.get(position).getImage());
                commentIntent.putExtra("screen","inside");
                startActivity(commentIntent);
                break;

            case R.id.junior_layout:
               juniorSelected();
                break;

            case R.id.regular_layout:
               regularSelected();
                break;

            case R.id.spice_layout:
                spiceSelected();
                break;

            case R.id.small_layout:
                small_tick.setVisibility(View.VISIBLE);
                medium_tick.setVisibility(View.INVISIBLE);
                large_tick.setVisibility(View.INVISIBLE);

                size_pos = 0;
                if(SizesList.get(0).getChildItems().size()==3) {
                    cooldrinkprice = Float.parseFloat(SizesList.get(0).getChildItems().get(0).getAdditionalPriceList().get(0).getPrice());
                }
                footer_price.setText(""+Constants.decimalFormat.format(((InsideProductActivity.price + InsideProductActivity.cooldrinkprice + InsideProductActivity.sauce_price + InsideProductActivity.bread_price)*InsideProductActivity.quantity)));
                break;

            case R.id.medium_layout:
                small_tick.setVisibility(View.INVISIBLE);
                medium_tick.setVisibility(View.VISIBLE);
                large_tick.setVisibility(View.INVISIBLE);

                if(SizesList.get(0).getChildItems().size()==2) {
                    size_pos = 0;
                    cooldrinkprice = Float.parseFloat(SizesList.get(0).getChildItems().get(0).getAdditionalPriceList().get(0).getPrice());
                }
                else if(SizesList.get(0).getChildItems().size()==3) {
                    size_pos = 1;
                    cooldrinkprice = Float.parseFloat(SizesList.get(0).getChildItems().get(1).getAdditionalPriceList().get(0).getPrice());
                }
                footer_price.setText(""+Constants.decimalFormat.format((((TestingMenuDetails.price + TestingMenuDetails.cooldrinkprice + TestingMenuDetails.sauce_price + TestingMenuDetails.bread_price)* TestingMenuDetails.quantity))));
                break;

            case R.id.large_layout:
                small_tick.setVisibility(View.INVISIBLE);
                medium_tick.setVisibility(View.INVISIBLE);
                large_tick.setVisibility(View.VISIBLE);

                if(SizesList.get(0).getChildItems().size()==2) {
                    size_pos = 1;
                    cooldrinkprice = Float.parseFloat(SizesList.get(0).getChildItems().get(1).getAdditionalPriceList().get(0).getPrice());
                }
                else if(SizesList.get(0).getChildItems().size()==3) {
                    size_pos = 2;
                    cooldrinkprice = Float.parseFloat(SizesList.get(0).getChildItems().get(2).getAdditionalPriceList().get(0).getPrice());
                }
                footer_price.setText(""+Constants.decimalFormat.format((((TestingMenuDetails.price + TestingMenuDetails.cooldrinkprice + TestingMenuDetails.sauce_price + TestingMenuDetails.bread_price)* TestingMenuDetails.quantity))));
                break;

            case R.id.footer_plus:
                if(!isItemTypesAvailable){
                    quantity = quantity + 1;
                    footer_quantity.setText("" + quantity);
                    footer_price.setText("" +Constants.decimalFormat.format(( ((TestingMenuDetails.price + TestingMenuDetails.cooldrinkprice + TestingMenuDetails.sauce_price + TestingMenuDetails.bread_price) * TestingMenuDetails.quantity))));
                }
                else{
                    if(itemTypeId!=null) {
                        quantity = quantity + 1;
                        footer_quantity.setText("" + quantity);
                        footer_price.setText("" + Constants.decimalFormat.format((((TestingMenuDetails.price + TestingMenuDetails.cooldrinkprice + TestingMenuDetails.sauce_price + TestingMenuDetails.bread_price) * TestingMenuDetails.quantity))));
                    }
                    else{
                        Toast.makeText(context, "Please select Item Type first", Toast.LENGTH_SHORT).show();
                    }
                }
                    break;

            case R.id.footer_minus:
                if (quantity > 1) {
                    quantity = quantity - 1;
                }
                footer_quantity.setText(""+quantity);
                footer_price.setText(""+Constants.decimalFormat.format((((TestingMenuDetails.price + TestingMenuDetails.cooldrinkprice + TestingMenuDetails.sauce_price + TestingMenuDetails.bread_price)* TestingMenuDetails.quantity))));
                break;

            case R.id.footer_addmore:

                if(quantity!=0) {
                    if (isItemTypesAvailable) {
                        if (itemTypeId == null) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TestingMenuDetails.this);
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getLayoutInflater();
                            int layout = R.layout.alert_dialog;
                            View dialogView = inflater.inflate(layout, null);
                            dialogBuilder.setView(dialogView);
                            dialogBuilder.setCancelable(false);

                            TextView title = (TextView) dialogView.findViewById(R.id.title);
                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                            no.setVisibility(View.GONE);
                            vert.setVisibility(View.GONE);

                            if(language.equalsIgnoreCase("En")) {
                                title.setText(getResources().getString(R.string.dajen));
                                yes.setText(getResources().getString(R.string.ok));
                                desc.setText(getResources().getString(R.string.inside_error1));
                            }
                            else{
                                title.setText(getResources().getString(R.string.dajen_ar));
                                yes.setText(getResources().getString(R.string.ok_ar));
                                desc.setText(getResources().getString(R.string.inside_error1_ar));
                            }

                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    customDialog.dismiss();
                                }
                            });

                            customDialog = dialogBuilder.create();
                            customDialog.show();
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = customDialog.getWindow();
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int screenWidth = size.x;

                            double d = screenWidth * 0.85;
                            lp.width = (int) d;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                        } else {
                            if (SoftDrinksList.size() > 0) {
                                String addl_ids = null , addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = "1,1";
                                ArrayList<Additionals> drinkArray = SoftDrinksList.get(0).getChildItems();
                                addl_ids = SizesList.get(0).getChildItems().get(size_pos).getAdditionalsId() + "," +
                                        drinkArray.get(drinks_pos).getAdditionalsId();
                                addl_name = SizesList.get(0).getChildItems().get(size_pos).getAdditionalName() + "," +
                                        drinkArray.get(drinks_pos).getAdditionalName();
                                addl_name_ar = SizesList.get(0).getChildItems().get(size_pos).getAdditionalNameAr() + "," +
                                        drinkArray.get(drinks_pos).getAdditionalName();
                                addl_image = SizesList.get(0).getChildItems().get(size_pos).getImages() + "," +
                                        drinkArray.get(drinks_pos).getImages();
                                addl_price = SizesList.get(0).getChildItems().get(size_pos).getAdditionalPriceList().get(0).getPrice() + ",0";

                                if(sauce_pos!=null && sauce_pos.size()>0){
                                    for(int i = 0; i<sauce_pos.size(); i++){
                                        addl_ids = addl_ids +","+ SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalsId();
                                        addl_name = addl_name +","+ SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar +","+ SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image +","+ SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getImages();
                                        addl_price = addl_price +","+ SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        addl_qty = addl_qty +",1";
                                    }
                                    }

                                if(bread_pos!=null && bread_pos.size()>0){
                                    for(int i = 0; i<bread_pos.size(); i++){
                                        addl_ids = addl_ids +","+ BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalsId();
                                        addl_name = addl_name +","+ BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar +","+ BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image +","+ BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getImages();
                                        addl_price = addl_price +","+ BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        addl_qty = addl_qty +",1";
                                    }
                                }

                                float finalPrice1 = TestingMenuDetails.price + TestingMenuDetails.cooldrinkprice + TestingMenuDetails.sauce_price + TestingMenuDetails.bread_price;
                                float finalPrice = TestingMenuDetails.price;
                                HashMap<String, String> values = new HashMap<>();
                                values.put("CategoryId", menuItems.get(position).getCatId());
                                values.put("ItemId", menuItems.get(position).getItemId());
                                values.put("ItemName", menuItems.get(position).getItemName());
                                values.put("ItemName_Ar", menuItems.get(position).getItemName_ar());
                                values.put("ItemDescription_En", menuItems.get(position).getDesc());
                                values.put("ItemDescription_Ar", menuItems.get(position).getDesc_ar());
                                values.put("ItemImage", menuItems.get(position).getImage());
                                values.put("ItemTypeId", itemTypeId);
                                values.put("TypeName", itemTypeName);
                                values.put("Price", Float.toString(finalPrice));
                                values.put("ModifierId", menuItems.get(position).getModifierID());
                                values.put("ModifierName", "0");
                                values.put("ModifierName_Ar", "0");
                                values.put("MdfrImage", "0");
                                values.put("AdditionalID", addl_ids);
                                values.put("AdditionalName", addl_name);
                                values.put("AdditionalName_Ar", addl_name_ar);
                                values.put("AddlImage", addl_image);
                                values.put("AddlTypeId", "0");
                                values.put("AdditionalPrice", addl_price);
                                values.put("Qty", Integer.toString(quantity));
                                values.put("TotalAmount", Float.toString(finalPrice1 * quantity));
                                values.put("Comment", Constants.COMMENTS);
                                values.put("Custom", "0");
                                values.put("SinglePrice", addl_qty);
                                myDbHelper.insertOrder(values);

                                if(Constants.MENU_TYPE.equals("main")) {
                                    userPrefEditor.putString("menu", "main");
                                    userPrefEditor.commit();
                                }
                                else if(Constants.MENU_TYPE.equals("bbq")){
                                    userPrefEditor.putString("menu", "bbq");
                                    userPrefEditor.commit();
                                }

                            } else {
                                HashMap<String, String> values = new HashMap<>();
                                values.put("CategoryId", menuItems.get(position).getCatId());
                                values.put("ItemId", menuItems.get(position).getItemId());
                                values.put("ItemName", menuItems.get(position).getItemName());
                                values.put("ItemName_Ar", menuItems.get(position).getItemName_ar());
                                values.put("ItemDescription_En", menuItems.get(position).getDesc());
                                values.put("ItemDescription_Ar", menuItems.get(position).getDesc_ar());
                                values.put("ItemImage", menuItems.get(position).getImage());
                                values.put("ItemTypeId", itemTypeId);
                                values.put("TypeName", itemTypeName);
                                values.put("Price", Float.toString(price));
                                values.put("ModifierId", menuItems.get(position).getModifierID());
                                values.put("ModifierName", "0");
                                values.put("ModifierName_Ar", "0");
                                values.put("MdfrImage", "0");
                                values.put("AdditionalID", "0");
                                values.put("AdditionalName", "0");
                                values.put("AdditionalName_Ar", "0");
                                values.put("AddlImage", "0");
                                values.put("AddlTypeId", "0");
                                values.put("AdditionalPrice", "0");
                                values.put("Qty", Integer.toString(quantity));
                                values.put("TotalAmount", Float.toString(((price * quantity) + cooldrinkprice)));
                                values.put("Comment", Constants.COMMENTS);
                                values.put("Custom", "0");
                                values.put("SinglePrice", "0");
                                myDbHelper.insertOrder(values);

                                if(Constants.MENU_TYPE.equals("main")) {
                                    userPrefEditor.putString("menu", "main");
                                    userPrefEditor.commit();
                                }
                                else if(Constants.MENU_TYPE.equals("bbq")){
                                    userPrefEditor.putString("menu", "bbq");
                                    userPrefEditor.commit();
                                }

                            }
                            Intent intent = new Intent(TestingMenuDetails.this, TestingMenuActivity.class);
                            intent.putExtra("id", Constants.Itemid);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        itemTypeId = "1";
                        itemTypeName = "REGULAR";
                        if (SoftDrinksList.size() > 0) {
                            ArrayList<Additionals> drinkArray = SoftDrinksList.get(0).getChildItems();
                            String addl_ids = null , addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = "1,1";

                            addl_ids = SizesList.get(0).getChildItems().get(size_pos).getAdditionalsId() + "," +
                                    drinkArray.get(drinks_pos).getAdditionalsId();
                            addl_name = SizesList.get(0).getChildItems().get(size_pos).getAdditionalName() + "," +
                                    drinkArray.get(drinks_pos).getAdditionalName();
                            addl_name_ar = SizesList.get(0).getChildItems().get(size_pos).getAdditionalNameAr() + "," +
                                    drinkArray.get(drinks_pos).getAdditionalName();
                            addl_image = SizesList.get(0).getChildItems().get(size_pos).getImages() + "," +
                                    drinkArray.get(drinks_pos).getImages();
                            addl_price = SizesList.get(0).getChildItems().get(size_pos).getAdditionalPriceList().get(0).getPrice() + ",0";

                            if(sauce_pos!=null && sauce_pos.size()>0){
                                for(int i = 0; i<sauce_pos.size(); i++){
                                    addl_ids = addl_ids +","+ SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalsId();
                                    addl_name = addl_name +","+ SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalName();
                                    addl_name_ar = addl_name_ar +","+ SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalNameAr();
                                    addl_image = addl_image +","+ SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getImages();
                                    addl_price = addl_price +","+ SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                    addl_qty = addl_qty +",1";
                                }
                            }

                            if(bread_pos!=null && bread_pos.size()>0){
                                for(int i = 0; i<bread_pos.size(); i++){
                                    addl_ids = addl_ids +","+ BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalsId();
                                    addl_name = addl_name +","+ BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalName();
                                    addl_name_ar = addl_name_ar +","+ BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalNameAr();
                                    addl_image = addl_image +","+ BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getImages();
                                    addl_price = addl_price +","+ BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                    addl_qty = addl_qty +",1";
                                }
                            }
                            float finalPrice1 = TestingMenuDetails.price + TestingMenuDetails.cooldrinkprice + TestingMenuDetails.sauce_price + TestingMenuDetails.bread_price;
                            float finalPrice = TestingMenuDetails.price;

                            HashMap<String, String> values = new HashMap<>();
                            values.put("CategoryId", menuItems.get(position).getCatId());
                            values.put("ItemId", menuItems.get(position).getItemId());
                            values.put("ItemName", menuItems.get(position).getItemName());
                            values.put("ItemName_Ar", menuItems.get(position).getItemName_ar());
                            values.put("ItemDescription_En", menuItems.get(position).getDesc());
                            values.put("ItemDescription_Ar", menuItems.get(position).getDesc_ar());
                            values.put("ItemImage", menuItems.get(position).getImage());
                            values.put("ItemTypeId", itemTypeId);
                            values.put("TypeName", itemTypeName);
                            values.put("Price", Float.toString(finalPrice));
                            values.put("ModifierId", menuItems.get(position).getModifierID());
                            values.put("ModifierName", "0");
                            values.put("ModifierName_Ar", "0");
                            values.put("MdfrImage", "0");
                            values.put("AdditionalID", addl_ids);
                            values.put("AdditionalName", addl_name);
                            values.put("AdditionalName_Ar", addl_name_ar);
                            values.put("AddlImage", addl_image);
                            values.put("AddlTypeId", "0");
                            values.put("AdditionalPrice", addl_price);
                            values.put("Qty", Integer.toString(quantity));
                            values.put("TotalAmount", Float.toString(finalPrice1 * quantity));
                            values.put("Comment", Constants.COMMENTS);
                            values.put("Custom", "0");
                            values.put("SinglePrice", addl_qty);
                            myDbHelper.insertOrder(values);

                            if(Constants.MENU_TYPE.equals("main")) {
                                userPrefEditor.putString("menu", "main");
                                userPrefEditor.commit();
                            }
                            else if(Constants.MENU_TYPE.equals("bbq")){
                                userPrefEditor.putString("menu", "bbq");
                                userPrefEditor.commit();
                            }

                        } else {
                            HashMap<String, String> values = new HashMap<>();
                            values.put("CategoryId", menuItems.get(position).getCatId());
                            values.put("ItemId", menuItems.get(position).getItemId());
                            values.put("ItemName", menuItems.get(position).getItemName());
                            values.put("ItemName_Ar", menuItems.get(position).getItemName_ar());
                            values.put("ItemDescription_En", menuItems.get(position).getDesc());
                            values.put("ItemDescription_Ar", menuItems.get(position).getDesc_ar());
                            values.put("ItemImage", menuItems.get(position).getImage());
                            values.put("ItemTypeId", itemTypeId);
                            values.put("TypeName", itemTypeName);
                            values.put("Price", Float.toString(price ));
                            values.put("ModifierId", menuItems.get(position).getModifierID());
                            values.put("ModifierName", "0");
                            values.put("ModifierName_Ar", "0");
                            values.put("MdfrImage", "0");
                            values.put("AdditionalID", "0");
                            values.put("AdditionalName", "0");
                            values.put("AdditionalName_Ar", "0");
                            values.put("AddlImage", "0");
                            values.put("AddlTypeId", "0");
                            values.put("AdditionalPrice", "0");
                            values.put("Qty", Integer.toString(quantity));
                            values.put("TotalAmount", Float.toString(((price * quantity) + cooldrinkprice)));
                            values.put("Comment", Constants.COMMENTS);
                            values.put("Custom", "0");
                            values.put("SinglePrice", "0");
                            myDbHelper.insertOrder(values);

                            if(Constants.MENU_TYPE.equals("main")) {
                                userPrefEditor.putString("menu", "main");
                                userPrefEditor.commit();
                            }
                            else if(Constants.MENU_TYPE.equals("bbq")){
                                userPrefEditor.putString("menu", "bbq");
                                userPrefEditor.commit();
                            }
                        }
                        Intent intent = new Intent(TestingMenuDetails.this, TestingMenuActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
                else{
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TestingMenuDetails.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.inside_error2));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.inside_error2_ar));
                    }
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                    }
                break;

            case R.id.grid_image:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                int layout = R.layout.bigimage_popup;
//                if(language.equalsIgnoreCase("En")){
//                    layout = R.layout.bigimage_popup;
//                }else if(language.equalsIgnoreCase("Ar")){
//                    layout = R.layout.bigimage_popup;
//                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);

                ImageView img = (ImageView) dialogView.findViewById(R.id.big_image);
                ImageView cance = (ImageView) dialogView.findViewById(R.id.rating_cancel);
                cance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                Glide.with(context).load(Constants.IMAGE_URL + menuItems.get(position).getImage()).into(img);

                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = context.getResources().getDimensionPixelSize(R.dimen.popup_height);
                window.setAttributes(lp);
                break;
        }
    }

    private class GetAdditionals extends AsyncTask<String, Integer, String> {

        String response;
        String  networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
//            Constants.menuItems.clear();
            dialog = new MaterialDialog.Builder(context)
                    .title(R.string.app_name)
                    .content("Loading Additionals...")
                    .progress(true, 0)
                    .cancelable(false)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONArray ja = new JSONArray(result);
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo = ja.getJSONObject(i);
                                Modifiers mod = new Modifiers();
                                ArrayList<Additionals> additionalsList = new ArrayList<>();
                                JSONArray key = jo.getJSONArray("Key");

                                for (int j = 0; j < key.length(); j++) {


                                    JSONObject jo1 = key.getJSONObject(j);
                                    if (j == 0) {
                                        String modifierName = jo1.getString("ModifierName");
                                        String modifierName_Ar = jo1.getString("ModifierName_Ar");
                                        String modifierId = jo1.getString("ModifierId");
//                                        if(modifierId.equalsIgnoreCase("1")){
//                                            continue Outer;
//                                        }
                                        mod.setModifierName(modifierName);
                                        mod.setModifierNameAr(modifierName_Ar);
                                        mod.setModifierId(modifierId);
                                    } else {

//                                        JSONObject issueObj = jo.getJSONObject("Value");
                                        Iterator iterator = jo1.keys();
                                        while (iterator.hasNext()) {
                                            Additionals adis = new Additionals();
                                            String additonalId = (String) iterator.next();
                                            ArrayList<AdditionalPrices> additionalPrices = new ArrayList<>();
                                            JSONArray ja1 = jo1.getJSONArray(additonalId);

                                            for (int k = 0; k < ja1.length(); k++) {
                                                JSONArray additionals = ja1.getJSONArray(k);
                                                if (k == 0) {
                                                    JSONObject additionalDetails = additionals.getJSONObject(0);
                                                    String additionalName = additionalDetails.getString("AdditionalName");
                                                    String additionalName_Ar = additionalDetails.getString("AdditionalName_Ar");
                                                    String images = additionalDetails.getString("Images");
                                                    String additionalId = additionalDetails.getString("AdditionalId");
                                                    adis.setAdditionalName(additionalName);
                                                    adis.setAdditionalNameAr(additionalName_Ar);
                                                    adis.setAdditionalsId(additionalId);
                                                    adis.setImages(images);
                                                    adis.setModifierId(mod.getModifierId());
                                                } else {
                                                    for (int l = 0; l < additionals.length(); l++) {
                                                        AdditionalPrices ap = new AdditionalPrices();
                                                        JSONObject jo2 = additionals.getJSONObject(l);
                                                        ap.setItemType(jo2.getString("Type"));
                                                        ap.setPrice(jo2.getString("Price"));
                                                        additionalPrices.add(ap);
                                                    }
                                                    adis.setAdditionalPriceList(additionalPrices);
                                                }
                                            }
                                            additionalsList.add(adis);
                                        }
                                    }
                                }
                                mod.setChildItems(additionalsList);
                                if(mod.getModifierId().equals("1003")){
                                    SizesList.add(mod);
                                }
                                else if(mod.getModifierId().equals("1")){
                                   SoftDrinksList.add(mod);
                                }
                                else if(mod.getModifierId().equals("1004")){
                                    SauceList.add(mod);
                                }
                                else if(mod.getModifierId().equals("1005")){
                                    BreadsList.add(mod);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(SauceList.size()==0 && BreadsList.size()==0){
                    extrasLayout.setVisibility(View.GONE);
                }
                else if(SauceList.size()==0){
                    sauceLine.setVisibility(View.GONE);
                    sauceTitle.setVisibility(View.GONE);
                    sauceGrid.setVisibility(View.GONE);
                }
                else if(BreadsList.size()==0){
                    breadLine.setVisibility(View.GONE);
                    breadTitle.setVisibility(View.GONE);
                    breadGrid.setVisibility(View.GONE);
                }
                else{
                    mSauceAdapter = new SauceAdapter(getApplicationContext(),SauceList.get(0).getChildItems(), language, "inside");
                    sauceGrid.setAdapter(mSauceAdapter);
                    mSauceAdapter.notifyDataSetChanged();

                    mBreadAdapter = new BreadAdapter(getApplicationContext(),BreadsList.get(0).getChildItems(), language, "inside");
                    breadGrid.setAdapter(mBreadAdapter);
                    mBreadAdapter.notifyDataSetChanged();
                }
                if(SizesList.get(0).getModifierName().equals("SIZES")){
                    setSizes();
                }
                mDrinksAdapter = new SoftDrinksAdapter(getApplicationContext(),SoftDrinksList.get(0).getChildItems(), language);
                SoftDrinks_listview.setAdapter(mDrinksAdapter);
                mDrinksAdapter.notifyDataSetChanged();

//                mSauceAdapter = new SauceAdapter(getApplicationContext(),SauceList.get(0).getChildItems(), language, "testing");
//                sauceGrid.setAdapter(mSauceAdapter);
//                mSauceAdapter.notifyDataSetChanged();
//
//                mBreadAdapter = new BreadAdapter(getApplicationContext(),BreadsList.get(0).getChildItems(), language, "testing");
//                breadGrid.setAdapter(mBreadAdapter);
//                mBreadAdapter.notifyDataSetChanged();
            }
            else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent sideItemsIntent = new Intent(TestingMenuDetails.this,TestingMenuActivity.class);
        startActivity(sideItemsIntent);
        finish();
    }

    public void setSizes(){
        small_layout.setVisibility(View.GONE);
        line_hor3.setVisibility(View.GONE);
        if(language.equalsIgnoreCase("En")) {
            medium_text.setText(SizesList.get(0).getChildItems().get(0).getAdditionalName());
            large_text.setText(SizesList.get(0).getChildItems().get(1).getAdditionalName());

            if(!SizesList.get(0).getChildItems().get(0).getAdditionalPriceList().get(0).getPrice().equals("0")) {
                medium_price.setText(getResources().getString(R.string.add) + " " + Constants.decimalFormat.format(Float.parseFloat(SizesList.get(0).getChildItems().get(0).getAdditionalPriceList().get(0).getPrice())));
            }
            else{
                medium_price.setVisibility(View.INVISIBLE);
            }

            large_price.setText(getResources().getString(R.string.add)+" "+Constants.decimalFormat.format(Float.parseFloat(SizesList.get(0).getChildItems().get(1).getAdditionalPriceList().get(0).getPrice())));
        }
        else{
            medium_text.setText(SizesList.get(0).getChildItems().get(0).getAdditionalNameAr());
            large_text.setText(SizesList.get(0).getChildItems().get(1).getAdditionalNameAr());

            medium_price.setText(getResources().getString(R.string.add_ar)+" "+Constants.decimalFormat.format(Float.parseFloat(SizesList.get(0).getChildItems().get(0).getAdditionalPriceList().get(0).getPrice())));
            large_price.setText(getResources().getString(R.string.add_ar)+" "+Constants.decimalFormat.format(Float.parseFloat(SizesList.get(0).getChildItems().get(1).getAdditionalPriceList().get(0).getPrice())));
        }

        medium_tick.setVisibility(View.VISIBLE);
        size_pos = 0;
        cooldrinkprice = Float.parseFloat(SizesList.get(0).getChildItems().get(0).getAdditionalPriceList().get(0).getPrice());
        footer_price.setText(""+Constants.decimalFormat.format((((TestingMenuDetails.price + TestingMenuDetails.cooldrinkprice + TestingMenuDetails.sauce_price + TestingMenuDetails.bread_price)* TestingMenuDetails.quantity))));

//        Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + SizesList.get(0).getChildItems().get(0).getImages()).placeholder(R.drawable.icon_xxhdpi).into(small_glass);
//        Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + SizesList.get(0).getChildItems().get(1).getImages()).placeholder(R.drawable.icon_xxhdpi).into(medium_glass);
//        Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + SizesList.get(0).getChildItems().get(2).getImages()).placeholder(R.drawable.icon_xxhdpi).into(large_glass);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        refreshCount = 0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }


    public void showItemTypeDialog(String text1, String text2){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TestingMenuDetails.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        if(language.equalsIgnoreCase("En")) {
            desc.setText(getResources().getString(R.string.inside_error1));
        }
        else{
            desc.setText(getResources().getString(R.string.inside_error1_ar));
        }
        no.setText(text2);
        yes.setText(text1);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                regularSelected();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                spiceSelected();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void showItemTypeDialog1(String text1, String text2){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TestingMenuDetails.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        if(language.equalsIgnoreCase("En")) {
            desc.setText(getResources().getString(R.string.inside_error1));
        }
        else{
            desc.setText(getResources().getString(R.string.inside_error1_ar));
        }
        no.setText(text2);
        yes.setText(text1);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                regularSelected();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                juniorSelected();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void regularSelected(){
        junior_layout.setBackgroundColor(Color.TRANSPARENT);
        regular_layout.setBackgroundResource(R.drawable.menu_selected);
        spice_layout.setBackgroundColor(Color.TRANSPARENT);

        jr_price.setTextColor(getResources().getColor(R.color.homecolor));
        reg_price.setTextColor(Color.parseColor("#000000"));
        spice_price.setTextColor(getResources().getColor(R.color.homecolor));

        if(!menuItems.get(position).getPrice1().equals("-1")){
            grid_price.setText("" + Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice1())));
            price = Float.parseFloat(menuItems.get(position).getPrice1());
            if(quantity == 0){
                quantity = quantity + 1;
                footer_quantity.setText("" + quantity);
            }
            footer_price.setText(""+Constants.decimalFormat.format(((InsideProductActivity.price + InsideProductActivity.cooldrinkprice + InsideProductActivity.sauce_price + InsideProductActivity.bread_price)*InsideProductActivity.quantity)));

            itemTypeId = "1";
            itemTypeName = "REGULAR";
        }
        else{
            grid_price.setText("" + Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice4())));
            price = Float.parseFloat(menuItems.get(position).getPrice4());
            if(quantity == 0){
                quantity = quantity + 1;
                footer_quantity.setText("" + quantity);
            }
            footer_price.setText(""+Constants.decimalFormat.format((((InsideProductActivity.price + InsideProductActivity.cooldrinkprice + InsideProductActivity.sauce_price + InsideProductActivity.bread_price)*InsideProductActivity.quantity))));

            itemTypeId = "4";
            itemTypeName = "REGULAR";
        }
    }

    public void juniorSelected(){
        junior_layout.setBackgroundResource(R.drawable.menu_selected);
        regular_layout.setBackgroundColor(Color.TRANSPARENT);
        spice_layout.setBackgroundColor(Color.TRANSPARENT);

        jr_price.setTextColor(Color.parseColor("#000000"));
        reg_price.setTextColor(getResources().getColor(R.color.homecolor));
        spice_price.setTextColor(getResources().getColor(R.color.homecolor));

        grid_price.setText("" + Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice3())));
        price = Float.parseFloat(menuItems.get(position).getPrice3());
        if(quantity == 0){
            quantity = quantity + 1;
            footer_quantity.setText("" + quantity);
        }
        footer_price.setText(""+Constants.decimalFormat.format((((InsideProductActivity.price + InsideProductActivity.cooldrinkprice + InsideProductActivity.sauce_price + InsideProductActivity.bread_price)*InsideProductActivity.quantity))));

        itemTypeId = "3";
        itemTypeName = "JUNIOR";
    }

    public void spiceSelected(){
        junior_layout.setBackgroundColor(Color.TRANSPARENT);
        regular_layout.setBackgroundColor(Color.TRANSPARENT);
        spice_layout.setBackgroundResource(R.drawable.menu_selected);

        jr_price.setTextColor(getResources().getColor(R.color.homecolor));
        reg_price.setTextColor(getResources().getColor(R.color.homecolor));
        spice_price.setTextColor(Color.parseColor("#000000"));

        grid_price.setText("" + Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice2())));
        price = Float.parseFloat(menuItems.get(position).getPrice2());
        if(quantity == 0){
            quantity = quantity + 1;
            footer_quantity.setText("" + quantity);
        }
        footer_price.setText(""+Constants.decimalFormat.format(((InsideProductActivity.price + InsideProductActivity.cooldrinkprice + InsideProductActivity.sauce_price + InsideProductActivity.bread_price)*InsideProductActivity.quantity)));

        itemTypeId = "2";
        itemTypeName = "SPICY";
    }
}
