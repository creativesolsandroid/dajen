package com.cs.dajen.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.R;

/**
 * Created by CS on 26-05-2017.
 */

public class AboutUs extends Activity {

    ImageView back_btn;
    private DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.about_us);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.about_us_ar);
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar4);
        FooterActivity.tabBarPosition = 4;
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());

        back_btn = (ImageView) findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
