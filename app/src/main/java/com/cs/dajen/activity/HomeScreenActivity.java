package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.cs.dajen.Adapters.BannersAdapter;
import com.cs.dajen.Adapters.HomeBannersAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.Banners;
import com.cs.dajen.Models.MenuItems;
import com.cs.dajen.Models.Rating;
import com.cs.dajen.Models.RatingDetails;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.github.demono.AutoScrollViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static com.cs.dajen.Constants.ChickenMenu;

/**
 * Created by CS on 24-04-2017.
 */

public class HomeScreenActivity extends Activity implements View.OnClickListener {

    RelativeLayout home_order, freeDelivery, location, website;
    LinearLayout FavOrder, Promotions;
    boolean doubleBackToExitPressedOnce = false;
    private DataBaseHelper myDbHelper;
    TextView ItemCount;
    LinearLayout MainMenu, TestingMenu, BBQatHome;
    Context ctx;
    SharedPreferences userPrefs;
    String mLoginStatus;
    private static final int LOGIN_REQUEST_FAV = 2;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    ImageView chicken;
    AlertDialog customDialog;
    boolean isFirstTime = true;
    int promoCatId;

    int ratingFromService;
    RelativeLayout rate_layout;
    MaterialRatingBar ratingBar;
    TextView cancel;
    String orderId;
    String userId;
    String OrderType = "", OrderStatus;
    ImageView promoImage;
    final ArrayList<RatingDetails> rateArray = new ArrayList<>();
    ArrayList<Banners> bannersArrayList = new ArrayList<>();
    ImageView storeImage;
    TextView changeLanguage;
    CircleIndicator defaultIndicator, defaultIndicator1;
    AutoScrollViewPager defaultViewpager, defaultViewpager1;
    public static TextView MenuName;
    BannersAdapter mAdapter;
    HomeBannersAdapter mBannersAdapter;
    Integer[] PromoBanners = {R.drawable.itemid_3,R.drawable.itemid_7,R.drawable.itemid_25,R.drawable.itemid_78};
    String[] PromoItemNames= {"Grilled 1/2 BBQ Roast & Rice Meal","Grilled BBQ Chicken","Family Broasted Chicken Meal","Entrecote"};
    String[] PromoItemNames_ar= {"وجبة نص دجاجه مشوي مع باربكيوم مع ارز","دجاج مشوى باربكيوم","وجبة بروستد العائليه","Entrecote"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.homescreen);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.homescreen_ar);
        }

        ctx = this;

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        myDbHelper = new DataBaseHelper(getApplicationContext());
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());

        defaultViewpager = (AutoScrollViewPager) findViewById(R.id.viewPager);
        defaultIndicator = (CircleIndicator) findViewById(R.id.indicator);
        defaultViewpager1 = (AutoScrollViewPager) findViewById(R.id.viewPager1);
        defaultIndicator1 = (CircleIndicator) findViewById(R.id.indicator1);

        home_order = (RelativeLayout) findViewById(R.id.home_order);
        freeDelivery = (RelativeLayout) findViewById(R.id.freedelivery);
        location = (RelativeLayout) findViewById(R.id.location);
        website = (RelativeLayout) findViewById(R.id.website);
        chicken = (ImageView) findViewById(R.id.meal_image);
        storeImage = (ImageView) findViewById(R.id.store_image);
        promoImage = (ImageView) findViewById(R.id.promo_image);

        changeLanguage = (TextView) findViewById(R.id.arabic_to_english);
        MenuName = (TextView) findViewById(R.id.menu_name);

        ratingBar = (MaterialRatingBar) findViewById(R.id.ratingBar1);
        cancel = (TextView) findViewById(R.id.skip_rating);
        rate_layout = (RelativeLayout) findViewById(R.id.rate_layout);

        ItemCount = (TextView) findViewById(R.id.home_itemcount);

        Promotions = (LinearLayout) findViewById(R.id.home_promotions);
        FavOrder = (LinearLayout) findViewById(R.id.fav_order);
        MainMenu = (LinearLayout) findViewById(R.id.main_menu);
        TestingMenu = (LinearLayout) findViewById(R.id.testing_menu);
        BBQatHome = (LinearLayout) findViewById(R.id.bbqathome);

        home_order.setOnClickListener(this);
        freeDelivery.setOnClickListener(this);
        MainMenu.setOnClickListener(this);
        TestingMenu.setOnClickListener(this);
        BBQatHome.setOnClickListener(this);
        location.setOnClickListener(this);
        website.setOnClickListener(this);
        FavOrder.setOnClickListener(this);
        changeLanguage.setOnClickListener(this);
        Promotions.setOnClickListener(this);

        if(language.equalsIgnoreCase("En")) {
            mAdapter = new BannersAdapter(HomeScreenActivity.this, PromoBanners, PromoItemNames, language,HomeScreenActivity.this);
        }
        else{
            mAdapter = new BannersAdapter(HomeScreenActivity.this, PromoBanners, PromoItemNames_ar, language,HomeScreenActivity.this);
        }
        defaultViewpager.setAdapter(mAdapter);
        defaultIndicator.setViewPager(defaultViewpager);
        defaultViewpager.startAutoScroll();
        defaultViewpager.setCycle(true);

//        if(ChickenMenu.size()==0) {
//            new GetMenuItems().execute(Constants.GET_ITEMS + "1");
//        }
    }

    @Override
    public void onBackPressed() {
        if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this,"Press BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!isFirstTime) {
            new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL + "-1&uId=" + userId);
        }
        else{
            new GetBanners().execute(Constants.GET_BANNERS +"-1&uId=" + userId);
        }
        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar1);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar4);
        }
        ItemCount.setText(""+myDbHelper.getTotalOrderQty());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.home_promotions:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    if(userPrefs.getString("menu","").equals("bbq")){

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HomeScreenActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.dajen));
                            yes.setText(getResources().getString(R.string.yes));
                            no.setText(getResources().getString(R.string.no));
                            desc.setText("You have " + myDbHelper.getTotalOrderQty() + " BBQ item(s) in your bag, by this action all items get clear. Do you want to continue with Main Menu?");
                        }
                        else {
                            title.setText(getResources().getString(R.string.dajen_ar));
                            yes.setText(getResources().getString(R.string.yes_ar));
                            no.setText(getResources().getString(R.string.no_ar));
                            desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "طلبات من قائمة ال BBQ في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع القائمة الرئيسية ؟");
                        }
                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                                myDbHelper.deleteOrderTable();
                                Constants.MENU_TYPE = "main";
                                Constants.Itemid = 11;
                                Intent crispIntent = new Intent(HomeScreenActivity.this,ProductActivity.class);
                                crispIntent.putExtra("id",11);
                                startActivity(crispIntent);
                                finish();
                            }
                        });

                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else {
                        Constants.MENU_TYPE = "main";
                        Constants.Itemid = promoCatId;
                        Intent crispIntent = new Intent(HomeScreenActivity.this,ProductActivity.class);
                        crispIntent.putExtra("id",promoCatId);
                        startActivity(crispIntent);
                        finish();
                    }
                }
                else {
                    Constants.MENU_TYPE = "main";
                    Constants.Itemid = promoCatId;
                    Intent crispIntent = new Intent(HomeScreenActivity.this,ProductActivity.class);
                    crispIntent.putExtra("id",promoCatId);
                    startActivity(crispIntent);
                    finish();
                }
                break;

            case R.id.arabic_to_english:
                if(language.equalsIgnoreCase("En")){
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();

                    FooterActivity.tabBarPosition = 1;
                    Intent intent = new Intent(HomeScreenActivity.this, HomeScreenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                else{
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();

                    FooterActivity.tabBarPosition = 1;
                    Intent intent = new Intent(HomeScreenActivity.this, HomeScreenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;
            case R.id.main_menu:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    if(userPrefs.getString("menu","").equals("bbq")){

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HomeScreenActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.dajen));
                            yes.setText(getResources().getString(R.string.yes));
                            no.setText(getResources().getString(R.string.no));
                            desc.setText("You have " + myDbHelper.getTotalOrderQty() + " BBQ item(s) in your bag, by this action all items get clear. Do you want to continue with Main Menu?");
                        }
                        else{
                            title.setText(getResources().getString(R.string.dajen_ar));
                            yes.setText(getResources().getString(R.string.yes_ar));
                            no.setText(getResources().getString(R.string.no_ar));
                            desc.setText(" لديك" + myDbHelper.getTotalOrderQty() + " طلبات من قائمة ال BBQ في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع القائمة الرئيسية ؟");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                                myDbHelper.deleteOrderTable();
                                Constants.MENU_TYPE = "main";
                                startActivity(new Intent(HomeScreenActivity.this, MenuActivity.class));
                                finish();
                            }
                        });

                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else {
                        Constants.MENU_TYPE = "main";
                        startActivity(new Intent(HomeScreenActivity.this, MenuActivity.class));
                        finish();
                    }
                }
                else {
                    Constants.MENU_TYPE = "main";
                    startActivity(new Intent(HomeScreenActivity.this, MenuActivity.class));
                    finish();
                }
                break;

            case R.id.testing_menu:

                if(myDbHelper.getTotalOrderQty() > 0) {
                    if(userPrefs.getString("menu","").equals("bbq")){

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HomeScreenActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.dajen));
                            yes.setText(getResources().getString(R.string.yes));
                            no.setText(getResources().getString(R.string.no));
                            desc.setText("You have " + myDbHelper.getTotalOrderQty() + " BBQ item(s) in your bag, by this action all items get clear. Do you want to continue with Main Menu?");
                        }
                        else{
                            title.setText(getResources().getString(R.string.dajen_ar));
                            yes.setText(getResources().getString(R.string.yes_ar));
                            no.setText(getResources().getString(R.string.no_ar));
                            desc.setText(" لديك" + myDbHelper.getTotalOrderQty() + " طلبات من قائمة ال BBQ في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع القائمة الرئيسية ؟");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                                myDbHelper.deleteOrderTable();
                                Constants.MENU_TYPE = "main";
                                Constants.Itemid = 10;
                                Intent crispIntent = new Intent(HomeScreenActivity.this,TestingMenuActivity.class);
                                crispIntent.putExtra("id",10);
                                startActivity(crispIntent);
                                finish();
                            }
                        });

                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else {
                        Constants.MENU_TYPE = "main";
                        Constants.Itemid = 10;
                        Intent crispIntent = new Intent(HomeScreenActivity.this,TestingMenuActivity.class);
                        crispIntent.putExtra("id",10);
                        startActivity(crispIntent);
                        finish();
                    }
                }
                else {
                    Constants.MENU_TYPE = "main";
                    Constants.Itemid = 10;
                    Intent crispIntent = new Intent(HomeScreenActivity.this,TestingMenuActivity.class);
                    crispIntent.putExtra("id",10);
                    startActivity(crispIntent);
                    finish();
                }
                break;

            case R.id.bbqathome:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    if(userPrefs.getString("menu","").equals("main")){

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HomeScreenActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.dajen));
                            yes.setText(getResources().getString(R.string.yes));
                            no.setText(getResources().getString(R.string.no));
                            desc.setText("You have " + myDbHelper.getTotalOrderQty() + " Main Menu item(s) in your bag, by this action all items get clear. Do you want to continue with BBQ Menu?");
                        }
                        else{
                            title.setText(getResources().getString(R.string.dajen_ar));
                            yes.setText(getResources().getString(R.string.yes_ar));
                            no.setText(getResources().getString(R.string.no_ar));
                            desc.setText(" لديك" + myDbHelper.getTotalOrderQty() + " طلبات من القائمة الرئيسية  في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع قائمة ال BBQ ؟");

                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                                myDbHelper.deleteOrderTable();
                                Constants.MENU_TYPE = "bbq";
                                startActivity(new Intent(HomeScreenActivity.this, BBQatHome.class));
                                finish();
                            }
                        });

                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

                    }
                    else{
                        Constants.MENU_TYPE = "bbq";
                        startActivity(new Intent(HomeScreenActivity.this, BBQatHome.class));
                        finish();
                    }
                }
                else {
                    Constants.MENU_TYPE = "bbq";
                    startActivity(new Intent(HomeScreenActivity.this, BBQatHome.class));
                    finish();
                }
                break;

            case R.id.fav_order:
                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Constants.TRACK_ID = "-1";
                    Intent favIntent = new Intent(HomeScreenActivity.this,TrackOrder.class);
                    favIntent.putExtra("screen","home");
                    startActivity(favIntent);
                    finish();
                } else {
                    Intent intent = new Intent(HomeScreenActivity.this, LoginActivity.class);
                    startActivityForResult(intent, LOGIN_REQUEST_FAV);
                }
                break;
            case R.id.home_order:

                if(myDbHelper.getTotalOrderQty() > 0) {
                    if(userPrefs.getString("menu","").equals("bbq")){

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HomeScreenActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.dajen));
                            yes.setText(getResources().getString(R.string.yes));
                            no.setText(getResources().getString(R.string.no));
                            desc.setText("You have " + myDbHelper.getTotalOrderQty() + " BBQ item(s) in your bag, by this action all items get clear. Do you want to continue with Main Menu?");
                        }
                        else{
                            title.setText(getResources().getString(R.string.dajen_ar));
                            yes.setText(getResources().getString(R.string.yes_ar));
                            no.setText(getResources().getString(R.string.no_ar));
                            desc.setText(" لديك" + myDbHelper.getTotalOrderQty() + " طلبات من قائمة ال BBQ في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع القائمة الرئيسية ؟");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                                myDbHelper.deleteOrderTable();
                                Constants.MENU_TYPE = "main";
                                if(ChickenMenu.size()>0) {
                                    Constants.Itemid = 1;
                                    Intent produtIntent = new Intent(HomeScreenActivity.this, InsideProductActivity.class);
                                    produtIntent.putExtra("position", 0);
                                    produtIntent.putExtra("arraylist", ChickenMenu);
                                    produtIntent.putExtra("screen", "chicken");
                                    startActivity(produtIntent);
                                    finish();
                                }
                            }
                        });

                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else {
                        Constants.MENU_TYPE = "main";
                        if(Constants.ChickenMenu.size()>0) {
                            Constants.Itemid = 1;
                            Intent produtIntent = new Intent(HomeScreenActivity.this, InsideProductActivity.class);
                            produtIntent.putExtra("position", 0);
                            produtIntent.putExtra("arraylist", ChickenMenu);
                            produtIntent.putExtra("screen", "chicken");
                            startActivity(produtIntent);
                            finish();
                        }
                    }
                }
                else {
                    Constants.MENU_TYPE = "main";
                    if(Constants.ChickenMenu.size()>0) {
                        Constants.Itemid = 1;
                        Intent produtIntent = new Intent(HomeScreenActivity.this, InsideProductActivity.class);
                        produtIntent.putExtra("position", 0);
                        produtIntent.putExtra("arraylist", ChickenMenu);
                        produtIntent.putExtra("screen", "chicken");
                        startActivity(produtIntent);
                        finish();
                    }
                }
                break;

            case R.id.freedelivery:
//                if(userPrefs.getString("menu","1").equals("main")) {
//                    if (Constants.CurrentOrderActivity.equals("inside")) {
//                        Intent insideIntent = new Intent(HomeScreenActivity.this, InsideProductActivity.class);
//                        insideIntent.putExtra("position", Constants.ItemPosition);
//                        insideIntent.putExtra("arraylist", Constants.menuItems);
//                        insideIntent.putExtra("screen", "inside");
//                        startActivity(insideIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("salad")) {
//                        Intent sideItemsIntent = new Intent(HomeScreenActivity.this, SaladActivity.class);
//                        sideItemsIntent.putExtra("position", Constants.ItemPosition);
//                        sideItemsIntent.putExtra("arraylist", Constants.menuItems);
//                        startActivity(sideItemsIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("product")) {
//                        Intent sideItemsIntent = new Intent(HomeScreenActivity.this, ProductActivity.class);
//                        sideItemsIntent.putExtra("id", Constants.Itemid);
//                        startActivity(sideItemsIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("menu")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, MenuActivity.class);
//                        menuIntent.putExtra("type", "main");
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("checkout")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, CheckoutActivity.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("ordertype")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, OrderTypeActivity.class);
//                        startActivity(menuIntent);
//                        finish();
////                    } else if (Constants.CurrentOrderActivity.equals("stores")) {
////                        Intent menuIntent = new Intent(HomeScreenActivity.this, SelectStoresActivity.class);
////                        startActivity(menuIntent);
////                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("address")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, AddressActivity.class);
//                        menuIntent.putExtra("confirm_order", true);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("confirmation")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, Confirmation.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("completed")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, OrderCompleted.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("menutype")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, MenuType.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("fav")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, FavoriteOrdersActivity.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("history")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, OrderHistoryActivity.class);
//                        startActivity(menuIntent);
//                        finish();
//                    }
//                    else {
                        if (myDbHelper.getTotalOrderQty() > 0) {
                            if (userPrefs.getString("menu", "main").equals("main")) {
                                Intent menuIntent = new Intent(HomeScreenActivity.this, MenuActivity.class);
                                startActivity(menuIntent);
                                finish();
                            } else if (userPrefs.getString("menu", "main").equals("bbq")) {
                                Intent menuIntent = new Intent(HomeScreenActivity.this, BBQatHome.class);
                                startActivity(menuIntent);
                                finish();
                            }
                        } else {
                            Intent menuIntent = new Intent(HomeScreenActivity.this, OrderActivity.class);
                            startActivity(menuIntent);
                            finish();
                        }
//                    }
//                }
//                else if (userPrefs.getString("menu", "1").equals("bbq")) {
//                    if (Constants.CurrentOrderActivity.equals("bbqMenu")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, BBQatHomeMenu.class);
//                        startActivity(menuIntent);
//                        finish();
//                    }
//                    else if(Constants.CurrentOrderActivity.equals("bbqProduct")){
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, BBQProductDetails.class);
//                        startActivity(menuIntent);
//                        finish();
//                    }
//                    else if (Constants.CurrentOrderActivity.equals("checkout")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, CheckoutActivity.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("ordertype")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, OrderTypeActivity.class);
//                        startActivity(menuIntent);
//                        finish();
////                    } else if (Constants.CurrentOrderActivity.equals("stores")) {
////                        Intent menuIntent = new Intent(HomeScreenActivity.this, SelectStoresActivity.class);
////                        startActivity(menuIntent);
////                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("address")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, AddressActivity.class);
//                        menuIntent.putExtra("confirm_order", true);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("confirmation")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, Confirmation.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("completed")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, OrderCompleted.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("menutype")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, MenuType.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("fav")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, FavoriteOrdersActivity.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("history")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, OrderHistoryActivity.class);
//                        startActivity(menuIntent);
//                        finish();
//                    }
//                    else {
////                        if (myDbHelper.getTotalOrderQty() > 0) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, MenuType.class);
//                        startActivity(menuIntent);
//                        finish();
////                        } else {
////                            Intent menuIntent = new Intent(HomeScreenActivity.this, OrderActivity.class);
////                            startActivity(menuIntent);
////                            finish();
////                        }
//                    }
//                }
//                else{
//                    if (Constants.CurrentOrderActivity.equals("inside")) {
//                        Intent insideIntent = new Intent(HomeScreenActivity.this, InsideProductActivity.class);
//                        insideIntent.putExtra("position", Constants.ItemPosition);
//                        insideIntent.putExtra("arraylist", Constants.menuItems);
//                        insideIntent.putExtra("screen", "inside");
//                        startActivity(insideIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("salad")) {
//                        Intent sideItemsIntent = new Intent(HomeScreenActivity.this, SaladActivity.class);
//                        sideItemsIntent.putExtra("position", Constants.ItemPosition);
//                        sideItemsIntent.putExtra("arraylist", Constants.menuItems);
//                        startActivity(sideItemsIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("product")) {
//                        Intent sideItemsIntent = new Intent(HomeScreenActivity.this, ProductActivity.class);
//                        sideItemsIntent.putExtra("id", Constants.Itemid);
//                        startActivity(sideItemsIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("menu")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, MenuActivity.class);
//                        menuIntent.putExtra("type", "main");
//                        startActivity(menuIntent);
//                        finish();
//                    }else if (Constants.CurrentOrderActivity.equals("bbqMenu")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, BBQatHomeMenu.class);
//                        startActivity(menuIntent);
//                        finish();
//                    }
//                    else if(Constants.CurrentOrderActivity.equals("bbqProduct")){
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, BBQProductDetails.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("checkout")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, CheckoutActivity.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("ordertype")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, OrderTypeActivity.class);
//                        startActivity(menuIntent);
//                        finish();
////                    } else if (Constants.CurrentOrderActivity.equals("stores")) {
////                        Intent menuIntent = new Intent(HomeScreenActivity.this, SelectStoresActivity.class);
////                        startActivity(menuIntent);
////                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("address")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, AddressActivity.class);
//                        menuIntent.putExtra("confirm_order", true);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("confirmation")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, Confirmation.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("completed")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, OrderCompleted.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("menutype")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, MenuType.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("fav")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, FavoriteOrdersActivity.class);
//                        startActivity(menuIntent);
//                        finish();
//                    } else if (Constants.CurrentOrderActivity.equals("history")) {
//                        Intent menuIntent = new Intent(HomeScreenActivity.this, OrderHistoryActivity.class);
//                        startActivity(menuIntent);
//                        finish();
//                    }  else {
//                        if (myDbHelper.getTotalOrderQty() > 0) {
////                            if (userPrefs.getString("menu", "main").equals("main")) {
//                            Intent menuIntent = new Intent(HomeScreenActivity.this, MenuType.class);
//                            startActivity(menuIntent);
//                            finish();
////                            } else if (userPrefs.getString("menu", "main").equals("bbq")) {
////                                Intent menuIntent = new Intent(HomeScreenActivity.this, BBQatHomeMenu.class);
////                                startActivity(menuIntent);
////                                finish();
////                            }
//                        } else {
//                            Intent menuIntent = new Intent(HomeScreenActivity.this, OrderActivity.class);
//                            startActivity(menuIntent);
//                            finish();
//                        }
//                    }
//                }
                break;

            case R.id.location:
                Intent intent = new Intent(HomeScreenActivity.this, SelectStoresActivity.class);
                intent.putExtra("class","main");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

                break;

            case R.id.website:
                Intent AboutIntent = new Intent(HomeScreenActivity.this, MoreWebView.class);
                AboutIntent.putExtra("class","main");
                AboutIntent.putExtra("title","Dajen");
                AboutIntent.putExtra("url","http://www.dajen-chain.com/");
                AboutIntent.putExtra("webview_toshow","main");
                startActivity(AboutIntent);
            break;
        }
    }

    public class GetMenuItems extends AsyncTask<String, Integer, String> {

        String response;
        String  networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            dialog = new MaterialDialog.Builder(ctx)
                    .title(R.string.app_name)
                    .content("Loading...")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .show();

        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONArray jA = new JSONArray(result);
                            for (int i =0; i<jA.length(); i++){
                                JSONObject jsonObject = jA.getJSONObject(i);
                                MenuItems items = new MenuItems();

                                JSONObject keyObj = jsonObject.getJSONObject("Key");
                                items.setCatId(keyObj.getString("CategoryId"));
                                items.setItemId(keyObj.getString("ItemId"));
                                items.setItemName(keyObj.getString("ItemName"));
                                items.setModifierID(keyObj.getString("ModifierId"));
                                items.setItemName_ar(keyObj.getString("ItemName_Ar"));
                                items.setDesc(keyObj.getString("Description"));
                                items.setDesc_ar(keyObj.getString("Description_Ar"));
                                items.setAdditionalId(keyObj.getString("AdditionalsId"));
                                items.setImage(keyObj.getString("Images"));

                                int count = 0;
                                JSONObject priceObj = jsonObject.getJSONObject("Value");
                                if(priceObj.has("1")){
                                    count = count+1;
                                    items.setPrice1(priceObj.getString("1"));
                                }
                                else{
                                    items.setPrice1("-1");
                                }

                                if(priceObj.has("2")){
                                    count = count+1;
                                    items.setPrice2(priceObj.getString("2"));
                                }
                                else{
                                    items.setPrice2("-1");
                                }

                                if(priceObj.has("3")){
                                    count = count+1;
                                    items.setPrice3(priceObj.getString("3"));
                                }
                                else{
                                    items.setPrice3("-1");
                                }

                                if(priceObj.has("4")){
                                    count = count+1;
                                    items.setPrice4(priceObj.getString("4"));
                                }
                                else{
                                    items.setPrice4("-1");
                                }

                                if(priceObj.has("5")){
                                    count = count+1;
                                    items.setPrice5(priceObj.getString("5"));
                                }
                                else{
                                    items.setPrice5("-1");
                                }

                                if(priceObj.has("6")){
                                    count = count+1;
                                    items.setPrice6(priceObj.getString("6"));
                                }
                                else{
                                    items.setPrice6("-1");
                                }

                                if(priceObj.has("7")){
                                    count = count+1;
                                    items.setPrice7(priceObj.getString("7"));
                                }
                                else{
                                    items.setPrice7("-1");
                                }

                                if(count>1){
                                    items.setTwoCount(true);
                                }
                                else{
                                    items.setTwoCount(false);
                                }
                                ChickenMenu.add(items);
                                Constants.menuItems.add(items);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == LOGIN_REQUEST_FAV && resultCode == RESULT_OK){
            Constants.TRACK_ID = "-1";
            Intent favIntent = new Intent(HomeScreenActivity.this,TrackOrder.class);
            favIntent.putExtra("screen","home");
            startActivity(favIntent);
            finish();
        }
        else if (resultCode == RESULT_CANCELED){
//            Toast.makeText(HomeScreenActivity.this, "Login failed..", Toast.LENGTH_SHORT).show();
        }
    }

    public class GetOrderTrackDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        //        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {

            networkStatus = NetworkUtil.getConnectivityStatusString(HomeScreenActivity.this);
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Reloading...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
//            arrayList.clear();
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(HomeScreenActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
//                        Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String failure = jo.getString("Failure");
                            } catch (JSONException jse) {

                                try {
                                    JSONArray ja = jo.getJSONArray("Received");

                                    JSONObject jo1 = ja.getJSONObject(0);
                                    orderId = jo1.getString("OrderId");
                                    OrderType = jo1.getString("OrderType");
                                    ratingFromService = jo1.getInt("Rating");
                                    OrderStatus = "Order!";

                                    RatingDetails rd = new RatingDetails();
                                    JSONArray jA = jo1.getJSONArray("Ratings");
                                    JSONObject obj = jA.getJSONObject(0);
                                    JSONArray ratingArray = obj.getJSONArray("5");
                                    ArrayList<Rating> ratingsArrayList = new ArrayList<>();
                                    for (int i = 0; i < ratingArray.length(); i++) {
                                        JSONObject object1 = ratingArray.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);

                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray1 = obj.getJSONArray("4");
                                    rd.setGivenrating("4");
                                    for (int i = 0; i < ratingArray1.length(); i++) {
                                        JSONObject object1 = ratingArray1.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray2 = obj.getJSONArray("3");
                                    rd.setGivenrating("3");
                                    for (int i = 0; i < ratingArray2.length(); i++) {
                                        JSONObject object1 = ratingArray2.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);

                                    JSONArray ratingArray3 = obj.getJSONArray("2");
                                    rd.setGivenrating("2");
                                    for (int i = 0; i < ratingArray3.length(); i++) {
                                        JSONObject object1 = ratingArray3.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray4 = obj.getJSONArray("1");
                                    rd.setGivenrating("1");
                                    for (int i = 0; i < ratingArray4.length(); i++) {
                                        JSONObject object1 = ratingArray4.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);

                                    rateArray.add(rd);
                                } catch (JSONException je) {
                                    je.printStackTrace();

                                }

                                try {
                                    JSONArray ja = jo.getJSONArray("Delivered");
//                                    JSONObject jo1 = ja.getJSONObject(0);
                                    Log.i("TAG","is deliverd "+ratingFromService);
                                    if (ratingFromService == 0) {

                                        rate_layout.setVisibility(View.VISIBLE);
                                        cancel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                rate_layout.setVisibility(View.GONE);
                                            }
                                        });

                                        ratingBar.setRating(0);
                                        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                            @Override
                                            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                if (fromUser) {
                                                    Intent intent = new Intent(HomeScreenActivity.this, RatingActivity.class);
                                                    intent.putExtra("rating", rating);
                                                    intent.putExtra("array", rateArray);
                                                    intent.putExtra("userid", userId);
                                                    intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                                    startActivity(intent);
                                                    rate_layout.setVisibility(View.GONE);
                                                }
                                            }
                                        });
                                    }
                                } catch (JSONException je) {
                                    je.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {

            }
        }

    }

    public class GetBanners extends AsyncTask<String, Integer, String> {

        String  networkStatus;
        MaterialDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {

            networkStatus = NetworkUtil.getConnectivityStatusString(HomeScreenActivity.this);
            dialog = new MaterialDialog.Builder(HomeScreenActivity.this)
                    .title(R.string.app_name)
                    .content("please wait..")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
//            arrayList.clear();
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HomeScreenActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
//                        Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject resultjo = new JSONObject(result);
                            JSONObject jo = resultjo.getJSONObject("Success");
                            JSONObject trackObj = jo.getJSONObject("TrackingDetails");
                            isFirstTime = false;

                            try {
                                String failure = jo.getString("Failure");
                            } catch (JSONException jse) {

                                try {
                                    JSONArray bannerArray = jo.getJSONArray("BannerDetails");
                                    for (int i = 0; i < bannerArray.length(); i++){
                                        JSONObject bannerObj = bannerArray.getJSONObject(i);
                                        Banners banners = new Banners();
                                        banners.setId(bannerObj.getString("Id"));
                                        banners.setImageName(bannerObj.getString("Bannername"));
                                        banners.setIsClick(bannerObj.getString("IsClick"));
                                        bannersArrayList.add(banners);
                                    }

                                    storeImage.setVisibility(View.GONE);
                                    mBannersAdapter = new HomeBannersAdapter(HomeScreenActivity.this, bannersArrayList, language,HomeScreenActivity.this);

                                    JSONArray SeasonOffersArray = jo.getJSONArray("SeasonOffers");
                                    JSONObject SeasonOffersObj = SeasonOffersArray.getJSONObject(0);
                                    String imageURL = SeasonOffersObj.getString("SeasonImage");
                                    promoCatId = SeasonOffersObj.getInt("CategoryId");
                                    Glide.with(HomeScreenActivity.this).load(Constants.IMAGE_URL+imageURL).into(promoImage);
                                    Log.d("TAG", "onPostExecute: "+Constants.IMAGE_URL+imageURL);

                                    defaultViewpager1.setAdapter(mBannersAdapter);
                                    defaultIndicator1.setViewPager(defaultViewpager1);
                                    defaultViewpager1.startAutoScroll();
                                    defaultViewpager1.setCycle(true);

                                    JSONArray ja = trackObj.getJSONArray("Received");

                                    JSONObject jo1 = ja.getJSONObject(0);
                                    orderId = jo1.getString("OrderId");
                                    OrderType = jo1.getString("OrderType");
                                    ratingFromService = jo1.getInt("Rating");
                                    OrderStatus = "Order!";

                                    RatingDetails rd = new RatingDetails();
                                    JSONArray jA = jo1.getJSONArray("Ratings");
                                    JSONObject obj = jA.getJSONObject(0);
                                    JSONArray ratingArray = obj.getJSONArray("5");
                                    ArrayList<Rating> ratingsArrayList = new ArrayList<>();
                                    for (int i = 0; i < ratingArray.length(); i++) {
                                        JSONObject object1 = ratingArray.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);

                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray1 = obj.getJSONArray("4");
                                    rd.setGivenrating("4");
                                    for (int i = 0; i < ratingArray1.length(); i++) {
                                        JSONObject object1 = ratingArray1.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray2 = obj.getJSONArray("3");
                                    rd.setGivenrating("3");
                                    for (int i = 0; i < ratingArray2.length(); i++) {
                                        JSONObject object1 = ratingArray2.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);

                                    JSONArray ratingArray3 = obj.getJSONArray("2");
                                    rd.setGivenrating("2");
                                    for (int i = 0; i < ratingArray3.length(); i++) {
                                        JSONObject object1 = ratingArray3.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray4 = obj.getJSONArray("1");
                                    rd.setGivenrating("1");
                                    for (int i = 0; i < ratingArray4.length(); i++) {
                                        JSONObject object1 = ratingArray4.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);

                                    rateArray.add(rd);
                                } catch (JSONException je) {
                                    je.printStackTrace();
                                }

                                try {
                                    JSONArray ja = trackObj.getJSONArray("Delivered");
//                                    JSONObject jo1 = ja.getJSONObject(0);
                                    Log.i("TAG","is deliverd "+ratingFromService);
                                    if (ratingFromService == 0) {

                                        rate_layout.setVisibility(View.VISIBLE);
                                        cancel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                rate_layout.setVisibility(View.GONE);
                                            }
                                        });

                                        ratingBar.setRating(0);
                                        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                            @Override
                                            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                if (fromUser) {
                                                    Intent intent = new Intent(HomeScreenActivity.this, RatingActivity.class);
                                                    intent.putExtra("rating", rating);
                                                    intent.putExtra("array", rateArray);
                                                    intent.putExtra("userid", userId);
                                                    intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                                    startActivity(intent);
                                                    rate_layout.setVisibility(View.GONE);
                                                }
                                            }
                                        });
                                    }
                                } catch (JSONException je) {
                                    je.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {

            }

            if(dialog != null){
                dialog.dismiss();
            }
        }

    }

}
