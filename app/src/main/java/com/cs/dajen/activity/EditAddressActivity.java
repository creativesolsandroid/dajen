package com.cs.dajen.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.JSONParser;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by CS on 04-08-2016.
 */
public class EditAddressActivity extends Activity {
    RelativeLayout addressHome,addressWork, addressOther;
    TextView title, homeTxt, workTxt, otherTxt;
    EditText flatNo, landmark, otherAddress, addressSelected;
    TextView updateAddress;
    RelativeLayout photoLayout;
    ImageView buildingPhoto;

    String addressType;
    String imaName;
    Boolean isImageCaptured = false;

    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int CAMERA_REQUEST = 4;
    private static final int STORAGE_REQUEST = 5;
    static final int IMAGE_REQUEST_CAMERA = 1;
    Bitmap thumbnail=null;

    HttpClient httpClient = new DefaultHttpClient();
    private DefaultHttpClient mHttpClient11;
    private String original_imagePath = null;
    String response,response1;
    MaterialDialog dialog;
    Boolean imageAvailable = false;

    String address, latitude, longitude, image, houseNo, houseName, sLandmark, id;
    ImageView home, work, other, back_btn;
    SharedPreferences userPrefs;
    String userId;
    SharedPreferences languagePrefs;
    String language;
    JSONObject parent = new JSONObject();
    String flatNumber,landMark,Otheraddress;
    AlertDialog customDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.save_address);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.save_address_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        address = getIntent().getExtras().getString("address");
        latitude = getIntent().getExtras().getString("latitude");
        longitude = getIntent().getExtras().getString("longitude");
        image = getIntent().getExtras().getString("image");
        addressType = getIntent().getExtras().getString("address_type");
        houseName = getIntent().getExtras().getString("house_name");
        houseNo = getIntent().getExtras().getString("house_no");
        sLandmark = getIntent().getExtras().getString("landmark");
        id = getIntent().getExtras().getString("id");
        updateAddress = (TextView) findViewById(R.id.save_address);
        flatNo = (EditText) findViewById(R.id.flat_number);
        landmark = (EditText) findViewById(R.id.landmark_name);
        otherAddress = (EditText) findViewById(R.id.address_name);
        addressSelected = (EditText) findViewById(R.id.address);

        addressHome = (RelativeLayout) findViewById(R.id.save_address_home_layout);
        addressWork = (RelativeLayout) findViewById(R.id.save_address_office_layout);
        addressOther = (RelativeLayout) findViewById(R.id.save_address_other_layout);
        photoLayout = (RelativeLayout) findViewById(R.id.photo_layout);

        buildingPhoto = (ImageView) findViewById(R.id.building_photo);

        home = (ImageView) findViewById(R.id.home_circle);
        work = (ImageView) findViewById(R.id.office_circle);
        other = (ImageView) findViewById(R.id.other_circle);
        back_btn = (ImageView) findViewById(R.id.address_backbtn);
        title = (TextView) findViewById(R.id.header_title);

        addressSelected.setText(address);
        flatNo.setText(houseNo);
        otherAddress.setText(houseName);
        landmark.setText(sLandmark);
        if(language.equalsIgnoreCase("En")) {
            updateAddress.setText(getResources().getString(R.string.save));
            title.setText("Edit Address");
        }
        else{
            updateAddress.setText(getResources().getString(R.string.save_ar));
            title.setText(getResources().getString(R.string.save_address_ar));
        }

        if(!image.equals(null) && !image.equals("null") &&image.length()>0){
            imageAvailable = true;
            Glide.with(EditAddressActivity.this).load(Constants.ADDRESS_IMAGE_URL+image).into(buildingPhoto);
        }

        if(addressType.equals("1")){
            home.setImageDrawable(getResources().getDrawable(R.drawable.address_selected_circle));
            work.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
            other.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
        }else if(addressType.equals("2")){
            work.setImageDrawable(getResources().getDrawable(R.drawable.address_selected_circle));
            home.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
            other.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
        }else if(addressType.equals("3")){
            other.setImageDrawable(getResources().getDrawable(R.drawable.address_selected_circle));
            work.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
            home.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
        }

        addressHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "1";
                home.setImageDrawable(getResources().getDrawable(R.drawable.address_selected_circle));
                work.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
                other.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
            }
        });

        addressWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "2";
                work.setImageDrawable(getResources().getDrawable(R.drawable.address_selected_circle));
                home.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
                other.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
            }
        });

        addressOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "3";
                other.setImageDrawable(getResources().getDrawable(R.drawable.address_selected_circle));
                work.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
                home.setImageDrawable(getResources().getDrawable(R.drawable.save_address_circle));
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EditAddressActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                if(language.equalsIgnoreCase("En")) {
                    no.setText(getResources().getString(R.string.no));
                    yes.setText(getResources().getString(R.string.yes));
                    desc.setText(getResources().getString(R.string.save_address_alert_text));
                }
                else{
                    title.setText(getResources().getString(R.string.dajen_ar));
                    no.setText(getResources().getString(R.string.no_ar));
                    yes.setText(getResources().getString(R.string.yes_ar));
                    desc.setText(getResources().getString(R.string.save_address_alert_text_ar));
                }

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                        UpdateAddress();
                    }
                });
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                });

                customDialog = dialogBuilder.create();
                customDialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth*0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });

        updateAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateAddress();
            }
        });

        photoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
//                    if (!canAccessCamera()) {
//                        requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
//                    } else
                        if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    } else {
                            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                            getIntent.setType("image/*");

                            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            pickIntent.setType("image/*");

                            Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
                            startActivityForResult(chooserIntent, IMAGE_REQUEST_CAMERA);
                    }
                }else {
                    Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    getIntent.setType("image/*");

                    Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    pickIntent.setType("image/*");

                    Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
                    startActivityForResult(chooserIntent, IMAGE_REQUEST_CAMERA);
                }
            }
        });
    }


    public class EditAddressDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(EditAddressActivity.this);
//            dialog = new MaterialDialog.Builder(EditAddressActivity.this)
//                    .title(R.string.app_name)
//                    .content("Updating address...")
//                    .progress(true, 0)
//                    .widgetColor(getResources().getColor(R.color.homecolor))
//                    .progressIndeterminateStyle(true)
//                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.EDIT_ADDRESS_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response1 = convertInputStreamToString(inputStream);
                            return response1;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response1);
                return response1;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            Log.i("TAG", "user response:" + result);

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(EditAddressActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            String s = jo.getString("Success");
                            setResult(RESULT_OK);
                            finish();
                            Toast.makeText(EditAddressActivity.this, "Address updated successfully", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    }
                }

            }else {
                Toast.makeText(EditAddressActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(EditAddressActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

//            case CAMERA_REQUEST:
//                if (canAccessCamera()) {
//                    if (!canAccessStorage()) {
//                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
//                    } else {
//                        Intent camera_intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(camera_intent, IMAGE_REQUEST_CAMERA);
//                    }
//                }
//                else {
//                    Toast.makeText(EditAddressActivity.this, "Camera permission denied, Unable to take picture", Toast.LENGTH_LONG).show();
//                }
//                break;

            case STORAGE_REQUEST:
                if (canAccessStorage()) {
//                    if (!canAccessCamera()) {
//                        requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
//                    } else {
//                        Intent camera_intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(camera_intent, IMAGE_REQUEST_CAMERA);
//                    }

                    Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    getIntent.setType("image/*");

                    Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    pickIntent.setType("image/*");

                    Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
                    startActivityForResult(chooserIntent, IMAGE_REQUEST_CAMERA);
                }
                else {
                    Toast.makeText(EditAddressActivity.this, "Storage permission denied, Unable to take picture", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case IMAGE_REQUEST_CAMERA:
                if(resultCode==RESULT_OK)
                {
                    isImageCaptured = true;
                    Uri uri = data.getData();

                    try {
                        thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    convertPixel();
                }
                break;

        }
    }

    private void convertPixel() {

        thumbnail = Bitmap.createScaledBitmap(thumbnail, 300, 300,
                false);
        thumbnail = codec(thumbnail, Bitmap.CompressFormat.JPEG, 50);

        Drawable drawable = new BitmapDrawable(getResources(),thumbnail);
        buildingPhoto.setImageDrawable(drawable);

//        new Dobackground().execute();
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    class Dobackground extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            submit();

            // data = parser.getDocument(entity);
            return null;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            if(response!=null) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jo = jsonArray.getJSONObject(0);
                    if (jo.getString("Name")!=null){
//                        new SaveAddressDetails().execute(Constants.SAVE_ADDRESS_URL + userId + "?UsrAddress=" + address + "&HouseNo=" + flatNumber + "&LandMark=" + landMark + "&AddressType=" + addressType + "&Latitude=" + latitude + "&Longitude=" + longitude + "&HouseName=" + Otheraddress + "&AddressImage=" + imaName);

                        try {
                            JSONArray mainItem = new JSONArray();

                            JSONObject mainObj = new JSONObject();
                            mainObj.put("Id",id);
                            mainObj.put("UserId", userId);
                            mainObj.put("HouseNo", flatNumber);
                            mainObj.put("LandMark", landMark);
                            mainObj.put("Address", address);
                            mainObj.put("AddressType", addressType);
                            mainObj.put("Latitude", latitude);
                            mainObj.put("Longitude", longitude);
                            mainObj.put("IsActive", true);
                            mainObj.put("HouseName", Otheraddress);
                            mainObj.put("AddressImage", imaName);

                            mainItem.put(mainObj);

                            parent.put("UserAddress", mainItem);
                            Log.i("TAG", parent.toString());
                        }catch (JSONException je){
                            je.printStackTrace();
                        }

                        new EditAddressDetails().execute(parent.toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(EditAddressActivity.this,"Image upload failed.Please try again",Toast.LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(EditAddressActivity.this,"Image upload failed.Please try again",Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    private void submit() {
        File ff=null;
        HttpResponse httpResponse = null;
        if(thumbnail!=null){
            original_imagePath=StoreByteImage(thumbnail);
        }

        HttpParams params = new BasicHttpParams();
        params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
        mHttpClient11 = new DefaultHttpClient(params);

        try{

            HttpPost httppost = new HttpPost(Constants.UPLOAD_IMAGE);

            if(original_imagePath!=null){
                ff=new File(original_imagePath);
            }

            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            if(null!=ff&& ff.exists()){
                multipartEntity.addPart("image", new FileBody(ff));
            }
            imaName = ff.getName();
//            JSONObject mainObj = new JSONObject();
//            try {
//
//                mainObj.put("UsrAddress", address);
//                mainObj.put("HouseNo", flatNumber);
//                mainObj.put("LandMark", landMark);
//                mainObj.put("AddressType", addressType);
//                mainObj.put("Latitude", latitude);
//                mainObj.put("Longitude", longitude);
//                mainObj.put("HouseName", Otheraddress);
//                mainObj.put("AddressImage", imaName);
//                Log.i("TAG", mainObj.toString());
//            } catch (JSONException je) {
//
//            }
////
//            httppost.addHeader("JsonData", mainObj.toString());

            httppost.setEntity(multipartEntity);

            httpResponse=  mHttpClient11.execute(httppost);

	      /* String responseString = EntityUtils.toString(response.getEntity());
	        Log.d("UPLOAD", "------------------------------"+responseString);*/

        }catch(Exception e){
            e.printStackTrace();
        }

        // key and value pair


        InputStream entity = null;
        try {
            entity = httpResponse.getEntity().getContent();
            if(entity != null) {
                response = convertInputStreamToString(entity);
                Log.i("TAG", "user response:" + response);
            }
        }catch(NullPointerException e)
        {
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Log.i("","-----------------response-----------");
    }

    public  String StoreByteImage( Bitmap bitmap) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String pictureName = "IMG_"+userId+"_" + timeStamp + ".jpg";
        File sdImageMainDirectory = new File("/sdcard/"+ pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

            options.inSampleSize = 5;

			/*Bitmap myImage = BitmapFactory.decodeByteArray(imageData, 0,

			imageData.length,options);*/
            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            Log.i("Register","==========naem of file================="+sdImageMainDirectory.toString());


            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        }

        return sdImageMainDirectory.getAbsolutePath();

    }

    public class SaveAddressDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        //        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(EditAddressActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(EditAddressActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try{
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            if(s.equals("1")){
                                setResult(RESULT_OK);
                                finish();
                            }
                        }catch (JSONException je){
                            setResult(RESULT_CANCELED);
                            finish();
                        }

                    }
                }

            }else {
                Toast.makeText(EditAddressActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public void UpdateAddress(){
        flatNumber = flatNo.getText().toString();
        landMark = landmark.getText().toString();
        address = addressSelected.getText().toString();
        Otheraddress = otherAddress.getText().toString();

        if(Otheraddress.length() == 0){
            otherAddress.setError("");
        }
        else if(address==null && address.length()==0){
            addressSelected.setError("");
        } else if(flatNumber.length()==0){
            flatNo.setError("");
        }else{

            dialog = new MaterialDialog.Builder(EditAddressActivity.this)
                    .title(R.string.app_name)
                    .content("Updating Address...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();

            if(isImageCaptured) {
                new Dobackground().execute();
            }
            else{
                try {
                    JSONArray mainItem = new JSONArray();

                    JSONObject mainObj = new JSONObject();
                    mainObj.put("Id",id);
                    mainObj.put("UserId", userId);
                    mainObj.put("HouseNo", flatNumber);
                    mainObj.put("LandMark", landMark);
                    mainObj.put("Address", address);
                    mainObj.put("AddressType", addressType);
                    mainObj.put("Latitude", latitude);
                    mainObj.put("Longitude", longitude);
                    mainObj.put("IsActive", true);
                    mainObj.put("HouseName", Otheraddress);
                    if(imageAvailable) {
                        mainObj.put("AddressImage", image);
                    }
                    else{
                        mainObj.put("AddressImage", "");
                    }
                    mainItem.put(mainObj);

                    parent.put("UserAddress", mainItem);
                    Log.i("TAG", parent.toString());
                }catch (JSONException je){
                    je.printStackTrace();
                }

                new EditAddressDetails().execute(parent.toString());
            }

        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EditAddressActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        if(language.equalsIgnoreCase("En")) {
            no.setText(getResources().getString(R.string.no));
            yes.setText(getResources().getString(R.string.yes));
            desc.setText(getResources().getString(R.string.save_address_alert_text));
        }
        else{
            title.setText(getResources().getString(R.string.dajen_ar));
            no.setText(getResources().getString(R.string.no_ar));
            yes.setText(getResources().getString(R.string.yes_ar));
            desc.setText(getResources().getString(R.string.save_address_alert_text_ar));
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                UpdateAddress();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
//        super.onBackPressed();
    }
 }
