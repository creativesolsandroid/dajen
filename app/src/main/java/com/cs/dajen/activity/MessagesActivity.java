package com.cs.dajen.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.dajen.Adapters.MessagesAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.Messages;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by CS on 09-07-2016.
 */
public class MessagesActivity extends AppCompatActivity {
    private ArrayList<Messages> messageList = new ArrayList<>();
    private String response12 = null;
    SwipeMenuListView messagesListView;
    TextView emptyView;
    MessagesAdapter mAdapter;
    Toolbar toolbar;
    SharedPreferences userPrefs;
    SharedPreferences countPrefs;
    SharedPreferences.Editor countPrefEditor;
    String userId;
//    TextView title;
    int readCount = 0;
    ImageView back_btn;
    private DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.message_activity);
        }
        else{
            setContentView(R.layout.message_activity_ar);
        }


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        countPrefs = getSharedPreferences("COUNT_PREFS", Context.MODE_PRIVATE);
        countPrefEditor  = countPrefs.edit();

        myDbHelper = new DataBaseHelper(getApplicationContext());

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 4;

        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar4);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar1);
        }
        Constants.CurrentMoreActivity = "messages";

//        title = (TextView) findViewById(R.id.header_title);
        emptyView = (TextView) findViewById(R.id.empty_view);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        messagesListView = (SwipeMenuListView) findViewById(R.id.messages_list);
        mAdapter = new MessagesAdapter(this, messageList, language);
        messagesListView.setAdapter(mAdapter);
        messagesListView.setEmptyView(emptyView);
//        if(language.equalsIgnoreCase("En")){
//            title.setText("Messages");
//        }else if(language.equalsIgnoreCase("Ar")){
//            title.setText("الرسائل");
//            emptyView.setText("لا يوجد رسائل لديك");
//        }

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // Create different menus depending on the view type
                switch (menu.getViewType()) {
                    case 0:
                        // create "read" item
                        SwipeMenuItem editItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        editItem.setBackground(new ColorDrawable(Color.rgb(0x25,
                                0xAE, 0x88)));
                        // set item width
                        editItem.setWidth(dp2px(110));

                        if(language.equalsIgnoreCase("En")){
                            editItem.setTitle("Mark Read");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            editItem.setTitle("قرأت");
                        }
                        // set item title fontsize
                        editItem.setTitleSize(16);
                        // set item title font color
                        editItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(editItem);

                        // create "delete" item
                        SwipeMenuItem deleteItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                                0x3F, 0x25)));
                        // set item width
                        deleteItem.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                        deleteItem.setTitle("Delete");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            deleteItem.setTitle("حذف");
                        }
                        // set item title fontsize
                        deleteItem.setTitleSize(18);
                        // set item title font color
                        deleteItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(deleteItem);
                        break;
                    case 1:
                        // create "delete" item
                        SwipeMenuItem deleteItem1 = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        deleteItem1.setBackground(new ColorDrawable(Color.rgb(0xF9,
                                0x3F, 0x25)));
                        // set item width
                        deleteItem1.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                        deleteItem1.setTitle("Delete");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            deleteItem1.setTitle("حذف");
                        }
                        // set item title fontsize
                        deleteItem1.setTitleSize(18);
                        // set item title font color
                        deleteItem1.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(deleteItem1);
                        break;
                }
            }
        };
        // set creator
        messagesListView.setMenuCreator(creator);

        // step 2. listener item click event
        messagesListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:

                        JSONObject parent = new JSONObject();
                        try {
                            JSONArray mainItem = new JSONArray();

                            JSONObject mainObj = new JSONObject();
                            mainObj.put("PID",messageList.get(position).getpId());
                            mainObj.put("IsRead", messageList.get(position).getIsRead());
                            mainObj.put("Status", "false");
                            mainItem.put(mainObj);

                            parent.put("PushNotification", mainItem);
                            Log.i("TAG", parent.toString());
                        }catch (JSONException je){
                        }
                        new UpdateNotification().execute(parent.toString());
                        break;

                    case 1:

                        JSONObject parent1 = new JSONObject();
                        try {
                            JSONArray mainItem1 = new JSONArray();

                            JSONObject mainObj1 = new JSONObject();
                            mainObj1.put("PID",messageList.get(position).getpId());
                            mainObj1.put("IsRead", messageList.get(position).getIsRead());
                            mainObj1.put("Status", "false");
                            mainItem1.put(mainObj1);

                            parent1.put("PushNotification", mainItem1);
                            Log.i("TAG", parent1.toString());
                        }catch (JSONException je){

                        }
                        new DeleteNotification().execute(parent1.toString());
                        break;
                }
                return false;
            }
        });

//        messagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if ( !messageList.get(position).equals("true")) {
//                    JSONObject parent1 = new JSONObject();
//                    try {
//                        JSONArray mainItem = new JSONArray();
//
//
//
//                        JSONObject mainObj = new JSONObject();
//                        mainObj.put("PID",messageList.get(position).getpId());
//                        mainObj.put("IsRead", "false");
//                        mainObj.put("Status", "true");
//                        mainItem.put(mainObj);
//
//
//
//                        parent1.put("PushNotification", mainItem);
//                        Log.i("TAG", parent1.toString());
//                    }catch (JSONException je){
//
//                    }
//                    new UpdateNotification().execute(parent1.toString());
//                }
//                Intent i = new Intent(MessagesActivity.this, MessageDetails.class);
//                i.putExtra("message", messageList.get(position).getPushMessage());
//                i.putExtra("date", messageList.get(position).getSentDate());
//                i.putExtra("status", messageList.get(position).getPushNotificationType());
//                startActivity(i);
//            }
//        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        new GetMessages().execute(Constants.GET_MESSAGES_URL+userId);
    }

    public class GetMessages extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MessagesActivity.this);
            dialog = new MaterialDialog.Builder(MessagesActivity.this)
                    .title(R.string.app_name)
                    .content("Loading Messages...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
            messageList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            messageList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(MessagesActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i<ja.length(); i++) {

                                    Messages msg = new Messages();
                                    JSONObject jo1 = ja.getJSONObject(i);


                                    String pId = jo1.getString("PID");
                                    String pushMessage = jo1.getString("PushMessage");
                                    String pushNotificationType = jo1.getString("PushNotificationType");
                                    String sentDate = jo1.getString("SentDate");
                                    String isRead = jo1.getString("IsRead");
                                    if(isRead.equalsIgnoreCase("false")){
                                        readCount = readCount + 1;
                                    }

                                    msg.setpId(pId);
                                    msg.setPushMessage(pushMessage);
                                    msg.setPushNotificationType(pushNotificationType);
                                    msg.setSentDate(sentDate);
                                    msg.setIsRead(isRead);

                                    messageList.add(msg);

                                }

                                countPrefEditor.putInt("count",readCount);
                            }catch (JSONException je){
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Oregano");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(MessagesActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }


    public class UpdateNotification extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String networkStatus, response;
        InputStream inputStream = null;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MessagesActivity.this);
            dialog = new MaterialDialog.Builder(MessagesActivity.this)
                    .title(R.string.app_name)
                    .content("Please wait...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.UPDATE_MESSAGE_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(MessagesActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
                        new GetMessages().execute(Constants.GET_MESSAGES_URL+userId);

                    }
                }

            }else {
                Toast.makeText(MessagesActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);

        }
    }



    public class DeleteNotification extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String response;
        double lat, longi;
        String networkStatus;
        InputStream inputStream = null;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MessagesActivity.this);
            dialog = new MaterialDialog.Builder(MessagesActivity.this)
                    .title(R.string.app_name)
                    .content("Please wait...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.UPDATE_MESSAGE_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "result: " + response);
                return response;
            }else{
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(MessagesActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
                        Log.i("TAG", "result: " + result);
                        new GetMessages().execute(Constants.GET_MESSAGES_URL+userId);
                    }
                }

            }else {
                Toast.makeText(MessagesActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }


    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }
}
