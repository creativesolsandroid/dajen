package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.R;

import java.util.List;

/**
 * Created by CS on 25-05-2017.
 */

public class MoreActivity extends Activity implements View.OnClickListener{

    RelativeLayout fb, twitter, instgram, youtube, myProfile, Location, AboutUs, ContactUs, RateOurApp;
    SharedPreferences userPrefs;
    SharedPreferences.Editor  userPrefEditor;
    String mLoginStatus;
    private static final int PROFILE_REQUEST = 1;
    private static final int ADDRESS_REQUEST = 2;
    private DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    TextView arabic, english;
    AlertDialog customDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.more_activity);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.more_activity_ar);
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 4;

        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar4);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar1);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        mLoginStatus = userPrefs.getString("login_status", "");

        fb = (RelativeLayout) findViewById(R.id.fb);
        twitter = (RelativeLayout) findViewById(R.id.twitter);
        instgram = (RelativeLayout) findViewById(R.id.instagram);
        youtube = (RelativeLayout) findViewById(R.id.youtube);
        myProfile = (RelativeLayout) findViewById(R.id.profile);
        Location = (RelativeLayout) findViewById(R.id.location);
        AboutUs = (RelativeLayout) findViewById(R.id.about_us);
        ContactUs = (RelativeLayout) findViewById(R.id.contact_us);
        RateOurApp = (RelativeLayout) findViewById(R.id.rate_our_app);

        arabic = (TextView) findViewById(R.id.arabic);
        english = (TextView) findViewById(R.id.english);

        fb.setOnClickListener(this);
        twitter.setOnClickListener(this);
        instgram.setOnClickListener(this);
        youtube.setOnClickListener(this);
        myProfile.setOnClickListener(this);
        Location.setOnClickListener(this);
        AboutUs.setOnClickListener(this);
        ContactUs.setOnClickListener(this);
        RateOurApp.setOnClickListener(this);
        arabic.setOnClickListener(this);
        english.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.profile:
                mLoginStatus = userPrefs.getString("login_status", "");
                if(mLoginStatus.equalsIgnoreCase("loggedin")){
                    Intent intent = new Intent(MoreActivity.this, MyProfile.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(MoreActivity.this, LoginActivity.class);
                    startActivityForResult(intent, PROFILE_REQUEST);
                }
                break;

            case R.id.location:
                mLoginStatus = userPrefs.getString("login_status", "");
                if(mLoginStatus.equalsIgnoreCase("loggedin")){
                    Intent i = new Intent(MoreActivity.this, AddressActivityPlacePicker.class);
                    startActivity(i);
                }else{
                    Intent intent = new Intent(MoreActivity.this, LoginActivity.class);
                    startActivityForResult(intent, ADDRESS_REQUEST);
                }
                break;

            case R.id.fb:
                Intent fbIntent = new Intent(MoreActivity.this, MoreWebView.class);
                fbIntent.putExtra("title", "Facebook");
                fbIntent.putExtra("class", "more");
                fbIntent.putExtra("webview_toshow", "more");
                fbIntent.putExtra("url","http://www.facebook.com/dajencompany");
                startActivity(fbIntent);
                break;

            case R.id.twitter:
                Intent twitterIntent = new Intent(MoreActivity.this, MoreWebView.class);
                twitterIntent.putExtra("title", "Twitter");
                twitterIntent.putExtra("class", "more");
                twitterIntent.putExtra("webview_toshow", "more");
                twitterIntent.putExtra("url","https://twitter.com/dajencompany");
                startActivity(twitterIntent);
                break;

            case R.id.instagram:
                Intent instagramIntent = new Intent(MoreActivity.this, MoreWebView.class);
                instagramIntent.putExtra("title", "Instagram");
                instagramIntent.putExtra("class", "more");
                instagramIntent.putExtra("webview_toshow", "more");
                instagramIntent.putExtra("url","https://www.instagram.com/dajencompany");
                startActivity(instagramIntent);
                break;

            case R.id.youtube:
                Intent youtubeIntent = new Intent(MoreActivity.this, MoreWebView.class);
                youtubeIntent.putExtra("title", "Youtube");
                youtubeIntent.putExtra("class", "more");
                youtubeIntent.putExtra("webview_toshow", "more");
                youtubeIntent.putExtra("url","https://www.youtube.com/user/dajencompany");
                startActivity(youtubeIntent);
                break;

            case R.id.about_us:
                startActivity(new Intent(MoreActivity.this, com.cs.dajen.activity.AboutUs.class));
                break;

            case R.id.rate_our_app:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.dajen")));
                break;

            case R.id.arabic:
                if(!language.equalsIgnoreCase("Ar")){
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();

                    FooterActivity.tabBarPosition = 1;
                    Intent intent = new Intent(MoreActivity.this, HomeScreenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;

            case R.id.english:
                if(!language.equalsIgnoreCase("En")){
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();

                    FooterActivity.tabBarPosition = 1;
                    Intent intent = new Intent(MoreActivity.this, HomeScreenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;

            case R.id.contact_us:

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("plain/text");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@dajen-chain.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Dajen Experience");
                i.putExtra(Intent.EXTRA_TITLE  , "Dajen Experience");
                final PackageManager pm = getPackageManager();
                final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                String className = null;
                for (final ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                        className = info.activityInfo.name;

                        if(className != null && !className.isEmpty()){
                            break;
                        }
                    }
                }
                i.setClassName("com.google.android.gm", className);
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(MoreActivity.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
        Constants.CurrentMoreActivity = "more";
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(MoreActivity.this,HomeScreenActivity.class));
        finish();
        FooterActivity.tabBarPosition = 1;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PROFILE_REQUEST && resultCode == RESULT_OK) {
            Intent intent = new Intent(MoreActivity.this, MyProfile.class);
            startActivity(intent);
        } else if (requestCode == ADDRESS_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Intent i = new Intent(MoreActivity.this, AddressActivity.class);
            startActivity(i);
        } else if (resultCode == RESULT_CANCELED) {
        }
    }
}
