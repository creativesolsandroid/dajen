package com.cs.dajen.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by CS on 2/12/2017.
 */

public class OrderCompleted extends AppCompatActivity {

    TextView storeAddressTextView, totalItems, totalAmount, estTime, orderNumber, paymentMode, orderType;
    TextView storeAddressTxt, store_name, store_phone, netTotal, vatAmount, amount;
    RelativeLayout mtrack_order, mcreate_new_order;
    ImageView favOrder;
    public static String Map_url, storeId, storeName, storeAddress, storePhone, storeName_ar, storeAddress_ar, total_amt, total_item, expected_time, payment_modes, order_types, order_numbers, orderId, mamount, mvatamount, mnetamount;
    private Double latitude, longitude;
    SharedPreferences userPrefs;
    String response, userResponse, userId;

    TextView vatPercent,receipt_close;
    RelativeLayout recieptLayout;
    ImageView minvoice;

    SharedPreferences orderPrefs;
    AlertDialog alertDialog;
    SharedPreferences.Editor orderPrefsEditor;
    private Timer timer = new Timer();
    SharedPreferences languagePrefs;
    String language;
    ImageView map;
    private DataBaseHelper myDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
        setContentView(R.layout.completed_activity);
        }
        else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.completed_activity_ar);
        }

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myDbHelper = new DataBaseHelper(getApplicationContext());

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        if (language.equalsIgnoreCase("En")) {
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
            FooterActivity.tabBarPosition = 2;
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
            FooterActivity.tabBarPosition = 2;
        }

        mtrack_order = (RelativeLayout) findViewById(R.id.track_order);
        mcreate_new_order = (RelativeLayout) findViewById(R.id.new_order);


        try {
        if(getIntent().getExtras().getString("storeName") != null) {
            storeName = getIntent().getExtras().getString("storeName");
            storeAddress = getIntent().getExtras().getString("storeAddress");
            storeName_ar = getIntent().getExtras().getString("storeName_ar");
            storeAddress_ar = getIntent().getExtras().getString("storeAddress_ar");
            storePhone = getIntent().getExtras().getString("storePhone");
            Map_url = getIntent().getExtras().getString("map_url");
            total_amt = getIntent().getExtras().getString("total_amt");
            total_item = getIntent().getExtras().getString("total_items");
            expected_time = getIntent().getExtras().getString("expected_time");
            payment_modes = getIntent().getExtras().getString("payment_mode");
            order_types = getIntent().getExtras().getString("order_type");
            order_numbers = getIntent().getExtras().getString("order_number");
            mamount = getIntent().getExtras().getString("amount");
            mvatamount = getIntent().getExtras().getString("vatamount");
            mnetamount = getIntent().getExtras().getString("netamount");
        }
            } catch (Exception e) {
                e.printStackTrace();
            }


        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor = orderPrefs.edit();

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userResponse = userPrefs.getString("user_profile", null);

        userId = userPrefs.getString("userId", null);
//        storeAddressTextView = (TextView) findViewById(R.id.store_address);
        totalItems = (TextView) findViewById(R.id.comp_items);
        totalAmount = (TextView) findViewById(R.id.comp_amount);
        estTime = (TextView) findViewById(R.id.comp_exp_time);
        orderNumber = (TextView) findViewById(R.id.order_number);
        paymentMode = (TextView) findViewById(R.id.comp_pay_mode);
        orderType = (TextView) findViewById(R.id.text1);
        favOrder = (ImageView) findViewById(R.id.fav_star);
        storeAddressTxt = (TextView) findViewById(R.id.conf_address);
        store_name = (TextView) findViewById(R.id.conf_name);
        store_phone = (TextView) findViewById(R.id.conf_phone);
        map = (ImageView) findViewById(R.id.confirmation_map);

        recieptLayout = (RelativeLayout) findViewById(R.id.receipt_layout);
        receipt_close = (TextView) findViewById(R.id.receipt_close);
        amount = (TextView) findViewById(R.id.amount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        vatPercent = (TextView) findViewById(R.id.vatPercent);
        netTotal = (TextView) findViewById(R.id.net_total);
        minvoice= (ImageView) findViewById(R.id.invoice);
//        mget_direction = (TextView) findViewById(R.id.get_directions);


        String[] orderNo = order_numbers.split("-");
        String[] parts = order_numbers.split(",");
        orderId = parts[0];

        if(language.equalsIgnoreCase("En")) {
            orderNumber.setText(getResources().getString(R.string.order_number) + " :#" + orderNo[1]);
        }
        else{
            orderNumber.setText(getResources().getString(R.string.order_number_ar) + " :" + orderNo[1]+"#");
        }

        orderPrefsEditor.putString("order_id", orderId);
        orderPrefsEditor.putString("order_status", "open");
        orderPrefsEditor.commit();

        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieptLayout.setVisibility(View.GONE);
            }
        });


        minvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recieptLayout.setVisibility(View.VISIBLE);
            }
        });
//        DecimalFormat decim = new DecimalFormat("0.00");
        amount.setText(mamount);
//        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
        vatAmount.setText(mvatamount);
        netTotal.setText(mnetamount);

        Glide.with(getApplicationContext()).load(Map_url).into(map);
        timer.schedule(new MyTimerTask(), 30000, 30000);


        mtrack_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                Constants.TRACK_ID = orderId;
                Intent intent = new Intent(OrderCompleted.this, TrackOrder.class);
                intent.putExtra("screen", "completed");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        mcreate_new_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                Intent intent = new Intent(OrderCompleted.this, OrderActivity.class);
                intent.putExtra("startWith", 2);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        if (language.equalsIgnoreCase("En")) {
            storeAddressTxt.setText(getString(R.string.conf_address)+" " +storeAddress);
            store_name.setText(getString(R.string.conf_name)+" " +storeName);
            store_phone.setText(getString(R.string.conf_phone)+" +"+storePhone);
        } else if (language.equalsIgnoreCase("Ar")) {
            storeAddressTxt.setText(getString(R.string.conf_address_ar)+":" +storeAddress);
            store_name.setText(getString(R.string.conf_name_ar)+" " +storeName);
            store_phone.setText(getString(R.string.conf_phone_ar)+" "+ storePhone+"+");
        }
        totalItems.setText(total_item);
        totalAmount.setText(total_amt);
        SimpleDateFormat datetime = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat time = new SimpleDateFormat("hh:mm a", Locale.US);
        Date date2 = null, time2 = null;
        try {
            time2 = datetime.parse(expected_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String date1 = date.format(time2);
        String time1 = time.format(time2);
        estTime.setText(date1+"\n"+time1);
        if (order_types.equalsIgnoreCase("Delivery")) {
            if (language.equalsIgnoreCase("En")) {
                orderType.setText(getResources().getString(R.string.delivery_address));
            } else if (language.equalsIgnoreCase("Ar")) {
                storeAddressTxt.setText(getResources().getString(R.string.delivery_address_ar));
            }
//            storeAddressTxt.setTextSize(12);
        } else {
            orderType.setText(getResources().getString(R.string.store_address));
        }
//
        if (language.equalsIgnoreCase("En")) {
            if (payment_modes.equals("2")) {
                paymentMode.setText("Cash Payment");
            } else if (payment_modes.equals("3")) {
                paymentMode.setText("Online Payment");
            } else if (payment_modes.equals("4")) {
                paymentMode.setText("Free Drink");
            }
//            orderType.setText(order_types);
        } else if (language.equalsIgnoreCase("Ar")) {
            if (payment_modes.equals("2")) {
                paymentMode.setText("نقدي عند استلام الطلب");
            } else if (payment_modes.equals("3")) {
                paymentMode.setText("الدفع أونلاين");
            } else if (payment_modes.equals("4")) {
                paymentMode.setText("مجاني");
            }
//            if (order_types.equalsIgnoreCase("Dine-In")) {
//                orderType.setText("داخل الفرع");
//            } else if (order_types.equalsIgnoreCase("Carryout")) {
//                orderType.setText("خارج الفرع");
//            } else if (order_types.equalsIgnoreCase("Delivery")) {
//                orderType.setText("توصيل");
//                mget_direction.setVisibility(View.GONE);
//            }
//        }
//        new Handler().postDelayed(new Runnable() {
//
//            /*
//             * Showing splash screen with a timer. This will be useful when you
//             * want to show case your app logo / company
//             */
//
//            @Override
//            public void run() {
//                // This method will be executed once the timer is over
//                // Start your app main activity
//                Intent intent = new Intent(OrderCompleted.this, TrackOrderSteps.class);
//                intent.putExtra("orderId", orderId);
//                startActivity(intent);
//            }
//        }, 30000);

//        if (order_types.equalsIgnoreCase("Delivery")) {
//            mget_direction.setVisibility(View.GONE);
//        }
        }

        favOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderCompleted.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.insert_fav_order_dialog;
                if(language.equalsIgnoreCase("En")){
                    layout = R.layout.insert_fav_order_dialog;
                }else if(language.equalsIgnoreCase("Ar")){
                    layout = R.layout.insert_fav_order_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);

                final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
                TextView cancelBtn = (TextView) dialogView.findViewById(R.id.no_cancel);
                final TextView saveOrder = (TextView) dialogView.findViewById(R.id.save_order);
                favName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(favName.getText().toString().length()>0){
                            saveOrder.setEnabled(true);
                        }else{
                            saveOrder.setEnabled(false);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {


                    }
                });


                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });



                saveOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new InsertFavOrder().execute(Constants.INSERT_FAVORITE_ORDER_URL+orderId+"&Fname="+favName.getText().toString().replace(" ", "%20"));
                        alertDialog.dismiss();
                    }
                });


                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

    public class InsertFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderCompleted.this);
            dialog = new MaterialDialog.Builder(OrderCompleted.this)
                    .title(R.string.app_name)
                    .content("Please wait...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderCompleted.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            String s = jo.getString("Success");
                            favOrder.setImageResource(R.drawable.star_filled);
                            favOrder.setEnabled(false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(OrderCompleted.this, "Can not reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            }else {
                Toast.makeText(OrderCompleted.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    timer.cancel();
                    Constants.TRACK_ID = orderId;
                    Intent intent = new Intent(OrderCompleted.this, TrackOrder.class);
                    intent.putExtra("screen", "completed");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        timer.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
        Constants.CurrentOrderActivity = "completed";
    }
}
