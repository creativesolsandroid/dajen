package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Constants;
import com.cs.dajen.JSONParser;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.cs.dajen.SplashScreenActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 15-06-2016.
 */
public class RegistrationActivity extends Activity {
    EditText mName, mPhoneNumber, mEmail, mPassword, mConfirmPassword, mNickname, mFamilyName;
    private String firstName, familyName, nickName, countryCode = "+966", phoneNumber, email, password, confirmPassword, gender = "Male";
    LinearLayout signUpBtn;
    ImageView showPassword,showPassword1, back_btn;
    ImageView gender_switch;
    CheckBox terms;
    TextView termsText;
    String response;
    Toolbar toolbar;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    Context context;
    AlertDialog customDialog;
    public static final String[] SMS_RECIEVER = {
            android.Manifest.permission.RECEIVE_SMS
    };
    public static final int SMS_REQUEST = 1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.register);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.register_ar);
        }
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        mName = (EditText) findViewById(R.id.register_name);
        mPhoneNumber = (EditText) findViewById(R.id.mobile_number);
        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);
        mConfirmPassword = (EditText) findViewById(R.id.confirm_password);
        mNickname = (EditText) findViewById(R.id.nickname);
        mFamilyName = (EditText) findViewById(R.id.family_name);

        showPassword = (ImageView) findViewById(R.id.show_password);
        showPassword1 = (ImageView) findViewById(R.id.show_password1);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        gender_switch = (ImageView) findViewById(R.id.gender);

//        terms = (CheckBox) findViewById(R.id.terms);
//        termsText = (TextView) findViewById(R.id.register_terms_text);
        signUpBtn = (LinearLayout) findViewById(R.id.register_button);
//        terms = (CheckBox) findViewById(R.id.terms);

//        termsText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent loginIntent = new Intent(RegistrationActivity.this, WebViewActivity.class);
//                loginIntent.putExtra("webview_toshow", "register_terms");
//                startActivity(loginIntent);
//            }
//        });
        termsText = (TextView) findViewById(R.id.register_terms_text);
        terms = (CheckBox) findViewById(R.id.terms);

        termsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(RegistrationActivity.this, MoreWebView.class);
                loginIntent.putExtra("webview_toshow", "register_terms");
                loginIntent.putExtra("class", "register");
                startActivity(loginIntent);
            }
        });

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if(!(ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED)){
                requestPermissions(SMS_RECIEVER, SMS_REQUEST);
            }
        }

        showPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD){
                    mPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                else{
                    mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                mPassword.setSelection(mPassword.length());
            }
        });
        showPassword1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mConfirmPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD){
                    mConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                else{
                    mConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                mConfirmPassword.setSelection(mConfirmPassword.length());
            }
        });

        gender_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(language.equalsIgnoreCase("En")) {
                    if (gender.equalsIgnoreCase("Male")) {
                        gender = "Female";
                        gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.female));
                    } else if (gender.equalsIgnoreCase("Female")) {
                        gender = "Male";
                        gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.male));
                    }
                }
                else{
                    if (gender.equalsIgnoreCase("Male")) {
                        gender = "Female";
                        gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.male));
                    } else if (gender.equalsIgnoreCase("Female")) {
                        gender = "Male";
                        gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.female));
                    }
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final JSONObject parent = new JSONObject();
                firstName = mName.getText().toString();
                familyName = mFamilyName.getText().toString();
                nickName = mNickname.getText().toString();
                phoneNumber = mPhoneNumber.getText().toString();
                email = mEmail.getText().toString().replaceAll(" ","");
                password = mPassword.getText().toString();
                confirmPassword = mConfirmPassword.getText().toString();
                if(firstName.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mName.setError("Please enter First Name");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mName.setError("من فضلك ارسل الاسم بالكامل");
                    }
                }else if(email.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mEmail.setError("Please enter Email");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mEmail.setError("من فضلك ادخل البريد الالكتروني");
                    }
                }else if(!isValidEmail(email)){
                    if(language.equalsIgnoreCase("En")) {
                        mEmail.setError("Please use Email format (example - abc@abc.com");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mEmail.setError("من فضلك استخدم صيغة البريد الالكتروني (example - abc@abc.com)");
                    }

                }else if(phoneNumber.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mPhoneNumber.setError("Please enter Mobile Number");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPhoneNumber.setError("من فضلك أدخل رقم الجوال");
                    }
                }else if(phoneNumber.length() != 9){
                    if(language.equalsIgnoreCase("En")) {
                        mPhoneNumber.setError("Please enter valid Mobile Number");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPhoneNumber.setError("من فضلك ادخل رقم جوال صحيح");
                    }

                }else if(password.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mPassword.setError("Please enter Password");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPassword.setError("من فضلك ادخل كلمة السر");
                    }
                }else if(password.length() < 8){
                    if(language.equalsIgnoreCase("En")) {
                        mPassword.setError("Password must be at least 8 characters");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPassword.setError("الرقم السري يجب أن يحتوي على الأقل على 8 حروف");
                    }
                }else if (!confirmPassword.equals(password)){
                    if(language.equalsIgnoreCase("En")) {
                        mConfirmPassword.setError("Passwords not match, please retype");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mConfirmPassword.setError("كلمة المرور غير صحيحة ، من فضلك أعد الإدخال ");
                    }

                }
                else if(!terms.isChecked()){
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.pls_review_tc));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.pls_review_tc_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else{
                    try {
                        JSONArray mainItem = new JSONArray();

                        JSONObject mainObj = new JSONObject();
                        mainObj.put("FullName",firstName);
                        mainObj.put("FamilyName", familyName);
                        mainObj.put("NickName", nickName);
                        mainObj.put("Email", email);
                        mainObj.put("Gender", gender);
                        mainObj.put("Mobile", "966"+phoneNumber);
                        mainObj.put("Password", password);
                        mainObj.put("DeviceToken", SplashScreenActivity.regId);
                        mainObj.put("DeviceType", "Android");
                        mainObj.put("Language", language);
                        mainObj.put("IsVerified", "true");
                        mainItem.put(mainObj);

                        parent.put("UserDetails", mainItem);
                        Log.i("TAG", parent.toString());
                    }catch (JSONException je){

                    }

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setText(getResources().getString(R.string.ok));
                    yes.setText(getResources().getString(R.string.edit));
                    desc.setText("We will be verifying the mobile number:\n\n"+countryCode+" "+ phoneNumber+"\n\nIs this OK, or would you like to edit the number?");

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            mPhoneNumber.requestFocus();
                        }
                    });
                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                                    new GetVerificationCode().execute(Constants.VERIFY_MOBILE+ "966"+phoneNumber, parent.toString());
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);

                }
            }
        });

    }

    public class GetVerificationCode extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;
        String userData;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new MaterialDialog.Builder(context)
                    .title(R.string.app_name)
                    .content("Verifying mobile number...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                Log.i("TAG",""+params[0]);
                response = jParser
                        .getJSONFromUrl(params[0]);
                userData = params[1];
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject jo1 = jo.getJSONObject("Success");
                                String otp = jo1.getString("OTP");
                                String phNo = jo1.getString("MobileNo");
//
                                Intent i = new Intent(context, VerifyOTP.class);
                                i.putExtra("OTP", otp);
                                i.putExtra("phone_number", phNo);
                                i.putExtra("user_data", userData);
                                startActivityForResult(i, 2);

                            }catch (JSONException je){
                                String msg = jo.getString("Failure");
                                je.printStackTrace();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);
                                yes.setText(getResources().getString(R.string.ok));
                                desc.setText(msg);

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }else if (resultCode == RESULT_CANCELED){
            setResult(RESULT_CANCELED);
            finish();
//            Toast.makeText(LoginActivity.this, "Registration unseccessfull", Toast.LENGTH_SHORT).show();
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

    }
}
