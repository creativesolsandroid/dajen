package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.cs.dajen.Adapters.BreadAdapter;
import com.cs.dajen.Adapters.BurgerAdapter;
import com.cs.dajen.Adapters.SauceAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.AdditionalPrices;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.Models.MenuItems;
import com.cs.dajen.Models.Modifiers;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.cs.dajen.widgets.CustomGridView;
import com.cs.dajen.widgets.CustomListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by CS on 28-04-2017.
 */

public class BBQBurgerActivity extends Activity implements View.OnClickListener{


    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    ImageView back_btn, grid_image, comment, footer_plus, footer_minus;
    TextView title, grid_title, grid_desc, grid_price;
    TextView footer_add, footer_quantity;
    public static TextView footer_price;
    int position, price_count=0;
    ArrayList<MenuItems> menuItems = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    String itemTypeId, itemTypeName;
    private ArrayList<Modifiers> SauceList = new ArrayList<>();
    private ArrayList<Modifiers> BreadsList = new ArrayList<>();
    private ArrayList<Modifiers> BurgerList = new ArrayList<>();
    public static int refreshCount;
    String screen;
    AlertDialog alertDialog, customDialog;
    Context context;
    CustomGridView sauceGrid,breadGrid;
    CustomListView burgerAdditionals;
    SauceAdapter mSauceAdapter;
    BreadAdapter mBreadAdapter;
    BurgerAdapter mBurgerAdapter;
    RelativeLayout extrasLayout;
    public static double total_price;
    public static double sauce_price;
    public static double bread_price;
    public static double price;

    public static int quantity;

    public static ArrayList<String> sauce_pos = new ArrayList<>();
    public static ArrayList<String> bread_pos = new ArrayList<>();
    public static ArrayList<String> burger_pos = new ArrayList<>();
    public static ArrayList<String> burger_id = new ArrayList<>();

    SharedPreferences languagePrefs;
    String language;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.dajen_burger_product_details);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.dajen_burger_product_details_ar);
        }

        context = this;

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        Constants.CurrentOrderActivity = "burger";
        FooterActivity.tabBarPosition = 2;

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        refreshCount = 0;
        total_price = 0.00;
        quantity = 0;
        price = 0.00;
        sauce_price = 0.00;
        bread_price = 0.00;

        try {
            sauce_pos.clear();
            bread_pos.clear();
            burger_pos.clear();
            burger_id.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        position = getIntent().getIntExtra("position",0);
        menuItems = (ArrayList<MenuItems>) getIntent().getSerializableExtra("arraylist");
        try {
            screen = getIntent().getStringExtra("screen");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Constants.ItemPosition = position;
        menuItems = Constants.menuItems;


        back_btn = (ImageView) findViewById(R.id.back_btn);
        grid_image = (ImageView) findViewById(R.id.grid_image);
        comment = (ImageView) findViewById(R.id.comment);

        title = (TextView) findViewById(R.id.title);
        grid_title = (TextView) findViewById(R.id.grid_title);
        grid_desc = (TextView) findViewById(R.id.grid_desc);
        grid_price = (TextView) findViewById(R.id.grid_price);

        footer_add = (TextView) findViewById(R.id.footer_addmore);
        footer_price = (TextView) findViewById(R.id.footer_amount);
        footer_quantity = (TextView) findViewById(R.id.footer_qty);
        footer_plus = (ImageView) findViewById(R.id.footer_plus);
        footer_minus = (ImageView) findViewById(R.id.footer_minus);

        extrasLayout = (RelativeLayout) findViewById(R.id.extra_layout);
        sauceGrid = (CustomGridView) findViewById(R.id.extras_grid);
        breadGrid = (CustomGridView) findViewById(R.id.bread_grid);
        burgerAdditionals = (CustomListView) findViewById(R.id.burger_additionals);

        Constants.COMMENTS = "";

        Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + menuItems.get(position).getImage()).into(grid_image);
        if(language.equalsIgnoreCase("En")) {
            title.setText("" + menuItems.get(position).getItemName());
            grid_title.setText("" + menuItems.get(position).getItemName());
            grid_desc.setText("" + menuItems.get(position).getDesc());
        }
        else{
            title.setText("" + menuItems.get(position).getItemName_ar());
            grid_title.setText("" + menuItems.get(position).getItemName_ar());
            grid_desc.setText("" + menuItems.get(position).getDesc_ar());
        }

        if(!menuItems.get(position).getTwoCount()) {
            grid_price.setText("" + decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice1())));
            footer_price.setText("" + decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice1())));
            price = Float.parseFloat(menuItems.get(position).getPrice1());
            quantity = 1;
        }

        if(!menuItems.get(position).getAdditionalId().equals("null")) {
            new GetAdditionals().execute(Constants.ADDITIONALS_URL + menuItems.get(position).getAdditionalId());
        }
        else{
            extrasLayout.setVisibility(View.GONE);
        }

        grid_image.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        footer_plus.setOnClickListener(this);
        footer_minus.setOnClickListener(this);
        footer_add.setOnClickListener(this);
        comment.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.back_btn:
                Intent sideItemsIntent = new Intent(BBQBurgerActivity.this,ProductActivity.class);
                sideItemsIntent.putExtra("id",Constants.Itemid);
                startActivity(sideItemsIntent);
                finish();
                break;

            case R.id.comment:
                Intent commentIntent = new Intent(BBQBurgerActivity.this, CommentActivity.class);
                if(language.equalsIgnoreCase("En")) {
                    commentIntent.putExtra("name",menuItems.get(position).getItemName());
                }else if(language.equalsIgnoreCase("Ar")){
                    commentIntent.putExtra("name",menuItems.get(position).getItemName_ar());
                }
                commentIntent.putExtra("image",menuItems.get(position).getImage());
                commentIntent.putExtra("screen","inside");
                startActivity(commentIntent);
                break;

            case R.id.footer_plus:
                quantity = quantity + 1;
                footer_quantity.setText("" + quantity);
                double footerprice = ((BBQBurgerActivity.price + BBQBurgerActivity.sauce_price + BBQBurgerActivity.bread_price) * BBQBurgerActivity.quantity);
                footer_price.setText("" + decimalFormat.format(footerprice));

                break;

            case R.id.footer_minus:
                if (quantity > 1) {
                    quantity = quantity - 1;
                }
                footer_quantity.setText(""+quantity);
                double footerprice1 = ((BBQBurgerActivity.price + BBQBurgerActivity.sauce_price + BBQBurgerActivity.bread_price)* BBQBurgerActivity.quantity);
                footer_price.setText(""+ decimalFormat.format(footerprice1));
                break;

            case R.id.footer_addmore:

                if(quantity!=0) {
                        itemTypeId = "1";
                        itemTypeName = "REGULAR";
                        if (BurgerList.size() > 0) {
                            String addl_ids = "" , addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = "";

                            if(burger_pos!=null && burger_pos.size()>0){
                                for(int i = 0; i<burger_pos.size(); i++){
                                    if(addl_ids!=null && addl_ids.length() == 0) {
                                        addl_ids = BurgerList.get(0).getChildItems().get(Integer.parseInt(burger_pos.get(i))).getAdditionalsId();
                                        addl_name = BurgerList.get(0).getChildItems().get(Integer.parseInt(burger_pos.get(i))).getAdditionalName();
                                        addl_name_ar = BurgerList.get(0).getChildItems().get(Integer.parseInt(burger_pos.get(i))).getAdditionalNameAr();
                                        addl_image = BurgerList.get(0).getChildItems().get(Integer.parseInt(burger_pos.get(i))).getImages();
                                        addl_price = BurgerList.get(0).getChildItems().get(Integer.parseInt(burger_pos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        addl_qty = "1";
                                    }
                                    else{
                                        addl_ids = addl_ids + "," + BurgerList.get(0).getChildItems().get(Integer.parseInt(burger_pos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + BurgerList.get(0).getChildItems().get(Integer.parseInt(burger_pos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + BurgerList.get(0).getChildItems().get(Integer.parseInt(burger_pos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + BurgerList.get(0).getChildItems().get(Integer.parseInt(burger_pos.get(i))).getImages();
                                        addl_price = addl_price + "," + BurgerList.get(0).getChildItems().get(Integer.parseInt(burger_pos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        addl_qty = addl_qty + ",1";
                                    }
                                }
                            }

                            if(sauce_pos!=null && sauce_pos.size()>0){
                                for(int i = 0; i<sauce_pos.size(); i++){
                                    if(addl_ids!=null && addl_ids.length() == 0) {
                                        addl_ids = SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalsId();
                                        addl_name = SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalName();
                                        addl_name_ar = SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalNameAr();
                                        addl_image = SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getImages();
                                        addl_price = SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        addl_qty = "1";
                                    }
                                    else{
                                        addl_ids = addl_ids + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getImages();
                                        addl_price = addl_price + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(sauce_pos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        addl_qty = addl_qty + ",1";
                                    }
                                }
                            }

                            if(bread_pos!=null && bread_pos.size()>0){
                                for(int i = 0; i<bread_pos.size(); i++){
                                    if(addl_ids!=null && addl_ids.length() == 0) {
                                        addl_ids = BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalsId();
                                        addl_name = BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalName();
                                        addl_name_ar = BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalNameAr();
                                        addl_image = BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getImages();
                                        addl_price = BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        addl_qty = "1";
                                    }
                                    else{
                                        addl_ids = addl_ids + "," + BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getImages();
                                        addl_price = addl_price + "," + BreadsList.get(0).getChildItems().get(Integer.parseInt(bread_pos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        addl_qty = addl_qty + ",1";
                                    }
                                }
                            }
                            double finalPrice1 = BBQBurgerActivity.price + BBQBurgerActivity.sauce_price + BBQBurgerActivity.bread_price;
                            double finalPrice = BBQBurgerActivity.price;

                            HashMap<String, String> values = new HashMap<>();
                            values.put("CategoryId", menuItems.get(position).getCatId());
                            values.put("ItemId", menuItems.get(position).getItemId());
                            values.put("ItemName", menuItems.get(position).getItemName());
                            values.put("ItemName_Ar", menuItems.get(position).getItemName_ar());
                            values.put("ItemDescription_En", menuItems.get(position).getDesc());
                            values.put("ItemDescription_Ar", menuItems.get(position).getDesc_ar());
                            values.put("ItemImage", menuItems.get(position).getImage());
                            values.put("ItemTypeId", itemTypeId);
                            values.put("TypeName", itemTypeName);
                            values.put("Price", String.valueOf((finalPrice)));
                            values.put("ModifierId", menuItems.get(position).getModifierID());
                            values.put("ModifierName", "0");
                            values.put("ModifierName_Ar", "0");
                            values.put("MdfrImage", "0");
                            values.put("AdditionalID", addl_ids);
                            values.put("AdditionalName", addl_name);
                            values.put("AdditionalName_Ar", addl_name_ar);
                            values.put("AddlImage", addl_image);
                            values.put("AddlTypeId", "0");
                            values.put("AdditionalPrice", addl_price);
                            values.put("Qty", Integer.toString(quantity));
                            values.put("TotalAmount", String.valueOf(finalPrice1 * quantity));
                            values.put("Comment", Constants.COMMENTS);
                            values.put("Custom", "0");
                            values.put("SinglePrice", addl_qty);
                            myDbHelper.insertOrder(values);

//                            if(Constants.MENU_TYPE.equals("main")) {
//                                userPrefEditor.putString("menu", "main");
//                                userPrefEditor.commit();
//                            }
//                            else if(Constants.MENU_TYPE.equals("bbq")){
                                userPrefEditor.putString("menu", "bbq");
                                userPrefEditor.commit();
//                            }

                        } else {
                            HashMap<String, String> values = new HashMap<>();
                            values.put("CategoryId", menuItems.get(position).getCatId());
                            values.put("ItemId", menuItems.get(position).getItemId());
                            values.put("ItemName", menuItems.get(position).getItemName());
                            values.put("ItemName_Ar", menuItems.get(position).getItemName_ar());
                            values.put("ItemDescription_En", menuItems.get(position).getDesc());
                            values.put("ItemDescription_Ar", menuItems.get(position).getDesc_ar());
                            values.put("ItemImage", menuItems.get(position).getImage());
                            values.put("ItemTypeId", itemTypeId);
                            values.put("TypeName", itemTypeName);
                            values.put("Price", String.valueOf(price));
                            values.put("ModifierId", menuItems.get(position).getModifierID());
                            values.put("ModifierName", "0");
                            values.put("ModifierName_Ar", "0");
                            values.put("MdfrImage", "0");
                            values.put("AdditionalID", "0");
                            values.put("AdditionalName", "0");
                            values.put("AdditionalName_Ar", "0");
                            values.put("AddlImage", "0");
                            values.put("AddlTypeId", "0");
                            values.put("AdditionalPrice", "0");
                            values.put("Qty", Integer.toString(quantity));
                            values.put("TotalAmount", String.valueOf(((price * quantity))));
                            values.put("Comment", Constants.COMMENTS);
                            values.put("Custom", "0");
                            values.put("SinglePrice", "0");
                            myDbHelper.insertOrder(values);

//                            if(Constants.MENU_TYPE.equals("main")) {
//                                userPrefEditor.putString("menu", "main");
//                                userPrefEditor.commit();
//                            }
//                            else if(Constants.MENU_TYPE.equals("bbq")){
                                userPrefEditor.putString("menu", "bbq");
                                userPrefEditor.commit();
//                            }
                        }
                        Intent intent = new Intent(BBQBurgerActivity.this, ProductActivity.class);
                        intent.putExtra("id", Constants.Itemid);
                        startActivity(intent);
                        finish();
                }
                else{
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQBurgerActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.inside_error2));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.inside_error2_ar));
                    }
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                    }
                break;

            case R.id.grid_image:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                int layout = R.layout.bigimage_popup;
//                if(language.equalsIgnoreCase("En")){
//                    layout = R.layout.bigimage_popup;
//                }else if(language.equalsIgnoreCase("Ar")){
//                    layout = R.layout.bigimage_popup;
//                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);

                ImageView img = (ImageView) dialogView.findViewById(R.id.big_image);
                ImageView cance = (ImageView) dialogView.findViewById(R.id.rating_cancel);
                cance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                Glide.with(context).load(Constants.IMAGE_URL + menuItems.get(position).getImage()).into(img);

                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = context.getResources().getDimensionPixelSize(R.dimen.popup_height);
                window.setAttributes(lp);
                break;
        }
    }

    private class GetAdditionals extends AsyncTask<String, Integer, String> {

        String response;
        String  networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
//            Constants.menuItems.clear();
            dialog = new MaterialDialog.Builder(context)
                    .title(R.string.app_name)
                    .content("Loading Additionals...")
                    .progress(true, 0)
                    .cancelable(false)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONArray ja = new JSONArray(result);
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo = ja.getJSONObject(i);
                                Modifiers mod = new Modifiers();
                                ArrayList<Additionals> additionalsList = new ArrayList<>();
                                JSONArray key = jo.getJSONArray("Key");

                                for (int j = 0; j < key.length(); j++) {


                                    JSONObject jo1 = key.getJSONObject(j);
                                    if (j == 0) {
                                        String modifierName = jo1.getString("ModifierName");
                                        String modifierName_Ar = jo1.getString("ModifierName_Ar");
                                        String modifierId = jo1.getString("ModifierId");
//                                        if(modifierId.equalsIgnoreCase("1")){
//                                            continue Outer;
//                                        }
                                        mod.setModifierName(modifierName);
                                        mod.setModifierNameAr(modifierName_Ar);
                                        mod.setModifierId(modifierId);
                                    } else {

//                                        JSONObject issueObj = jo.getJSONObject("Value");
                                        Iterator iterator = jo1.keys();
                                        while (iterator.hasNext()) {
                                            Additionals adis = new Additionals();
                                            String additonalId = (String) iterator.next();
                                            ArrayList<AdditionalPrices> additionalPrices = new ArrayList<>();
                                            JSONArray ja1 = jo1.getJSONArray(additonalId);

                                            for (int k = 0; k < ja1.length(); k++) {
                                                JSONArray additionals = ja1.getJSONArray(k);
                                                if (k == 0) {
                                                    JSONObject additionalDetails = additionals.getJSONObject(0);
                                                    String additionalName = additionalDetails.getString("AdditionalName");
                                                    String additionalName_Ar = additionalDetails.getString("AdditionalName_Ar");
                                                    String images = additionalDetails.getString("Images");
                                                    String additionalId = additionalDetails.getString("AdditionalId");
                                                    adis.setAdditionalName(additionalName);
                                                    adis.setAdditionalNameAr(additionalName_Ar);
                                                    adis.setAdditionalsId(additionalId);
                                                    adis.setImages(images);
                                                    adis.setModifierId(mod.getModifierId());
                                                } else {
                                                    for (int l = 0; l < additionals.length(); l++) {
                                                        AdditionalPrices ap = new AdditionalPrices();
                                                        JSONObject jo2 = additionals.getJSONObject(l);
                                                        ap.setItemType(jo2.getString("Type"));
                                                        ap.setPrice(jo2.getString("Price"));
                                                        additionalPrices.add(ap);
                                                    }
                                                    adis.setAdditionalPriceList(additionalPrices);
                                                }
                                            }
                                            additionalsList.add(adis);
                                        }
                                    }
                                }
                                mod.setChildItems(additionalsList);
                                if(mod.getModifierId().equals("1004")){
                                    SauceList.add(mod);
                                }
                                else if(mod.getModifierId().equals("1005")){
                                    BreadsList.add(mod);
                                }
                                else if(mod.getModifierId().equals("1008")){
                                    BurgerList.add(mod);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(SauceList.size()==0 && BreadsList.size()==0){
                    extrasLayout.setVisibility(View.GONE);
                }
                else{
                    if(SauceList.size()>0) {
                        mSauceAdapter = new SauceAdapter(getApplicationContext(), SauceList.get(0).getChildItems(), language, "burger");
                        sauceGrid.setAdapter(mSauceAdapter);
                        mSauceAdapter.notifyDataSetChanged();
                    }

                    if(BreadsList.size()>0) {
                        mBreadAdapter = new BreadAdapter(getApplicationContext(), BreadsList.get(0).getChildItems(), language, "burger");
                        breadGrid.setAdapter(mBreadAdapter);
                        mBreadAdapter.notifyDataSetChanged();
                    }
                }

                if(BurgerList.size()>0) {
                    for(int i = 0; i<BurgerList.get(0).getChildItems().size(); i++){
                        BBQBurgerActivity.burger_id.add(BurgerList.get(0).getChildItems().get(i).getAdditionalsId());
                        BBQBurgerActivity.burger_pos.add(Integer.toString(i));
                    }
                    mBurgerAdapter = new BurgerAdapter(getApplicationContext(), BurgerList.get(0).getChildItems(), language);
                    burgerAdditionals.setAdapter(mBurgerAdapter);
                    mBurgerAdapter.notifyDataSetChanged();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent sideItemsIntent = new Intent(BBQBurgerActivity.this,ProductActivity.class);
        sideItemsIntent.putExtra("id",Constants.Itemid);
        startActivity(sideItemsIntent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        refreshCount = 0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }
}
