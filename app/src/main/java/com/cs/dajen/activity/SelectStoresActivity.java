package com.cs.dajen.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Adapters.SelectStoresAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.GPSTracker;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.StoreInfo;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 15-06-2016.
 */
public class SelectStoresActivity extends Activity {

    Toolbar toolbar;
    ListView listView;
    TextView title;

    private String timeResponse = null;

    String response;
    private ArrayList<StoreInfo> storesList = new ArrayList<>();
    SelectStoresAdapter mAdapter;

    Double lat, longi;
    String serverTime;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    String mWhichClass;
    MaterialDialog dialog;
    AlertDialog customDialog;

    SharedPreferences languagePrefs;
    String language;

    String storeId, starttime, endtime, storeName, storeAddress, imagesURL, is24x7, storename_ar, storeaddress_ar;
    Double latitude, longitudes;

    private SwipeRefreshLayout swipeLayout;
    boolean loading = false;
    private DataBaseHelper myDbHelper;
    ImageView back_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.stores_activity);
        } else {
            setContentView(R.layout.stores_activity_ar);
        }


        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);

        title = (TextView) findViewById(R.id.header_title);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        listView = (ListView) findViewById(R.id.stores_listView);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);

        myDbHelper = new DataBaseHelper(this);

        mWhichClass = getIntent().getStringExtra("class");

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        } else {
            getGPSCoordinates();
        }

        if (mWhichClass != null && mWhichClass.equalsIgnoreCase("main")) {
//            title.setText("Stores");
            back_btn.setVisibility(View.INVISIBLE);
            FooterActivity.tabBarPosition = 3;
            if (language.equalsIgnoreCase("En")) {
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
            } else {
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
            }
        } else {
//            title.setText("Select Store");
            FooterActivity.tabBarPosition = 2;
            if (language.equalsIgnoreCase("En")) {
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
            } else {
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
            }
            Constants.CurrentOrderActivity = "stores";
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                if (!loading) {
                    loading = true;
                    getGPSCoordinates();
                } else {
                    swipeLayout.setRefreshing(false);
                }
            }
        });
        swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        storeName = "Dajen Khurais";
        storename_ar = "داجن خريص";
        storeAddress = "Exit 26,Khurais Road,Riyadh";
        storeaddress_ar = "مخرج 26، طريق خريص، الرياض";

        storeId = "11";

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StoreInfo si = storesList.get(position);
                if (si.getOpenFlag() != 1) {

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SelectStoresActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if (language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.store_is_closed));
                    } else {
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.store_is_closed_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);

                } else {
                    if (mWhichClass != null && mWhichClass.equalsIgnoreCase("main")) {
                        Intent intent = new Intent(SelectStoresActivity.this, StoreDetailsActivity.class);
                        if (language.equalsIgnoreCase("En")) {
                            intent.putExtra("storeName", si.getStoreName());
                            intent.putExtra("storeAddress", si.getStoreAddress());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            intent.putExtra("storeName", si.getStoreName_ar());
                            intent.putExtra("storeAddress", si.getStoreAddress_ar());
                        }
                        intent.putExtra("storeImage", si.getImageURL());
                        intent.putExtra("storeId", si.getStoreId());
                        intent.putExtra("latitude", si.getLatitude());
                        intent.putExtra("longitude", si.getLongitude());
                        intent.putExtra("lat", lat);
                        intent.putExtra("longi", longi);
                        intent.putExtra("start_time", si.getStartTime());
                        intent.putExtra("end_time", si.getEndTime());
                        intent.putExtra("is24x7", si.getIs24x7());
                        intent.putExtra("order_type", Constants.ORDER_TYPE);
                        startActivity(intent);
                    } else {
                        int distance = si.getDine_distance();
                        Intent intent = new Intent(SelectStoresActivity.this, Confirmation.class);
                        if (language.equalsIgnoreCase("En")) {
                            intent.putExtra("storeName", si.getStoreName());
                            intent.putExtra("storeAddress", si.getStoreAddress());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            intent.putExtra("storeName", si.getStoreName_ar());
                            intent.putExtra("storeAddress", si.getStoreAddress_ar());
                        }
                        intent.putExtra("storePhone", si.getStoreNumber());
                        intent.putExtra("storeImage", si.getImageURL());
                        intent.putExtra("storeId", si.getStoreId());
                        intent.putExtra("latitude", si.getLatitude());
                        intent.putExtra("longitude", si.getLongitude());
                        Log.e("TAG", "lat s" + lat);
                        intent.putExtra("lat", lat);
                        intent.putExtra("longi", longi);
                        intent.putExtra("start_time", si.getStartTime());
                        intent.putExtra("end_time", si.getEndTime());
                        intent.putExtra("full_hours", si.getIs24x7());
                        intent.putExtra("order_type", Constants.ORDER_TYPE);
                        intent.putExtra("distance", distance);

//                        if(language.equalsIgnoreCase("En")){
//                            intent.putExtra("storeName", storeName);
//                            intent.putExtra("storeAddress", storeAddress);
//                        }else if(language.equalsIgnoreCase("Ar")){
//                            intent.putExtra("storeName", storename_ar);
//                            intent.putExtra("storeAddress", storeaddress_ar);
//                        }
//                        intent.putExtra("storePhone", "966590153358");
//                        intent.putExtra("storeImage", "");
//                        intent.putExtra("storeId", storeId);
//                        intent.putExtra("latitude", 24.72747230529785);
//                        intent.putExtra("longitude", 46.78115463256836);
//                        intent.putExtra("lat", 24.6923202);
//                        intent.putExtra("longi", 46.6659896);
//                        intent.putExtra("start_time", "26-01-2018 12:30 PM");
//                        intent.putExtra("end_time", "27-01-2018 02:00 AM");
//                        intent.putExtra("full_hours", true);
//                        intent.putExtra("order_type", Constants.ORDER_TYPE);
                        startActivity(intent);
                    }
                }
            }
        });

    }

    public void getGPSCoordinates() {
        gps = new GPSTracker(SelectStoresActivity.this);
        if (gps != null) {
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
                new GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(SelectStoresActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                } else {
                    Toast.makeText(SelectStoresActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        //        MaterialDialog dialog;
        String dayOfWeek;

        @Override
        protected void onPreExecute() {
            storesList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(SelectStoresActivity.this);
//            dialog = new MaterialDialog.Builder(SelectStoresActivity.this)
//                    .title(R.string.app_name)
//                    .content("Fetching stores...")
//                    .progress(true, 0)
//                    .progressIndeterminateStyle(true)
//                    .show();
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0] + dayOfWeek);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            storesList.clear();
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        Toast.makeText(SelectStoresActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray ja = jsonObject.getJSONArray("Success");
                            for (int i = 0; i < ja.length(); i++) {
                                StoreInfo si = new StoreInfo();
                                JSONObject jo = ja.getJSONObject(i);
//                                lat = 24.70321657;
//                                longi = 46.68097073;
                                si.setStoreId(jo.getString("storeId"));
                                si.setOnlineOrderStatus(jo.getString("OnlineOrderStatus"));
                                si.setStartTime(jo.getString("ST"));
                                si.setEndTime(jo.getString("ET"));
                                si.setStoreName(jo.getString("StoreName"));
                                si.setStoreAddress(jo.getString("StoreAddress"));
                                si.setLatitude(jo.getDouble("Latitude"));
                                si.setLongitude(jo.getDouble("Longitude"));
                                si.setCountryName(jo.getString("CountryName"));
                                si.setCityName(jo.getString("CityName"));
                                si.setImageURL(jo.getString("imageURL"));
                                si.setDeliverydistance(jo.getInt("DeliveryDistance"));
                                si.setDine_distance(jo.getInt("DineInDistance"));
                                si.setFamilySection(jo.getString("FamilySection"));
                                si.setWifi(jo.getString("Wifi"));
                                si.setPatioSitting(jo.getString("PatioSitting"));
                                si.setDriveThru(jo.getString("DriveThru"));
                                si.setMeetingSpace(jo.getString("MeetingSpace"));
                                si.setHospital(jo.getString("Hospital"));
                                si.setUniversity(jo.getString("University"));
                                si.setOffice(jo.getString("Office"));
                                si.setShoppingMall(jo.getString("ShoppingMall"));
                                try {
                                    si.setAirPort(jo.getString("Airport"));
                                } catch (Exception e) {
                                    si.setAirPort("false");
                                }
                                try {
                                    si.setDineIn(jo.getString("DineIn"));
                                } catch (Exception e) {
                                    si.setDineIn("false");
                                }
                                try {
                                    si.setLadies(jo.getString("Ladies"));
                                } catch (Exception e) {
                                    si.setLadies("false");
                                }

                                si.setNeighborhood(jo.getString("Neighborhood"));
                                si.setStoreNumber(jo.getString("phone"));
                                si.setIs24x7(jo.getString("is24x7"));
                                si.setStatus(jo.getString("status"));
                                si.setOgCountry(jo.getString("OGCountry"));
                                si.setOgCity(jo.getString("OGCity"));
                                si.setStoreName_ar(jo.getString("StoreName_ar"));
                                si.setStoreAddress_ar(jo.getString("StoreAddress_ar"));
                                si.setStoreNumber(jo.getString("phone"));
                                try {
                                    si.setMessage(jo.getString("Message"));
                                } catch (Exception e) {
                                    si.setMessage("");
                                }

                                try {
                                    si.setMessage_ar(jo.getString("Message_ar"));
                                } catch (Exception e) {
                                    si.setMessage_ar("");
                                }

                                Location me = new Location("");
                                Location dest = new Location("");

                                me.setLatitude(lat);
                                me.setLongitude(longi);

                                dest.setLatitude(jo.getDouble("Latitude"));
                                dest.setLongitude(jo.getDouble("Longitude"));

                                float dist = (me.distanceTo(dest)) / 1000;
                                si.setDistance(dist);

                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                                Calendar c = Calendar.getInstance();
                                System.out.println("Current time => " + c.getTime());
                                serverTime = timeResponse;
                                String startTime = si.getStartTime();
                                String endTime = si.getEndTime();

                                if (dist <= si.getDine_distance() && (jo.getBoolean("OnlineOrderStatus"))) {

                                    if (startTime.equals("null") && endTime.equals("null")) {
                                        si.setOpenFlag(-1);
                                        storesList.add(si);
                                    } else {

                                        if (endTime.equals("00:00AM")) {
                                            si.setOpenFlag(1);
                                            storesList.add(si);

                                            continue;
                                        } else if (endTime.equals("12:00AM")) {
                                            endTime = "11:59PM";
                                        }

                                        Calendar now = Calendar.getInstance();

                                        int hour = now.get(Calendar.HOUR_OF_DAY);
                                        int minute = now.get(Calendar.MINUTE);


                                        Date serverDate = null;
                                        Date end24Date = null;
                                        Date start24Date = null;
                                        Date current24Date = null;
                                        Date dateToday = null;
                                        Calendar dateStoreClose = Calendar.getInstance();
                                        try {
                                            end24Date = dateFormat2.parse(endTime);
                                            start24Date = dateFormat2.parse(startTime);
                                            serverDate = dateFormat.parse(serverTime);
                                            dateToday = dateFormat.parse(serverTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        Date startDate = null;
                                        Date endDate = null;

                                        try {
                                            dateStoreClose.setTime(dateToday);
                                            dateStoreClose.add(Calendar.DATE, 1);
                                            String current24 = timeFormat1.format(serverDate);
                                            String end24 = timeFormat1.format(end24Date);
                                            String start24 = timeFormat1.format(start24Date);
                                            String startDateString = dateFormat1.format(dateToday);
                                            String endDateString = dateFormat1.format(dateToday);
                                            String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
                                            dateStoreClose.add(Calendar.DATE, -2);
                                            String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());


                                            try {
                                                end24Date = timeFormat1.parse(end24);
                                                start24Date = timeFormat1.parse(start24);
                                                current24Date = timeFormat1.parse(current24);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }


                                            String[] parts2 = start24.split(":");
                                            int startHour = Integer.parseInt(parts2[0]);
                                            int startMinute = Integer.parseInt(parts2[1]);

                                            String[] parts = end24.split(":");
                                            int endHour = Integer.parseInt(parts[0]);
                                            int endMinute = Integer.parseInt(parts[1]);

                                            String[] parts1 = current24.split(":");
                                            int currentHour = Integer.parseInt(parts1[0]);
                                            int currentMinute = Integer.parseInt(parts1[1]);


//                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");


                                            if (startTime.contains("AM") && endTime.contains("AM")) {
                                                if (startHour < endHour) {
                                                    startDateString = startDateString + " " + startTime;
                                                    endDateString = endDateString + "  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else if (startHour > endHour) {
                                                    if (serverTime.contains("AM")) {
                                                        if (currentHour > endHour) {
                                                            startDateString = startDateString + " " + startTime;
                                                            endDateString = endDateTomorrow + "  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        } else {
                                                            startDateString = endDateYesterday + " " + startTime;
                                                            endDateString = endDateString + "  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    } else {
                                                        startDateString = startDateString + " " + startTime;
                                                        endDateString = endDateTomorrow + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            } else if (startTime.contains("AM") && endTime.contains("PM")) {
                                                startDateString = startDateString + " " + startTime;
                                                endDateString = endDateString + "  " + endTime;
                                                try {
                                                    startDate = dateFormat2.parse(startDateString);
                                                    endDate = dateFormat2.parse(endDateString);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                            } else if (startTime.contains("PM") && endTime.contains("AM")) {
                                                if (serverTime.contains("AM")) {
                                                    if (currentHour <= endHour) {
                                                        startDateString = endDateYesterday + " " + startTime;
                                                        endDateString = endDateString + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        startDateString = startDateString + " " + startTime;
                                                        endDateString = endDateTomorrow + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } else {
                                                    startDateString = startDateString + " " + startTime;
                                                    endDateString = endDateTomorrow + "  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                            } else if (startTime.contains("PM") && endTime.contains("PM")) {
                                                startDateString = startDateString + " " + startTime;
                                                endDateString = endDateString + "  " + endTime;
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            startDate = dateFormat2.parse(si.getStartTime());
                                            endDate = dateFormat2.parse(si.getEndTime());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        String serverDateString = null;
                                        try {

                                            serverDateString = dateFormat.format(serverDate);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("TAG","serverdate " +serverDateString);

                                        try {
                                            serverDate = dateFormat.parse(serverDateString);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("TAG DATE", "" + startDate);
                                        Log.i("TAG DATE1", "" + endDate);
                                        Log.i("TAG DATE2", "" + serverDate);

                                        try {

                                            if (serverDate.after(startDate) && serverDate.before(endDate)) {
                                                Log.i("TAG Visible", "true");
                                                si.setOpenFlag(1);
                                                storesList.add(si);
                                            } else {
                                                si.setOpenFlag(0);
                                                storesList.add(si);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
//                                storesList.add(si);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Collections.sort(storesList, StoreInfo.storeDistance);
                        Collections.sort(storesList, StoreInfo.storeOpenSort);
                    }
                }

            } else

            {
                Toast.makeText(SelectStoresActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null)

            {
                dialog.dismiss();
            }
            mAdapter.notifyDataSetChanged();
            loading = false;
            swipeLayout.setRefreshing(false);

            if (storesList.size() == 0)

            {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SelectStoresActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                TextView title = (TextView) dialogView.findViewById(R.id.title);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                no.setVisibility(View.GONE);
                vert.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    title.setText(getResources().getString(R.string.dajen));
                    yes.setText(getResources().getString(R.string.ok));
                    desc.setText(getResources().getString(R.string.no_store_found));
                } else {
                    title.setText(getResources().getString(R.string.dajen_ar));
                    yes.setText(getResources().getString(R.string.ok_ar));
                    desc.setText(getResources().getString(R.string.no_store_found_ar));
                }

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                    }
                });

                customDialog = dialogBuilder.create();
                customDialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }

            super.

                    onPostExecute(result);

        }

    }


    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(SelectStoresActivity.this);
            dialog = new MaterialDialog.Builder(SelectStoresActivity.this)
                    .title(R.string.app_name)
                    .content("Fetching stores...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            } else {
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (serverTime == null) {
//                dialog.dismiss();
            } else if (serverTime.equals("no internet")) {
//                dialog.dismiss();
                Toast.makeText(SelectStoresActivity.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
//                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
//                    timeResponse = "06/02/2018 05:40 PM";
                } catch (JSONException je) {
                    je.printStackTrace();
                }
                new GetStoresInfo().execute(Constants.STORES_URL);
//                if(language.equalsIgnoreCase("En")) {

                mAdapter = new SelectStoresAdapter(SelectStoresActivity.this, storesList, lat, longi, timeResponse);
                listView.setAdapter(mAdapter);
//                }else if(language.equalsIgnoreCase("Ar")){
//                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
//                    mStoresListView.setAdapter(mStoreListAdapterArabic);
//                }

            }
            super.onPostExecute(result1);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mWhichClass != null && mWhichClass.equalsIgnoreCase("main")) {
            startActivity(new Intent(SelectStoresActivity.this, HomeScreenActivity.class));
            finish();
            FooterActivity.tabBarPosition = 1;
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }
}
