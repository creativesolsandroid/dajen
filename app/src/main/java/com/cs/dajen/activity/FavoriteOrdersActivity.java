package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Adapters.FavouriteOrderAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.FavouriteOrder;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 21-06-2016.
 */
public class FavoriteOrdersActivity extends Activity {

    private DataBaseHelper myDbHelper;
    String response;
    private ArrayList<FavouriteOrder> favoritesList = new ArrayList<>();
    GridView orderHistoryListView;
    TextView emptyView;
    private FavouriteOrderAdapter mAdapter;
    SharedPreferences userPrefs;
    String userId;
//    TextView title;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    ImageView back_btn;
    String screen;
    SharedPreferences.Editor userPrefEditor;
    AlertDialog customDialog;
    Boolean isAlertShowing = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.favourite_order);
        }
        else{
            setContentView(R.layout.favourite_order_ar);
        }
        myDbHelper = new DataBaseHelper(FavoriteOrdersActivity.this);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
//        title = (TextView) findViewById(R.id.header_title);

        screen = getIntent().getStringExtra("screen");

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        if(screen.equals("order")) {
            FooterActivity.tabBarPosition = 2;
            if(language.equalsIgnoreCase("En")){
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
            }
            else{
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
            }
            Constants.CurrentOrderActivity = "fav_order";
        }
        else{
            FooterActivity.tabBarPosition = 1;
            if(language.equalsIgnoreCase("En")){
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar1);
            }
            else{
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar4);
            }
            Constants.CurrentOrderActivity = "fav_home";
        }

        orderHistoryListView = (GridView) findViewById(R.id.checkout_list);
        emptyView = (TextView) findViewById(R.id.empty_view);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        mAdapter = new FavouriteOrderAdapter(FavoriteOrdersActivity.this, favoritesList, language);
        orderHistoryListView.setAdapter(mAdapter);
        new GetFavoriteOrderDetails().execute(Constants.GET_FAVORITE_ORDER_URL+userId);
//        if(language.equalsIgnoreCase("En")){
//            title.setText("Favorite Orders");
//        }else if(language.equalsIgnoreCase("Ar")){
//            title.setText("المفضلة");
//            emptyView.setText("لا يوجد منتجات في السلة");
//        }
        orderHistoryListView.setEmptyView(emptyView);

        orderHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                myDbHelper.deleteOrderTable();
                String orderId = favoritesList.get(position).getOrderId();
                new GetOrderDetails().execute(Constants.ORDERD_DETAILS_URL+userId+"&oId="+orderId);
            }
        });

//        orderHistoryListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(FavoriteOrdersActivity.this);
//                // ...Irrelevant code for customizing the buttons and title
//                LayoutInflater inflater = getLayoutInflater();
//                int layout = R.layout.alert_dialog;
//                View dialogView = inflater.inflate(layout, null);
//                dialogBuilder.setView(dialogView);
//                dialogBuilder.setCancelable(false);
//
//                TextView title = (TextView) dialogView.findViewById(R.id.title);
//                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//                View vert = (View) dialogView.findViewById(R.id.vert_line);
//
//                if(language.equalsIgnoreCase("En")) {
//                    title.setText(getResources().getString(R.string.dajen));
//                    yes.setText(getResources().getString(R.string.delete));
//                    yes.setText(getResources().getString(R.string.no));
//                    desc.setText(getResources().getString(R.string.fav_delete_text));
//                }
//                else{
//                    title.setText(getResources().getString(R.string.dajen_ar));
//                    yes.setText(getResources().getString(R.string.delete_ar));
//                    yes.setText(getResources().getString(R.string.no_ar));
//                    desc.setText(getResources().getString(R.string.fav_delete_text_ar));
//                }
//
//                yes.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        customDialog.dismiss();
//                    }
//                });
//
//                no.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        customDialog.dismiss();
//                    }
//                });
//
//                customDialog = dialogBuilder.create();
//                customDialog.show();
//                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                Window window = customDialog.getWindow();
//                lp.copyFrom(window.getAttributes());
//                //This makes the dialog take up the full width
//                Display display = getWindowManager().getDefaultDisplay();
//                Point size = new Point();
//                display.getSize(size);
//                int screenWidth = size.x;
//
//                double d = screenWidth*0.85;
//                lp.width = (int) d;
//                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                window.setAttributes(lp);
//
//                return true;
//            }
//        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(screen.equals("order")) {
                    Intent intent = new Intent(FavoriteOrdersActivity.this, OrderActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                else{
                    Intent intent = new Intent(FavoriteOrdersActivity.this, HomeScreenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }


    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteOrdersActivity.this);
            dialog = new MaterialDialog.Builder(FavoriteOrdersActivity.this)
                    .title(R.string.app_name)
                    .content("Please Wait...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("PUT");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    response = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(FavoriteOrdersActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

//                        try {
//                            JSONObject jo= new JSONObject(result);
//                            String s = jo.getString("Success");
                            new GetFavoriteOrderDetails().execute(Constants.GET_FAVORITE_ORDER_URL+userId);
//                            Toast.makeText(FavoriteOrdersActivity.this, "Order deleted successfully", Toast.LENGTH_SHORT).show();

//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Toast.makeText(FavoriteOrdersActivity.this, "Can not reach server", Toast.LENGTH_SHORT).show();
//                        }

                    }
                }

            }else {
                Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }
    }


    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        String menuType = "";
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteOrdersActivity.this);
            dialog = new MaterialDialog.Builder(FavoriteOrdersActivity.this)
                    .title(R.string.app_name)
                    .content("Please Wait...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            HashMap<String, String> values = new HashMap<>();

                            JSONObject jo = new JSONObject(result);

                            JSONArray jaa = jo.getJSONArray("MainItem");
                            JSONObject joo = jaa.getJSONObject(0);
                            menuType = joo.getString("MenuType");

                            JSONArray ja = jo.getJSONArray("SubItem");
                            myDbHelper.deleteOrderTable();
                            for(int i = 0; i< ja.length(); i++){
                                String ids = "0", additionalsStr = "",additionalsStrAr = "", additionalQty = "", additionalsPrice = "", itemTypeName="";
                                String categoryId = "", itemId = "", itemName = "",itemNameAr = "", itemImage = "", itemDesc = "",itemDescAr = "", itemType = "";
                                String price = "";
                                float additionPrice = 0;
                                float priceAd = 0, priceAd1 = 0, finalPrice = 0;
                                int quantity = 0;
                                JSONArray ja1 = ja.getJSONArray(i);
                                for(int j=0;j<ja1.length();j++){
                                    if(j==0){
                                        JSONObject jo1 = ja1.getJSONObject(0);
                                        itemId = jo1.getString("ItemId");
                                        itemName = jo1.getString("ItemName");
                                        itemNameAr = jo1.getString("ItemName_Ar");
                                        itemImage = jo1.getString("Images");
                                        itemDesc = jo1.getString("Description");
                                        itemDescAr = jo1.getString("Description_Ar");
                                        itemType = jo1.getString("Size");
                                        quantity = jo1.getInt("Qty");
                                        price = jo1.getString("ItemPrice");
                                        categoryId = jo1.getString("CategoryId");
                                    }else{
                                        JSONArray ja2 = ja1.getJSONArray(j);
                                        for(int k =0; k< ja2.length(); k++){
                                            JSONObject jo2 = ja2.getJSONObject(k);
                                            if(!jo2.getString("AdditionalID").equals("0")) {
                                                if (ids.equalsIgnoreCase("0")) {
                                                    ids = jo2.getString("AdditionalID");
                                                    additionalsStr = jo2.getString("AdditionalName");
                                                    additionalsStrAr = jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = jo2.getString("AdditionalPrice");
                                                    additionalQty = jo2.getString("AddQty");
                                                }else{
                                                    ids = ids +","+ jo2.getString("AdditionalID");
                                                    additionalsStr = additionalsStr +","+jo2.getString("AdditionalName");
                                                    additionalsStrAr = additionalsStrAr +","+jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = additionalsPrice + ","+jo2.getString("AdditionalPrice");
                                                    additionalQty = additionalQty + ","+jo2.getString("AddQty");
                                                }
                                                additionPrice = additionPrice + ((Float.parseFloat((jo2.getString("AdditionalPrice")))*Integer.parseInt(jo2.getString("AddQty"))));
                                            }

                                        }

                                        priceAd1 = Float.parseFloat(price) + additionPrice;
                                        priceAd = Float.parseFloat(price);
                                    }
                                }

                                if(priceAd!=0) {
                                    finalPrice = priceAd1 * quantity;
                                }
                                else{
                                    finalPrice = Float.parseFloat(price) * quantity;
                                }

                                if(itemType.equals("1")){
                                    itemTypeName = "REGULAR";
                                }
                                else if(itemType.equals("2")){
                                    itemTypeName = "SPICY";
                                }
                                else if(itemType.equals("3")){
                                    itemTypeName = "JUNIOR";
                                }
                                else if(itemType.equals("4")){
                                    itemTypeName = "REGULAR";
                                }
                                else if(itemType.equals("5")){
                                    itemTypeName = "SMALL";
                                }
                                else if(itemType.equals("6")){
                                    itemTypeName = "MEDIUM";
                                }
                                else if(itemType.equals("7")){
                                    itemTypeName = "LARGE";
                                }

                                values.put("CategoryId", categoryId);
                                values.put("ItemId", itemId);
                                values.put("ItemName", itemName);
                                values.put("ItemName_Ar", itemNameAr);
                                values.put("ItemDescription_En", itemDesc);
                                values.put("ItemDescription_Ar", itemDescAr);
                                values.put("ItemImage", itemImage);
                                values.put("ItemTypeId", itemType);
                                values.put("TypeName", itemTypeName);
                                values.put("Price", Float.toString(priceAd));
                                values.put("ModifierId","0");
                                values.put("ModifierName", "0");
                                values.put("ModifierName_Ar", "0");
                                values.put("MdfrImage", "0");
                                values.put("AdditionalID", ids);
                                values.put("AdditionalName", additionalsStr);
                                values.put("AdditionalName_Ar", additionalsStrAr);
                                values.put("AddlImage", "0");
                                values.put("AddlTypeId", "0");
                                values.put("AdditionalPrice", additionalsPrice);
                                values.put("Qty", Integer.toString(quantity));
                                values.put("TotalAmount", Float.toString(finalPrice));
                                values.put("Comment", "");
                                values.put("Custom", "0");
                                values.put("SinglePrice", additionalQty);
                            myDbHelper.insertOrder(values);

                                if(menuType.equals("1")){
                                    Constants.MENU_TYPE = "main";
                                    userPrefEditor.putString("menu", "main");
                                    userPrefEditor.commit();
                                }
                                else if(menuType.equals("2")){
                                    Constants.MENU_TYPE = "bbq";
                                    userPrefEditor.putString("menu", "bbq");
                                    userPrefEditor.commit();
                                }

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            Intent intent = new Intent(FavoriteOrdersActivity.this, CheckoutActivity.class);
            intent.putExtra("class","fav");
            startActivity(intent);

            super.onPostExecute(result);

        }

    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }



    public class GetFavoriteOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteOrdersActivity.this);
            dialog = new MaterialDialog.Builder(FavoriteOrdersActivity.this)
                    .title(R.string.app_name)
                    .content("Loading...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
            favoritesList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    JSONParser jParser = new JSONParser();

                    response = jParser
                            .getJSONFromUrl(params[0]);
                    Log.i("TAG", "user response:" + response);
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            favoritesList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i<ja.length(); i++) {

                                    FavouriteOrder fo = new FavouriteOrder();
                                    JSONObject jo1 = ja.getJSONObject(i);

                                    String orderId = jo1.getString("odrId");
                                    String favoriteName = jo1.getString("Fname");
                                    String orderDate = jo1.getString("Odate");
                                    String storeName = jo1.getString("Sname");
                                    String storeName_ar = jo1.getString("Sname_ar");
                                    String total_Price = jo1.getString("TotPrice");
                                    String itemDetails = "", itemDetailsAr = "";

                                    JSONArray ja1 = jo1.getJSONArray("items");
                                    for(int j=0;j<ja1.length();j++){
                                        JSONObject jo2 = ja1.getJSONObject(j);
                                        if(itemDetails.equalsIgnoreCase("") && itemDetailsAr.equalsIgnoreCase("")){
                                            itemDetails = jo2.getString("ItmName");
                                            itemDetailsAr = jo2.getString("ItmName_ar");
                                        }else{
                                            itemDetails = itemDetails + ", "+jo2.getString("ItmName");
                                            itemDetailsAr = itemDetailsAr+ ", "+jo2.getString("ItmName_ar");
                                        }
                                    }

                                    fo.setOrderId(orderId);
                                    fo.setFavoriteName(favoriteName);
                                    fo.setOrderDate(orderDate);
                                    fo.setStoreName(storeName);
                                    fo.setStoreName_ar(storeName_ar);
                                    fo.setTotalPrice(total_Price);
                                    fo.setItemDetails(itemDetails);
                                    fo.setItemDetails_ar(itemDetailsAr);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                    favoritesList.add(fo);

                                }
                            }catch (JSONException je){
                                je.printStackTrace();
//                                Toast.makeText(FavoriteOrdersActivity.this, "Sorry You Don't Have any Favorite Orders", Toast.LENGTH_SHORT).show();
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Oregano");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            if(favoritesList!=null && favoritesList.size()>0) {
                mAdapter.notifyDataSetChanged();
            }
            else{
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(FavoriteOrdersActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                no.setVisibility(View.GONE);
                vert.setVisibility(View.GONE);
                title.setText(getResources().getString(R.string.dajen));
                yes.setText(getResources().getString(R.string.ok));
                desc.setText("Sorry You Don't Have any Favorite Orders");

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                        isAlertShowing = false;
                        finish();
                    }
                });

                if(!isAlertShowing) {
                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    isAlertShowing = true;
                }
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth*0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
            super.onPostExecute(result);

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(screen.equals("order")) {
            Intent intent = new Intent(FavoriteOrdersActivity.this, OrderActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(FavoriteOrdersActivity.this, HomeScreenActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }
}
