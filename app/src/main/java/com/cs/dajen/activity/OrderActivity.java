package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.R;

/**
 * Created by CS on 28-04-2017.
 */

public class OrderActivity extends Activity implements View.OnClickListener{

    LinearLayout orderLayout;
    ImageView OrderImage;
    RelativeLayout cart;
    TextView cart_count;
    private DataBaseHelper myDbHelper;
    AlertDialog customDialog;
    View NewOrder, FavOrder, OrderHistory;
    FrameLayout frameLayout;
    SharedPreferences userPrefs;
    String mLoginStatus;
    private static final int LOGIN_REQUEST_HISTORY = 1;
    private static final int LOGIN_REQUEST_FAV = 2;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.order_ar);
        }

        Constants.CurrentOrderActivity = "";
        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);

        myDbHelper = new DataBaseHelper(getApplicationContext());

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");

        cart = (RelativeLayout) findViewById(R.id.cart);
        cart_count = (TextView) findViewById(R.id.cart_count);
        OrderImage = (ImageView) findViewById(R.id.order_image);
        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);
        NewOrder = (View) findViewById(R.id.new_order);
        FavOrder = (View) findViewById(R.id.fav_order);
        OrderHistory = (View) findViewById(R.id.order_history);

        ViewTreeObserver vto = frameLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    frameLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    frameLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                double width = 0.9 * frameLayout.getMeasuredWidth();
                double height = 0.9 * frameLayout.getMeasuredHeight();

                OrderImage.requestLayout();
                NewOrder.requestLayout();
                FavOrder.requestLayout();
                OrderImage.requestLayout();

                OrderImage.getLayoutParams().width = (int)width;
                OrderImage.getLayoutParams().height = (int)height;

                NewOrder.getLayoutParams().height = (int)(height/4.11);
                FavOrder.getLayoutParams().height = (int)(height/4.66);
                OrderHistory.getLayoutParams().height = (int)(height/4.66);
            }
        });

        cart.setOnClickListener(this);
        NewOrder.setOnClickListener(this);
        FavOrder.setOnClickListener(this);
        OrderHistory.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.tabBarPosition = 2;
        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
        cart_count.setText(""+myDbHelper.getTotalOrderQty());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.new_order:
                startActivity(new Intent(OrderActivity.this,MenuType.class));
                break;

            case R.id.fav_order:
                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent favIntent = new Intent(OrderActivity.this,FavoriteOrdersActivity.class);
                    favIntent.putExtra("screen","order");
                    startActivity(favIntent);
                } else {
                    Intent intent = new Intent(OrderActivity.this, LoginActivity.class);
                    startActivityForResult(intent, LOGIN_REQUEST_FAV);
                }
                break;

            case R.id.order_history:
                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    startActivity(new Intent(OrderActivity.this,OrderHistoryActivity.class));
                } else {
                    Intent intent = new Intent(OrderActivity.this, LoginActivity.class);
                    startActivityForResult(intent, LOGIN_REQUEST_HISTORY);
                }
                break;

            case R.id.cart:
                if(myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.cart_noitems));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.cart_noitems_ar));
                    }
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.cart_clear));
                        no.setText(getResources().getString(R.string.checkout_title));
                        desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.cart_clear_ar));
                        no.setText(getResources().getString(R.string.checkout_title_ar));
                        desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
                    }
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
                            cart_count.setText("" + myDbHelper.getTotalOrderQty());
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            Intent checkoutIntent = new Intent(OrderActivity.this, CheckoutActivity.class);
                            startActivity(checkoutIntent);

                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == LOGIN_REQUEST_HISTORY && resultCode == RESULT_OK){
            startActivity(new Intent(OrderActivity.this,OrderHistoryActivity.class));
        }
        else if(requestCode == LOGIN_REQUEST_FAV && resultCode == RESULT_OK){
            Intent favIntent = new Intent(OrderActivity.this,FavoriteOrdersActivity.class);
            favIntent.putExtra("screen","order");
            startActivity(favIntent);
        }
        else if (resultCode == RESULT_CANCELED){
//            Toast.makeText(OrderActivity.this, "Login unsuccessful..", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(OrderActivity.this,HomeScreenActivity.class));
        finish();
        FooterActivity.tabBarPosition = 1;
    }
}
