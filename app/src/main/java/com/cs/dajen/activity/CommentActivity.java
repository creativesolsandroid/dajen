package com.cs.dajen.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.R;

/**
 * Created by CS on 02-05-2017.
 */

public class CommentActivity extends Activity {

    ImageView image, back_btn;
    TextView title, add;
    EditText comment_et;
    String name,image_name, screen, orderId, comment;
    DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.comment);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.comment_ar);
        }

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 2;
        myDbHelper = new DataBaseHelper(getApplicationContext());
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());

        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        name = getIntent().getStringExtra("name");
        image_name = getIntent().getStringExtra("image");
        screen = getIntent().getStringExtra("screen");

        try {
            orderId = getIntent().getStringExtra("orderId");
            comment = getIntent().getStringExtra("comment");
        } catch (Exception e) {
            e.printStackTrace();
        }

        back_btn = (ImageView) findViewById(R.id.back_btn);
        add = (TextView) findViewById(R.id.comment_add);
        title = (TextView) findViewById(R.id.item_name);
        image = (ImageView) findViewById(R.id.item_image);
        comment_et = (EditText) findViewById(R.id.comment_et);

        title.setText(""+name);
        Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + image_name).into(image);

        if(screen.equalsIgnoreCase("checkout")){
            comment_et.setText(comment);
            comment_et.setSelection(comment.length());
        }
        else {
            comment_et.setText("" + Constants.COMMENTS);
            comment_et.setSelection(Constants.COMMENTS.length());
        }

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(comment_et.getText().toString().length()>0) {
                    if (screen.equalsIgnoreCase("checkout")) {
                        myDbHelper.updateCommentFromOrderID(orderId, comment_et.getText().toString());
                        finish();
                    } else {
                        Constants.COMMENTS = comment_et.getText().toString();
                        finish();
                    }
//                }
//                else{
//                    Toast.makeText(getApplicationContext(),""+getResources().getString(R.string.please_enter_comment),Toast.LENGTH_SHORT).show();
//                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
