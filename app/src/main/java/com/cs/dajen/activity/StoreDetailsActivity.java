package com.cs.dajen.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by CS on 25-06-2016.
 */
public class StoreDetailsActivity extends Activity {
    private String storeId, storeName, storeAddress, storePhoneNumber, stTime, edTime, storeImage;
    private Double latitude, longitude;
    private double lat, longi;
    private boolean is24x7;

    private DataBaseHelper myDbHelper;

    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    private String distanceResponse = null;

    LinearLayout storeCall;
    ImageView storeIcon;

    private ArrayList<String> storeHours = new ArrayList<>();
    String response;

    LinearLayout inviteFriends, rateTheStore, getDirectionsText;

    private TextView phoneNo, storeNameTextView, storeAddressTextView,
             favoriteStoreText, travelTimeText, orderNowText;

    private LinearLayout storeHoursLayout, openingHoursLayout, phoneNumberLayout;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    SharedPreferences languagePrefs;
    String language;
    TextView mondayTime, tuesdayTime, wednesdayTime, thursdayTime, fridayTime, saturdayTime, sundayTime;
    AlertDialog customDialog;
    ImageView back_btn;
    Boolean isOpen = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
           setContentView(R.layout.store_details_activity);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.store_details_activity_ar);
        }


        Constants.CurrentOrderActivity = "store details";
        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 3;
        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        storeId = getIntent().getExtras().getString("storeId");
        storeName = getIntent().getExtras().getString("storeName");
        storeImage = getIntent().getExtras().getString("storeImage");
        storeAddress = getIntent().getExtras().getString("storeAddress");
        latitude = getIntent().getExtras().getDouble("latitude");
        longitude = getIntent().getExtras().getDouble("longitude");
        lat = getIntent().getExtras().getDouble("lat");
        longi = getIntent().getExtras().getDouble("longi");
        stTime = getIntent().getExtras().getString("start_time");
        edTime = getIntent().getExtras().getString("end_time");
        is24x7 = getIntent().getExtras().getBoolean("is24x7");


        mondayTime = (TextView) findViewById(R.id.store_time_monday);
        tuesdayTime = (TextView) findViewById(R.id.store_time_tuesday);
        wednesdayTime = (TextView) findViewById(R.id.store_time_wednesday);
        thursdayTime = (TextView) findViewById(R.id.store_time_thursday);
        fridayTime = (TextView) findViewById(R.id.store_time_friday);
        saturdayTime = (TextView) findViewById(R.id.store_time_saturday);
        sundayTime = (TextView) findViewById(R.id.store_time_sunday);

        back_btn = (ImageView) findViewById(R.id.back_btn);

        phoneNo = (TextView) findViewById(R.id.phone_number);
        storeNameTextView = (TextView) findViewById(R.id.store_name);
        storeAddressTextView = (TextView) findViewById(R.id.store_address);

        travelTimeText = (TextView) findViewById(R.id.travel_time_text);

        storeCall = (LinearLayout) findViewById(R.id.call);
//        storeHoursTextView = (TextView) findViewById(R.id.store_detail_hours);
        inviteFriends = (LinearLayout) findViewById(R.id.invite_friends);
//        shareLocation = (CardView) findViewById(R.id.store_detail_share);
        rateTheStore = (LinearLayout) findViewById(R.id.rate_store);
        getDirectionsText = (LinearLayout) findViewById(R.id.get_directions);
//        travelTimeText = (TextView) findViewById(R.id.store_travel_time);
//        storeIcon = (ImageView) findViewById(R.id.store_icon);
        storeHoursLayout = (LinearLayout) findViewById(R.id.store_hours_layout);
        openingHoursLayout = (LinearLayout) findViewById(R.id.opening_hours);
//        phoneNumberLayout = (LinearLayout) findViewById(R.id.phone_number_layout);
//        orderNowText = (TextView) findViewById(R.id.ordernow_btn);

        storeNameTextView.setText(storeName);
        storeAddressTextView.setText(storeAddress);
//        storeHoursTextView.setText(stTime+" - "+edTime);
        if(!(storeImage.equals("") || storeImage.equals("null"))) {
            Glide.with(StoreDetailsActivity.this).load(Constants.IMAGE_URL + storeImage).into(storeIcon);
        }

        storeCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + storePhoneNumber));
                        startActivity(intent);
                    }
                }else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + storePhoneNumber));
                    startActivity(intent);
                }

            }
        });

        openingHoursLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isOpen) {
                    storeHoursLayout.setVisibility(View.VISIBLE);
                    isOpen = true;
                }
                else{
                    storeHoursLayout.setVisibility(View.GONE);
                    isOpen = false;
                }
            }
        });

        inviteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "I would love to have Food with you at Dajen in " + storeName +
                                ".\n http://maps.google.com/maps?saddr="+ lat + "," + longi+"&daddr=" + latitude + "," + longitude);
//                intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
//                        "");
                startActivity(Intent.createChooser(intent, "Share"));
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getDirectionsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Uri gmmIntentUri = Uri.parse("google.navigation:q=17.4474905,78.3412107");
//                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                mapIntent.setPackage("com.google.android.apps.maps");
//                startActivity(mapIntent);


                Uri gmmIntentUri = Uri.parse("google.navigation:q="+ latitude + "," + longitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);


//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr="+ lat + "," + longi+"&daddr=" + latitude + "," + longitude));
//                startActivity(intent);
            }
        });


        new getTrafficTime().execute();
        new GetStoreDetails().execute(Constants.STORE_DETAILS+storeId);
    }

    public class GetStoreDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetailsActivity.this);
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser.getJSONFromUrl(arg0[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if(result == null){

            } else if(result.equals("no internet")){
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                }
            } else {
                try {
                    JSONObject jo1 = new JSONObject(result);

                    JSONArray jsonArray = jo1.getJSONArray("Success");
                    JSONObject jo = jsonArray.getJSONObject(0);

                    storePhoneNumber = "+"+jo.getString("phone");
                    phoneNo.setText("Call +"+jo.getString("phone"));
                    if (jo.getBoolean("isSunClose")) {
                        sundayTime.setText("Closed");
                    } else {
                        sundayTime.setText(jo.getString("sunST") + " to " + jo.getString("sunET"));
                    }

                    if (jo.getBoolean("isMonClose")) {
                        mondayTime.setText("Closed");
                    } else {
                        mondayTime.setText(jo.getString("monST") + " to " + jo.getString("monET"));
                    }

                    if (jo.getBoolean("isTueClose")) {
                        tuesdayTime.setText("Closed");
                    } else {
                        tuesdayTime.setText(jo.getString("tueST") + " to " + jo.getString("tueET"));
                    }

                    if (jo.getBoolean("isWedClose")) {
                        wednesdayTime.setText("Closed");
                    } else {
                        wednesdayTime.setText(jo.getString("wedST") + " to " + jo.getString("wedET"));

                    }

                    if (jo.getBoolean("isThuClose")) {
                        thursdayTime.setText("Closed");
                    } else {
                        thursdayTime.setText(jo.getString("thuST") + " to " + jo.getString("thuET"));

                    }

                    if (jo.getBoolean("isFriClose")) {
                        fridayTime.setText("Closed");
                    } else {
                        fridayTime.setText(jo.getString("friST") + " to " + jo.getString("friET"));
                    }

                    if (jo.getBoolean("isSatClose")) {
                        saturdayTime.setText("Closed");
                    } else {
                        saturdayTime.setText(jo.getString("satST") + " to " + jo.getString("satET"));
                    }

                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }


                //detailsAdapter.notifyDataSetChanged();
                super.onPostExecute(result);
            }
        }
    }


    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetailsActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + lat +","+ longi +"&destinations="+ latitude +","+ longitude+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(StoreDetailsActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.connection_error));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.connection_error_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }else{
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
                        if(language.equalsIgnoreCase("En")) {
                            travelTimeText.setText(secs);
                        }else if(language.equalsIgnoreCase("Ar")){
                            travelTimeText.setText(secs+ "  وقت السفر" );
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            super.onPostExecute(result);

        }

    }


    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(StoreDetailsActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + storePhoneNumber));
                    startActivity(intent);
                }
                else {
                    Toast.makeText(StoreDetailsActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }
}
