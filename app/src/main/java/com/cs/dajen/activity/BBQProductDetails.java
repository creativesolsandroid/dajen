package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Adapters.BBQSauceAdapter;
import com.cs.dajen.Adapters.BBQSticksAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.AdditionalPrices;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.Models.Modifiers;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.cs.dajen.widgets.CustomGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import static com.cs.dajen.Constants.BBQMenu;
import static com.cs.dajen.Constants.BBQPosition;

/**
 * Created by CS on 06-06-2017.
 */

public class BBQProductDetails extends Activity implements View.OnClickListener{

    TextView title,boxName,boxPrice, footerAdd;
    public static TextView footerQty, footerPrice, boxSticksQty;
    public static ImageView sticksImage;
    ImageView boxImage,footerPlus, footerMinus;
    CustomGridView sticksGrid, saucesGrid;
    private ArrayList<Modifiers> SauceList = new ArrayList<>();
    private ArrayList<Modifiers> SticksList = new ArrayList<>();
    BBQSauceAdapter mSauceAdapter;
    BBQSticksAdapter mSticksAdapter;
    SharedPreferences languagePrefs;
    String language;
    public static ArrayList<String> sticktsQty = new ArrayList<>();
    public static ArrayList<String> sticksPos = new ArrayList<>();
    public static ArrayList<String> sauceQty = new ArrayList<>();
    public static ArrayList<String> saucePos = new ArrayList<>();
    public static int sticksCount = 0 , sauceCount = 0, quantity = 1, refreshCount = 0, refreshCount_sauce = 0;
    public static float price = 0, saucePrice = 0;
    ImageView back_btn;
    RelativeLayout comment;
    private DataBaseHelper myDbHelper;
    AlertDialog customDialog;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.bbq_inside_product);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.bbq_inside_product_ar);
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        Constants.CurrentOrderActivity = "bbqProduct";
        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 2;

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        if (language.equalsIgnoreCase("En")) {
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        } else {
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        sticksCount = 0;
        refreshCount = 0;
        refreshCount_sauce = 0;
        sauceCount = 0;
        saucePrice = 0;
        quantity = 1;

        try {
            sticktsQty.clear();
            sticksPos.clear();
            sauceQty.clear();
            saucePos.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        title = (TextView) findViewById(R.id.title);
        boxName = (TextView) findViewById(R.id.box_name);
        boxPrice = (TextView) findViewById(R.id.box_price);
        footerAdd = (TextView) findViewById(R.id.footer_addmore);
        boxSticksQty = (TextView) findViewById(R.id.box_sticks_qty);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        comment = (RelativeLayout) findViewById(R.id.comment_layout);

        footerQty = (TextView) findViewById(R.id.footer_qty);
        footerPrice = (TextView) findViewById(R.id.footer_amount);

        boxImage = (ImageView) findViewById(R.id.box);
        sticksImage = (ImageView) findViewById(R.id.sticks);
        footerPlus = (ImageView) findViewById(R.id.footer_plus);
        footerMinus = (ImageView) findViewById(R.id.footer_minus);

        sticksGrid = (CustomGridView) findViewById(R.id.sticks_grid);
        saucesGrid = (CustomGridView) findViewById(R.id.sauces_grid);

        Log.i("TAG","name "+Constants.BBQMenu.get(Constants.BBQPosition).getItemName());
        if(Constants.BBQMenu.get(Constants.BBQPosition).getItemName().equalsIgnoreCase("Arabian Recipe")){
            boxImage.setImageDrawable(getResources().getDrawable(R.drawable.arabian_box));
        }
        else if(Constants.BBQMenu.get(Constants.BBQPosition).getItemName().equalsIgnoreCase("Iranian Recipe")){
            boxImage.setImageDrawable(getResources().getDrawable(R.drawable.iranian_box));
        }
        else if(Constants.BBQMenu.get(Constants.BBQPosition).getItemName().equalsIgnoreCase("Indian Recipe")){
            boxImage.setImageDrawable(getResources().getDrawable(R.drawable.indian_box));
        }
        else if(Constants.BBQMenu.get(Constants.BBQPosition).getItemName().equalsIgnoreCase("Tandoori Recipe")){
            boxImage.setImageDrawable(getResources().getDrawable(R.drawable.tandoori_box));
        }
        else if(Constants.BBQMenu.get(Constants.BBQPosition).getItemName().equalsIgnoreCase("Mexican Recipe")){
            boxImage.setImageDrawable(getResources().getDrawable(R.drawable.mexican_box));
        }
        else if(Constants.BBQMenu.get(Constants.BBQPosition).getItemName().equalsIgnoreCase("Seasonal Spice Recipe")){
            boxImage.setImageDrawable(getResources().getDrawable(R.drawable.seasonal_box));
        }

        price = Float.parseFloat(Constants.BBQMenu.get(Constants.BBQPosition).getPrice1());

        Log.e("TAG", "price "+price);

//        Log.e("TAG","boxprice " +Float.toString(Float.parseFloat(Constants.decimalFormat.format(Constants.BBQMenu.get(Constants.BBQPosition).getPrice1()))));

//        String boxprice = Float.toString(Float.parseFloat(Constants.decimalFormat.format(Constants.BBQMenu.get(Constants.BBQPosition).getPrice1())));

        Log.e("TAG",""+Constants.decimalFormat.format(price));
        boxPrice.setText(Constants.decimalFormat.format(price));
//        Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + Constants.BBQMenu.get(Constants.BBQPosition).getImage()).into(boxImage);

        if (language.equalsIgnoreCase("En")) {
            title.setText(Constants.BBQMenu.get(Constants.BBQPosition).getItemName());
            boxName.setText(Constants.BBQMenu.get(Constants.BBQPosition).getItemName());
        }
        else{
            title.setText(Constants.BBQMenu.get(Constants.BBQPosition).getItemName_ar());
            boxName.setText(Constants.BBQMenu.get(Constants.BBQPosition).getItemName_ar());
        }
        new GetAdditionals().execute(Constants.ADDITIONALS_URL+Constants.BBQMenu.get(Constants.BBQPosition).getAdditionalId());

//        footerAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {

//                for(int i =0; i<SauceList.get(0).getChildItems().size(); i++){
//                    if(saucePos.contains(Integer.toString(i))){
//                        int count = 0;
//                        for(int j=0; j<sauceQty.size(); j++){
//                            if(sauceQty.get(j).equals(SauceList.get(0).getChildItems().get(i).getAdditionalName())){
//                                count = count + 1;
//                            }
//                        }
//                        if(i==0){
//                            quant1 = Integer.toString(count);
//                        }
//                        else{
//                            quant1 = quant1 + ","+Integer.toString(count);
//                        }
//                    }
//                    else{
//                        if(i==0){
//                            quant1 = "0";
//                        }
//                        else{
//                            quant1 = quant1 + ",0";
//                        }
//                    }
//                }
//            }
//        });

        Constants.COMMENTS = "";
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());

        back_btn.setOnClickListener(this);
        footerPlus.setOnClickListener(this);
        footerMinus.setOnClickListener(this);
        comment.setOnClickListener(this);
        footerAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.comment_layout:
                Intent commentIntent = new Intent(BBQProductDetails.this, CommentActivity.class);
                commentIntent.putExtra("name",Constants.BBQMenu.get(BBQPosition).getItemName());
                commentIntent.putExtra("image",Constants.BBQMenu.get(BBQPosition).getImage());
                commentIntent.putExtra("screen","inside");
                startActivity(commentIntent);
                break;

            case R.id.footer_plus:
                quantity = quantity + 1;
                footerQty.setText(""+quantity);
                footerPrice.setText(""+Constants.decimalFormat.format((BBQProductDetails.price+BBQProductDetails.saucePrice)*BBQProductDetails.quantity));
                break;

            case R.id.footer_minus:
                if(quantity>1) {
                    footerQty.setText(""+quantity);
                    quantity = quantity - 1;
                    footerPrice.setText(""+Constants.decimalFormat.format((BBQProductDetails.price + BBQProductDetails.saucePrice) * BBQProductDetails.quantity));
                }
                break;

            case R.id.back_btn:
                startActivity(new Intent(BBQProductDetails.this, BBQatHomeMenu.class));
                finish();
                break;

            case R.id.footer_addmore:
                String quant = null,quant1 = null;

//                for(int i =0; i<sticksPos.size(); i++){
//                    int count = 0;
//                    for(int j=0; j<sticktsQty.size(); j++){
//                        if(sticktsQty.get(j).equals(SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalName())){
//                            count = count + 1;
//                        }
//                    }
//                    if(i==0){
//                        quant = Integer.toString(count);
//                    }
//                    else{
//                        quant = quant + ","+Integer.toString(count);
//                    }
//                }
//
//                for(int i =0; i<saucePos.size(); i++){
//                    int count = 0;
//                    for(int j=0; j<sauceQty.size(); j++){
//                        if(sauceQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalName())){
//                            count = count + 1;
//                        }
//                    }
//                    if(i==0){
//                        quant1 = Integer.toString(count);
//                    }
//                    else{
//                        quant1 = quant1 + ","+Integer.toString(count);
//                    }
//                }

                if(sticksCount==9) {
                    String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = "0";

                    if (sticksPos != null && sticksPos.size() > 0) {
                        for (int i = 0; i < sticksPos.size(); i++) {
                            if (addl_ids != null && addl_ids.length() > 0) {

                                addl_ids = addl_ids + "," + SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalsId();
                                addl_name = addl_name + "," + SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalName();
                                addl_name_ar = addl_name_ar + "," + SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalNameAr();
                                addl_image = addl_image + "," + SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getImages();
                                addl_price = addl_price + "," + SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                int count = 0;
                                for (int j = 0; j < sticktsQty.size(); j++) {
                                    if (sticktsQty.get(j).equals(SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalName())) {
                                        count = count + 1;
                                    }
                                }
                                addl_qty = addl_qty + "," + Integer.toString(count);
                            } else {
                                addl_ids = SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalsId();
                                addl_name = SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalName();
                                addl_name_ar = SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalNameAr();
                                addl_image = SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getImages();
                                addl_price = SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                int count = 0;
                                for (int j = 0; j < sticktsQty.size(); j++) {
                                    if (sticktsQty.get(j).equals(SticksList.get(0).getChildItems().get(Integer.parseInt(sticksPos.get(i))).getAdditionalName())) {
                                        count = count + 1;
                                    }
                                }
                                addl_qty = Integer.toString(count);
                            }
                        }
                    }

                    if (saucePos != null && saucePos.size() > 0) {
                        for (int i = 0; i < saucePos.size(); i++) {
                            if (addl_ids != null && addl_ids.length() > 0) {

                                addl_ids = addl_ids + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalsId();
                                addl_name = addl_name + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalName();
                                addl_name_ar = addl_name_ar + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalNameAr();
                                addl_image = addl_image + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getImages();
                                addl_price = addl_price + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                int count = 0;
                                for (int j = 0; j < sauceQty.size(); j++) {
                                    if (sauceQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalName())) {
                                        count = count + 1;
                                    }
                                }
                                addl_qty = addl_qty + "," + Integer.toString(count);

                            } else {
                                addl_ids = SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalsId();
                                addl_name = SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalName();
                                addl_name_ar = SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalNameAr();
                                addl_image = SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getImages();
                                addl_price = SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                int count = 0;
                                for (int j = 0; j < sauceQty.size(); j++) {
                                    if (sauceQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucePos.get(i))).getAdditionalName())) {
                                        count = count + 1;
                                    }
                                }
                                addl_qty = Integer.toString(count);
                            }
                        }
                    }

                    String itemTypeId = "1";
                    String itemTypeName = "REGULAR";
                    float finalPrice1 = BBQProductDetails.price + BBQProductDetails.saucePrice;
                    float finalPrice = BBQProductDetails.price;
                    HashMap<String, String> values = new HashMap<>();
                    values.put("CategoryId", BBQMenu.get(Constants.BBQPosition).getCatId());
                    values.put("ItemId", BBQMenu.get(Constants.BBQPosition).getItemId());
                    values.put("ItemName", BBQMenu.get(Constants.BBQPosition).getItemName());
                    values.put("ItemName_Ar", BBQMenu.get(Constants.BBQPosition).getItemName_ar());
                    values.put("ItemDescription_En", BBQMenu.get(Constants.BBQPosition).getDesc());
                    values.put("ItemDescription_Ar", BBQMenu.get(Constants.BBQPosition).getDesc_ar());
                    values.put("ItemImage", BBQMenu.get(Constants.BBQPosition).getImage());
                    values.put("ItemTypeId", itemTypeId);
                    values.put("TypeName", itemTypeName);
                    values.put("Price", Float.toString(finalPrice));
                    values.put("ModifierId", BBQMenu.get(Constants.BBQPosition).getModifierID());
                    values.put("ModifierName", "0");
                    values.put("ModifierName_Ar", "0");
                    values.put("MdfrImage", "0");
                    values.put("AdditionalID", addl_ids);
                    values.put("AdditionalName", addl_name);
                    values.put("AdditionalName_Ar", addl_name_ar);
                    values.put("AddlImage", addl_image);
                    values.put("AddlTypeId", "0");
                    values.put("AdditionalPrice", addl_price);
                    values.put("Qty", Integer.toString(quantity));
                    values.put("TotalAmount", Float.toString(finalPrice1 * quantity));
                    values.put("Comment", Constants.COMMENTS);
                    values.put("Custom", "0");
                    values.put("SinglePrice", addl_qty);
                    myDbHelper.insertOrder(values);

                    userPrefEditor.putString("menu","bbq");
                    userPrefEditor.commit();

                    startActivity(new Intent(BBQProductDetails.this,BBQatHomeMenu.class));
                    finish();

//                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQProductDetails.this);
//                    // ...Irrelevant code for customizing the buttons and title
//                    LayoutInflater inflater = getLayoutInflater();
//                    int layout = R.layout.alert_dialog1;
//                    View dialogView = inflater.inflate(layout, null);
//                    dialogBuilder.setView(dialogView);
//                    dialogBuilder.setCancelable(false);
//
//                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//                    TextView title = (TextView) dialogView.findViewById(R.id.title);
//
//                    if(language.equalsIgnoreCase("En")) {
//                        title.setText(getResources().getString(R.string.dajen));
//                        yes.setText(getResources().getString(R.string.continue_with_another_box));
//                        no.setText(getResources().getString(R.string.checkout_title));
//                    }
//                    else{
//                        title.setText(getResources().getString(R.string.dajen_ar));
//                        yes.setText(getResources().getString(R.string.continue_with_another_box_ar));
//                        no.setText(getResources().getString(R.string.checkout_title_ar));
//                    }
//
//                    yes.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            startActivity(new Intent(BBQProductDetails.this, BBQatHomeMenu.class));
//                            finish();
//                            customDialog.dismiss();
//                        }
//                    });
//
//                    no.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            startActivity(new Intent(BBQProductDetails.this, CheckoutActivity.class));
//                            finish();
//                            customDialog.dismiss();
//                        }
//                    });
//
//                    customDialog = dialogBuilder.create();
//                    customDialog.show();
//                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                    Window window = customDialog.getWindow();
//                    lp.copyFrom(window.getAttributes());
//                    //This makes the dialog take up the full width
//                    Display display = getWindowManager().getDefaultDisplay();
//                    Point size = new Point();
//                    display.getSize(size);
//                    int screenWidth = size.x;
//
//                    double d1 = screenWidth*0.85;
//                    lp.width = (int) d1;
//                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                    window.setAttributes(lp);

                }
                else{
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQProductDetails.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.atleast_9));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.atleast_9_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d1 = screenWidth*0.85;
                    lp.width = (int) d1;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }

                break;
        }
    }

    private class GetAdditionals extends AsyncTask<String, Integer, String> {

        String response;
        String  networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
//            Constants.menuItems.clear();
            dialog = new MaterialDialog.Builder(BBQProductDetails.this)
                    .title(R.string.app_name)
                    .content("Loading Additionals...")
                    .progress(true, 0)
                    .cancelable(false)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONArray ja = new JSONArray(result);
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo = ja.getJSONObject(i);
                                Modifiers mod = new Modifiers();
                                ArrayList<Additionals> additionalsList = new ArrayList<>();
                                JSONArray key = jo.getJSONArray("Key");

                                for (int j = 0; j < key.length(); j++) {


                                    JSONObject jo1 = key.getJSONObject(j);
                                    if (j == 0) {
                                        String modifierName = jo1.getString("ModifierName");
                                        String modifierName_Ar = jo1.getString("ModifierName_Ar");
                                        String modifierId = jo1.getString("ModifierId");
//                                        if(modifierId.equalsIgnoreCase("1")){
//                                            continue Outer;
//                                        }
                                        mod.setModifierName(modifierName);
                                        mod.setModifierNameAr(modifierName_Ar);
                                        mod.setModifierId(modifierId);
                                    } else {

//                                        JSONObject issueObj = jo.getJSONObject("Value");
                                        Iterator iterator = jo1.keys();
                                        while (iterator.hasNext()) {
                                            Additionals adis = new Additionals();
                                            String additonalId = (String) iterator.next();
                                            ArrayList<AdditionalPrices> additionalPrices = new ArrayList<>();
                                            JSONArray ja1 = jo1.getJSONArray(additonalId);

                                            for (int k = 0; k < ja1.length(); k++) {
                                                JSONArray additionals = ja1.getJSONArray(k);
                                                if (k == 0) {
                                                    JSONObject additionalDetails = additionals.getJSONObject(0);
                                                    String additionalName = additionalDetails.getString("AdditionalName");
                                                    String additionalName_Ar = additionalDetails.getString("AdditionalName_Ar");
                                                    String images = additionalDetails.getString("Images");
                                                    String additionalId = additionalDetails.getString("AdditionalId");
                                                    adis.setAdditionalName(additionalName);
                                                    adis.setAdditionalNameAr(additionalName_Ar);
                                                    adis.setAdditionalsId(additionalId);
                                                    adis.setImages(images);
                                                    adis.setModifierId(mod.getModifierId());
                                                } else {
                                                    for (int l = 0; l < additionals.length(); l++) {
                                                        AdditionalPrices ap = new AdditionalPrices();
                                                        JSONObject jo2 = additionals.getJSONObject(l);
                                                        ap.setItemType(jo2.getString("Type"));
                                                        ap.setPrice(jo2.getString("Price"));
                                                        additionalPrices.add(ap);
                                                    }
                                                    adis.setAdditionalPriceList(additionalPrices);
                                                }
                                            }
                                            additionalsList.add(adis);
                                        }
                                    }
                                }
                                mod.setChildItems(additionalsList);
                                if(mod.getModifierId().equals("1007")){
                                    SticksList.add(mod);
                                }
                                else if(mod.getModifierId().equals("1004")){
                                    SauceList.add(mod);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                mSticksAdapter = new BBQSticksAdapter(getApplicationContext(),SticksList.get(0).getChildItems(), language, BBQProductDetails.this);
                sticksGrid.setAdapter(mSticksAdapter);
                mSticksAdapter.notifyDataSetChanged();

                mSauceAdapter = new BBQSauceAdapter(getApplicationContext(),SauceList.get(0).getChildItems(), language);
                saucesGrid.setAdapter(mSauceAdapter);
                mSauceAdapter.notifyDataSetChanged();
            }
            else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        startActivity(new Intent(BBQProductDetails.this, BBQatHomeMenu.class));
        finish();
    }
}
