package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Constants;
import com.cs.dajen.JSONParser;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.cs.dajen.SplashScreenActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 15-06-2016.
 */
public class LoginActivity extends Activity {

    private static final int REGISTRATION_REQUEST = 1;
    private static final int VERIFICATION_REQUEST = 2;
    private EditText mEmail, mPassword;
    LinearLayout loginBtn;
    TextView signUp, forgotPwd;
    Toolbar toolbar;
    String response;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    Context context;
    AlertDialog customDialog;
    ImageView showPassword;
    ImageView back_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.login);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.login_ar);
        }
        context = this;
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        showPassword = (ImageView) findViewById(R.id.show_password);
        mEmail = (EditText) findViewById(R.id.mobile_number);
        mPassword = (EditText) findViewById(R.id.password);
        signUp = (TextView) findViewById(R.id.signup_text);
        forgotPwd = (TextView) findViewById(R.id.forgot_password);
        loginBtn = (LinearLayout) findViewById(R.id.login_button);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        showPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD){
                    mPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                else{
                    mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                mPassword.setSelection(mPassword.length());
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();
                if (email.length() == 0) {
                    if(language.equalsIgnoreCase("En")) {
                        mEmail.setError(getResources().getString(R.string.pls_enter_mobile_num));
                    }else if(language.equalsIgnoreCase("Ar")){
                        mEmail.setError(getResources().getString(R.string.pls_enter_mobile_num_ar));
                    }
                    mEmail.requestFocus();
                }else if(mEmail.length() != 9){
                    if(language.equalsIgnoreCase("En")) {
                    mEmail.setError(getResources().getString(R.string.pls_enter_valid_mobile_num));
                    }else if(language.equalsIgnoreCase("Ar")){
                        mEmail.setError(getResources().getString(R.string.pls_enter_valid_mobile_num_ar));
                    }

                }else if (password.length() == 0) {
                    if(language.equalsIgnoreCase("En")) {
                        mPassword.setError(getResources().getString(R.string.pls_enter_pwd));
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPassword.setError(getResources().getString(R.string.pls_enter_pwd_ar));
                    }
                    mPassword.requestFocus();
                } else {
                    new CheckLoginDetails().execute(Constants.LOGIN_URL+"966"+email+"?psw="+password+"&dtoken="+ SplashScreenActivity.regId+"&lan="+ language);
                }
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivityForResult(intent, REGISTRATION_REQUEST);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REGISTRATION_REQUEST && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }else if(requestCode == VERIFICATION_REQUEST && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }else if (resultCode == RESULT_CANCELED){
//            finish();
//            Toast.makeText(LoginActivity.this, "Registration unseccessfull", Toast.LENGTH_SHORT).show();
        }
    }

    public class CheckLoginDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
            dialog = new MaterialDialog.Builder(context)
                    .title(R.string.app_name)
                    .content("Checking Login...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject jo1 = jo.getJSONObject("Success");
//                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                String gender = jo1.getString("Gender");
                                String nickname = jo1.getString("NickName");
                                String familyName = jo1.getString("FamilyName");
                                boolean isVerified = jo1.getBoolean("IsVerified");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("gender", gender);
                                    jsonObject.put("NickName", nickname);
                                    jsonObject.put("FamilyName", familyName);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.putString("login_status","loggedin");
                                    userPrefEditor.commit();
//                                        Intent loginIntent = new Intent();
                                    setResult(RESULT_OK);
                                    finish();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je){
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);
                                yes.setText(getResources().getString(R.string.ok));
                                desc.setText(getResources().getString(R.string.invalid_mobile));

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }else {
                Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }
}
