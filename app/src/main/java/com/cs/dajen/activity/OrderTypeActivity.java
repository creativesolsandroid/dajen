package com.cs.dajen.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.GPSTracker;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.StoreInfo;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by CS on 11-05-2017.
 */

public class OrderTypeActivity extends Activity implements View.OnClickListener {

    private DataBaseHelper myDbHelper;
    TextView cart_count;
    FrameLayout cart;
    AlertDialog customDialog;
    ImageView back_btn;
    RelativeLayout Delivery;
    LinearLayout Pickup, Dinein;
    LinearLayout Pickup1, Delivery1;
    View view;
    SharedPreferences userPrefs;
    String mLoginStatus;
    private static final int LOGIN_REQUEST = 1;
    private static final int DELIVERY_REQUEST = 2;
    private static final int DINEIN_REQUEST = 3;
    private static final int TAKEAWAY_REQUEST = 4;
    RelativeLayout menu1, menu2;

    MaterialDialog dialog;

    SharedPreferences languagePrefs;
    String language;

    private String timeResponse = null;
    String serverTime;
    Double lat, longi;
    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;

    private ArrayList<StoreInfo> storesList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order_type);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.order_type_ar);
        }

        Constants.CurrentOrderActivity = "ordertype";
        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 2;

        if (language.equalsIgnoreCase("En")) {
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        } else {
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");

        myDbHelper = new DataBaseHelper(getApplicationContext());

        cart = (FrameLayout) findViewById(R.id.cart);
        cart_count = (TextView) findViewById(R.id.cart_count);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        menu1 = (RelativeLayout) findViewById(R.id.menu1);
        menu2 = (RelativeLayout) findViewById(R.id.menu2);


        Delivery = (RelativeLayout) findViewById(R.id.type_delivery);
        Pickup = (LinearLayout) findViewById(R.id.type_pickup);
        Dinein = (LinearLayout) findViewById(R.id.type_dinein);

        Delivery1 = (LinearLayout) findViewById(R.id.type_delivery1);
        Pickup1 = (LinearLayout) findViewById(R.id.type_dinein1);
        view = (View) findViewById(R.id.view1);

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        } else {
            getGPSCoordinates();
        }


        if (userPrefs.getString("menu", "").equals("bbq")) {
            menu1.setVisibility(View.GONE);
            menu2.setVisibility(View.VISIBLE);
        } else {
            menu1.setVisibility(View.VISIBLE);
            menu2.setVisibility(View.GONE);
        }

        cart.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        Delivery.setOnClickListener(this);
        Pickup.setOnClickListener(this);
        Dinein.setOnClickListener(this);
        Delivery1.setOnClickListener(this);
        Pickup1.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                startActivity(new Intent(OrderTypeActivity.this, CheckoutActivity.class));
                finish();
                break;

            case R.id.type_delivery1:
                if (myDbHelper.getTotalOrderPrice() >= 50) {
                    mLoginStatus = userPrefs.getString("login_status", "");
                    Constants.ORDER_TYPE = "Delivery";
                    if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                        Intent intent = new Intent(OrderTypeActivity.this, AddressActivity.class);
                        intent.putExtra("confirm_order", true);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(OrderTypeActivity.this, LoginActivity.class);
                        startActivityForResult(intent, DELIVERY_REQUEST);
                    }
                } else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderTypeActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if (language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        no.setText(getResources().getString(R.string.no));
                        yes.setText(getResources().getString(R.string.yes));
                        desc.setText(getResources().getString(R.string.min_50));
                    } else {
                        title.setText(getResources().getString(R.string.dajen_ar));
                        no.setText(getResources().getString(R.string.no_ar));
                        yes.setText(getResources().getString(R.string.yes_ar));
                        desc.setText(getResources().getString(R.string.min_50_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            Intent intent = new Intent(OrderTypeActivity.this, MenuActivity.class);
                            intent.putExtra("startWith", 2);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                break;

            case R.id.type_dinein1:
                mLoginStatus = userPrefs.getString("login_status", "");
                Constants.ORDER_TYPE = "CarryOut";
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(OrderTypeActivity.this, SelectStoresActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(OrderTypeActivity.this, LoginActivity.class);
                    startActivityForResult(intent, DINEIN_REQUEST);
                }
                break;

            case R.id.type_delivery:
                if (myDbHelper.getTotalOrderPrice() >= 50) {
                    mLoginStatus = userPrefs.getString("login_status", "");
                    Constants.ORDER_TYPE = "Delivery";
                    if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                        Intent intent = new Intent(OrderTypeActivity.this, AddressActivity.class);
                        intent.putExtra("confirm_order", true);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(OrderTypeActivity.this, LoginActivity.class);
                        startActivityForResult(intent, DELIVERY_REQUEST);
                    }
                } else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderTypeActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if (language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        no.setText(getResources().getString(R.string.no));
                        yes.setText(getResources().getString(R.string.yes));
                        desc.setText(getResources().getString(R.string.min_50));
                    } else {
                        title.setText(getResources().getString(R.string.dajen_ar));
                        no.setText(getResources().getString(R.string.no_ar));
                        yes.setText(getResources().getString(R.string.yes_ar));
                        desc.setText(getResources().getString(R.string.min_50_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            Intent intent = new Intent(OrderTypeActivity.this, MenuActivity.class);
                            intent.putExtra("startWith", 2);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                break;

            case R.id.type_pickup:
                mLoginStatus = userPrefs.getString("login_status", "");
                Constants.ORDER_TYPE = "CarryOut";
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(OrderTypeActivity.this, SelectStoresActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(OrderTypeActivity.this, LoginActivity.class);
                    startActivityForResult(intent, TAKEAWAY_REQUEST);
                }
                break;

            case R.id.type_dinein:
                mLoginStatus = userPrefs.getString("login_status", "");
                Constants.ORDER_TYPE = "DineIn";
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(OrderTypeActivity.this, SelectStoresActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(OrderTypeActivity.this, LoginActivity.class);
                    startActivityForResult(intent, DINEIN_REQUEST);
                }
                break;

            case R.id.cart:
                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderTypeActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if (language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.cart_noitems));
                    } else {
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.cart_noitems_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                } else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderTypeActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if (language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.cart_clear));
                        no.setText(getResources().getString(R.string.checkout_title));
                        desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                    } else {
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.cart_clear_ar));
                        no.setText(getResources().getString(R.string.checkout_title_ar));
                        desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
                            Intent intent = new Intent(OrderTypeActivity.this, MenuActivity.class);
                            intent.putExtra("startWith", 2);
                            intent.putExtra("type", "main");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            finish();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == DELIVERY_REQUEST && resultCode == RESULT_OK) {
            Intent intent = new Intent(OrderTypeActivity.this, AddressActivity.class);
            intent.putExtra("confirm_order", true);
            startActivity(intent);
        } else if (requestCode == TAKEAWAY_REQUEST && resultCode == RESULT_OK) {
            Intent intent = new Intent(OrderTypeActivity.this, SelectStoresActivity.class);
            startActivity(intent);
        } else if (requestCode == DINEIN_REQUEST && resultCode == RESULT_OK) {
            Intent intent = new Intent(OrderTypeActivity.this, SelectStoresActivity.class);
            startActivity(intent);
        } else if (resultCode == RESULT_CANCELED) {
//            finish();
//            Toast.makeText(LoginActivity.this, "Registration unseccessfull", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(OrderTypeActivity.this, CheckoutActivity.class));
        finish();
    }

    public void getGPSCoordinates() {
        gps = new GPSTracker(OrderTypeActivity.this);
        if (gps != null) {
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
//                new GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OrderTypeActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                } else {
                    Toast.makeText(OrderTypeActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

//    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
//        ProgressDialog pDialog;
//        String networkStatus;
//        //        MaterialDialog dialog;
//        String dayOfWeek;
//        String response;
//
//        @Override
//        protected void onPreExecute() {
//            storesList.clear();
//            networkStatus = NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
////            dialog = new MaterialDialog.Builder(SelectStoresActivity.this)
////                    .title(R.string.app_name)
////                    .content("Fetching stores...")
////                    .progress(true, 0)
////                    .progressIndeterminateStyle(true)
////                    .show();
//            Calendar calendar = Calendar.getInstance();
//            Date date = calendar.getTime();
//            // full name form of the day
//            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0] + dayOfWeek);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            } else {
//                return "no internet";
//            }
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//            storesList.clear();
//            if (result != null) {
//                if (result.equalsIgnoreCase("no internet")) {
//                    if (language.equalsIgnoreCase("En")) {
//                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    if (result.equals("")) {
//                        Toast.makeText(OrderTypeActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//                    } else {
//
//                        try {
//                            JSONObject jsonObject = new JSONObject(result);
//                            JSONArray ja = jsonObject.getJSONArray("Success");
//                            for (int i = 0; i < ja.length(); i++) {
//                                StoreInfo si = new StoreInfo();
//                                JSONObject jo = ja.getJSONObject(i);
////                                lat = 24.70321657;
////                                longi = 46.68097073;
//                                si.setStoreId(jo.getString("storeId"));
//                                si.setOnlineOrderStatus(jo.getString("OnlineOrderStatus"));
//                                si.setStartTime(jo.getString("ST"));
//                                si.setEndTime(jo.getString("ET"));
//                                si.setStoreName(jo.getString("StoreName"));
//                                si.setStoreAddress(jo.getString("StoreAddress"));
//                                si.setLatitude(jo.getDouble("Latitude"));
//                                si.setLongitude(jo.getDouble("Longitude"));
//                                si.setCountryName(jo.getString("CountryName"));
//                                si.setCityName(jo.getString("CityName"));
//                                si.setImageURL(jo.getString("imageURL"));
//                                si.setDeliverydistance(jo.getInt("DeliveryDistance"));
//                                si.setDine_distance(jo.getInt("DineInDistance"));
//                                si.setFamilySection(jo.getString("FamilySection"));
//                                si.setWifi(jo.getString("Wifi"));
//                                si.setPatioSitting(jo.getString("PatioSitting"));
//                                si.setDriveThru(jo.getString("DriveThru"));
//                                si.setMeetingSpace(jo.getString("MeetingSpace"));
//                                si.setHospital(jo.getString("Hospital"));
//                                si.setUniversity(jo.getString("University"));
//                                si.setOffice(jo.getString("Office"));
//                                si.setShoppingMall(jo.getString("ShoppingMall"));
//                                try {
//                                    si.setAirPort(jo.getString("Airport"));
//                                } catch (Exception e) {
//                                    si.setAirPort("false");
//                                }
//                                try {
//                                    si.setDineIn(jo.getString("DineIn"));
//                                } catch (Exception e) {
//                                    si.setDineIn("false");
//                                }
//                                try {
//                                    si.setLadies(jo.getString("Ladies"));
//                                } catch (Exception e) {
//                                    si.setLadies("false");
//                                }
//
//                                si.setNeighborhood(jo.getString("Neighborhood"));
//                                si.setStoreNumber(jo.getString("phone"));
//                                si.setIs24x7(jo.getString("is24x7"));
//                                si.setStatus(jo.getString("status"));
//                                si.setOgCountry(jo.getString("OGCountry"));
//                                si.setOgCity(jo.getString("OGCity"));
//                                si.setStoreName_ar(jo.getString("StoreName_ar"));
//                                si.setStoreAddress_ar(jo.getString("StoreAddress_ar"));
//                                si.setStoreNumber(jo.getString("phone"));
//                                try {
//                                    si.setMessage(jo.getString("Message"));
//                                } catch (Exception e) {
//                                    si.setMessage("");
//                                }
//
//                                try {
//                                    si.setMessage_ar(jo.getString("Message_ar"));
//                                } catch (Exception e) {
//                                    si.setMessage_ar("");
//                                }
//
//                                Location me = new Location("");
//                                Location dest = new Location("");
//
//                                me.setLatitude(lat);
//                                me.setLongitude(longi);
//
//                                dest.setLatitude(jo.getDouble("Latitude"));
//                                dest.setLongitude(jo.getDouble("Longitude"));
//
//                                float dist = (me.distanceTo(dest)) / 1000;
//                                si.setDistance(dist);
//
//                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
//                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
//                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
//                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
//                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
//
//                                Calendar c = Calendar.getInstance();
//                                System.out.println("Current time => " + c.getTime());
//                                serverTime = timeResponse;
//                                String startTime = si.getStartTime();
//                                String endTime = si.getEndTime();
//
//                                if (dist <= si.getDine_distance() && (jo.getBoolean("OnlineOrderStatus"))) {
//
//                                    Log.i("TAG","dist " + dist);
//
//                                    if (startTime.equals("null") && endTime.equals("null")) {
//                                        si.setOpenFlag(-1);
//                                        storesList.add(si);
//                                    } else {
//
//                                        if (endTime.equals("00:00AM")) {
//                                            si.setOpenFlag(1);
//                                            storesList.add(si);
//
//                                            continue;
//                                        } else if (endTime.equals("12:00AM")) {
//                                            endTime = "11:59PM";
//                                        }
//
//                                        Calendar now = Calendar.getInstance();
//
//                                        int hour = now.get(Calendar.HOUR_OF_DAY);
//                                        int minute = now.get(Calendar.MINUTE);
//
//
//                                        Date serverDate = null;
//                                        Date end24Date = null;
//                                        Date start24Date = null;
//                                        Date current24Date = null;
//                                        Date dateToday = null;
//                                        Calendar dateStoreClose = Calendar.getInstance();
//                                        try {
//                                            end24Date = dateFormat2.parse(endTime);
//                                            start24Date = dateFormat2.parse(startTime);
//                                            serverDate = dateFormat.parse(serverTime);
//                                            dateToday = dateFormat.parse(serverTime);
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        Date startDate = null;
//                                        Date endDate = null;
//
//                                        try {
//                                            dateStoreClose.setTime(dateToday);
//                                            dateStoreClose.add(Calendar.DATE, 1);
//                                            String current24 = timeFormat1.format(serverDate);
//                                            String end24 = timeFormat1.format(end24Date);
//                                            String start24 = timeFormat1.format(start24Date);
//                                            String startDateString = dateFormat1.format(dateToday);
//                                            String endDateString = dateFormat1.format(dateToday);
//                                            String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
//                                            dateStoreClose.add(Calendar.DATE, -2);
//                                            String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());
//
//
//                                            try {
//                                                end24Date = timeFormat1.parse(end24);
//                                                start24Date = timeFormat1.parse(start24);
//                                                current24Date = timeFormat1.parse(current24);
//                                            } catch (ParseException e) {
//                                                e.printStackTrace();
//                                            }
//
//
//                                            String[] parts2 = start24.split(":");
//                                            int startHour = Integer.parseInt(parts2[0]);
//                                            int startMinute = Integer.parseInt(parts2[1]);
//
//                                            String[] parts = end24.split(":");
//                                            int endHour = Integer.parseInt(parts[0]);
//                                            int endMinute = Integer.parseInt(parts[1]);
//
//                                            String[] parts1 = current24.split(":");
//                                            int currentHour = Integer.parseInt(parts1[0]);
//                                            int currentMinute = Integer.parseInt(parts1[1]);
//
//
////                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");
//
//
//                                            if (startTime.contains("AM") && endTime.contains("AM")) {
//                                                if (startHour < endHour) {
//                                                    startDateString = startDateString + " " + startTime;
//                                                    endDateString = endDateString + "  " + endTime;
//                                                    try {
//                                                        startDate = dateFormat2.parse(startDateString);
//                                                        endDate = dateFormat2.parse(endDateString);
//                                                    } catch (ParseException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                } else if (startHour > endHour) {
//                                                    if (serverTime.contains("AM")) {
//                                                        if (currentHour > endHour) {
//                                                            startDateString = startDateString + " " + startTime;
//                                                            endDateString = endDateTomorrow + "  " + endTime;
//                                                            try {
//                                                                startDate = dateFormat2.parse(startDateString);
//                                                                endDate = dateFormat2.parse(endDateString);
//                                                            } catch (ParseException e) {
//                                                                e.printStackTrace();
//                                                            }
//                                                        } else {
//                                                            startDateString = endDateYesterday + " " + startTime;
//                                                            endDateString = endDateString + "  " + endTime;
//                                                            try {
//                                                                startDate = dateFormat2.parse(startDateString);
//                                                                endDate = dateFormat2.parse(endDateString);
//                                                            } catch (ParseException e) {
//                                                                e.printStackTrace();
//                                                            }
//                                                        }
//                                                    } else {
//                                                        startDateString = startDateString + " " + startTime;
//                                                        endDateString = endDateTomorrow + "  " + endTime;
//                                                        try {
//                                                            startDate = dateFormat2.parse(startDateString);
//                                                            endDate = dateFormat2.parse(endDateString);
//                                                        } catch (ParseException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    }
//                                                }
//                                            } else if (startTime.contains("AM") && endTime.contains("PM")) {
//                                                startDateString = startDateString + " " + startTime;
//                                                endDateString = endDateString + "  " + endTime;
//                                                try {
//                                                    startDate = dateFormat2.parse(startDateString);
//                                                    endDate = dateFormat2.parse(endDateString);
//                                                } catch (ParseException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            } else if (startTime.contains("PM") && endTime.contains("AM")) {
//                                                if (serverTime.contains("AM")) {
//                                                    if (currentHour <= endHour) {
//                                                        startDateString = endDateYesterday + " " + startTime;
//                                                        endDateString = endDateString + "  " + endTime;
//                                                        try {
//                                                            startDate = dateFormat2.parse(startDateString);
//                                                            endDate = dateFormat2.parse(endDateString);
//                                                        } catch (ParseException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    } else {
//                                                        startDateString = startDateString + " " + startTime;
//                                                        endDateString = endDateTomorrow + "  " + endTime;
//                                                        try {
//                                                            startDate = dateFormat2.parse(startDateString);
//                                                            endDate = dateFormat2.parse(endDateString);
//                                                        } catch (ParseException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    }
//                                                } else {
//                                                    startDateString = startDateString + " " + startTime;
//                                                    endDateString = endDateTomorrow + "  " + endTime;
//                                                    try {
//                                                        startDate = dateFormat2.parse(startDateString);
//                                                        endDate = dateFormat2.parse(endDateString);
//                                                    } catch (ParseException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                }
//
//                                            } else if (startTime.contains("PM") && endTime.contains("PM")) {
//                                                startDateString = startDateString + " " + startTime;
//                                                endDateString = endDateString + "  " + endTime;
//                                            }
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        try {
//                                            startDate = dateFormat2.parse(si.getStartTime());
//                                            endDate = dateFormat2.parse(si.getEndTime());
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        String serverDateString = null;
//                                        try {
//
//                                            serverDateString = dateFormat.format(serverDate);
//
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        Log.i("TAG", "serverdate " + serverDateString);
//
//                                        try {
//                                            serverDate = dateFormat.parse(serverDateString);
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        Log.i("TAG DATE", "" + startDate);
//                                        Log.i("TAG DATE1", "" + endDate);
//                                        Log.i("TAG DATE2", "" + serverDate);
//
//                                        try {
//
//                                            if (serverDate.after(startDate) && serverDate.before(endDate)) {
//                                                Log.i("TAG Visible", "true");
//                                                si.setOpenFlag(1);
//                                                storesList.add(si);
//                                            } else {
//                                                si.setOpenFlag(0);
//                                                storesList.add(si);
//                                            }
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                }
////                                storesList.add(si);
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        Collections.sort(storesList, StoreInfo.storeDistance);
//                        Collections.sort(storesList, StoreInfo.storeOpenSort);
//                    }
//                }
//
//            } else
//
//            {
//                Toast.makeText(OrderTypeActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//            }
//            if (dialog != null)
//
//            {
//                dialog.dismiss();
//            }
//
//            if (storesList.size() == 0)
//
//            {
//
//                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderTypeActivity.this);
//                // ...Irrelevant code for customizing the buttons and title
//                LayoutInflater inflater = getLayoutInflater();
//                int layout = R.layout.alert_dialog;
//                View dialogView = inflater.inflate(layout, null);
//                dialogBuilder.setView(dialogView);
//                dialogBuilder.setCancelable(false);
//
//                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//                TextView title = (TextView) dialogView.findViewById(R.id.title);
//                View vert = (View) dialogView.findViewById(R.id.vert_line);
//
//                no.setVisibility(View.GONE);
//                vert.setVisibility(View.GONE);
//                if (language.equalsIgnoreCase("En")) {
//                    title.setText(getResources().getString(R.string.dajen));
//                    yes.setText(getResources().getString(R.string.ok));
//                    desc.setText(getResources().getString(R.string.no_store_found));
//                } else {
//                    title.setText(getResources().getString(R.string.dajen_ar));
//                    yes.setText(getResources().getString(R.string.ok_ar));
//                    desc.setText(getResources().getString(R.string.no_store_found_ar));
//                }
//
//                yes.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        customDialog.dismiss();
//                    }
//                });
//
//                customDialog = dialogBuilder.create();
//                customDialog.show();
//                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                Window window = customDialog.getWindow();
//                lp.copyFrom(window.getAttributes());
//                //This makes the dialog take up the full width
//                Display display = getWindowManager().getDefaultDisplay();
//                Point size = new Point();
//                display.getSize(size);
//                int screenWidth = size.x;
//
//                double d = screenWidth * 0.85;
//                lp.width = (int) d;
//                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                window.setAttributes(lp);
//            }
//
//            super.
//
//                    onPostExecute(result);
//
//        }
//
//    }

    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
            dialog = new MaterialDialog.Builder(OrderTypeActivity.this)
                    .title(R.string.app_name)
                    .content("Fetching stores...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            } else {
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (serverTime == null) {
//                dialog.dismiss();
            } else if (serverTime.equals("no internet")) {
//                dialog.dismiss();
                Toast.makeText(OrderTypeActivity.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
//                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
//                    timeResponse = "06/02/2018 05:40 PM";
                } catch (JSONException je) {
                    je.printStackTrace();
                }
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                if(language.equalsIgnoreCase("En")) {

//                }else if(language.equalsIgnoreCase("Ar")){
//                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
//                    mStoresListView.setAdapter(mStoreListAdapterArabic);
//                }

            }
            super.onPostExecute(result1);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
        cart_count.setText("" + myDbHelper.getTotalOrderQty());
    }
}
