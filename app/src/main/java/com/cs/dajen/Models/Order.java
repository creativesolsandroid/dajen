package com.cs.dajen.Models;

/**
 * Created by CS on 14-06-2016.
 */
public class Order {
    String orderId, categoryId, itemId, itemName, itemNameAr, itemImage, description, descriptionAr, itemTypeId, typeName, itemPrice, modifierId, modifierName, modifierName_Ar, mdfrImage, itemPriceAd, additionalId, additionalName, additionalNameAr, addlImage, addlTypeId, additionalPrice, qty, totalAmount, size, comment, custom, singleprice, additoinal;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNameAr() {
        return itemNameAr;
    }

    public void setItemNameAr(String itemNameAr) {
        this.itemNameAr = itemNameAr;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(String itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public String getModifierName() {
        return modifierName;
    }

    public void setModifierName(String modifierName) {
        this.modifierName = modifierName;
    }

    public String getModifierName_Ar() {
        return modifierName_Ar;
    }

    public void setModifierName_Ar(String modifierName_Ar) {
        this.modifierName_Ar = modifierName_Ar;
    }

    public String getMdfrImage() {
        return mdfrImage;
    }

    public void setMdfrImage(String mdfrImage) {
        this.mdfrImage = mdfrImage;
    }

    public String getItemPriceAd() {
        return itemPriceAd;
    }

    public void setItemPriceAd(String itemPriceAd) {
        this.itemPriceAd = itemPriceAd;
    }

    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public String getAdditionalName() {
        return additionalName;
    }

    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }

    public String getAdditionalNameAr() {
        return additionalNameAr;
    }

    public void setAdditionalNameAr(String additionalNameAr) {
        this.additionalNameAr = additionalNameAr;
    }

    public String getAddlImage() {
        return addlImage;
    }

    public void setAddlImage(String addlImage) {
        this.addlImage = addlImage;
    }

    public String getAddlTypeId() {
        return addlTypeId;
    }

    public void setAddlTypeId(String addlTypeId) {
        this.addlTypeId = addlTypeId;
    }

    public String getAdditionalPrice() {
        return additionalPrice;
    }

    public void setAdditionalPrice(String additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public String getSingleprice() {
        return singleprice;
    }

    public void setSingleprice(String singleprice) {
        this.singleprice = singleprice;
    }

    public String getAdditoinal() {
        return additoinal;
    }

    public void setAdditoinal(String additoinal) {
        this.additoinal = additoinal;
    }
}
