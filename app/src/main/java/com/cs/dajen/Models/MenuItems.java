package com.cs.dajen.Models;

import java.io.Serializable;

/**
 * Created by CS on 27-04-2017.
 */

public class MenuItems implements Serializable {

    String catId, itemId, itemName, modifierID, itemName_ar, desc, desc_ar, additionalId, image;
    String price1, price2, price3, price4, price5, price6, price7;
    Boolean twoCount;

    public Boolean getTwoCount() {
        return twoCount;
    }

    public void setTwoCount(Boolean twoCount) {
        this.twoCount = twoCount;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getModifierID() {
        return modifierID;
    }

    public void setModifierID(String modifierID) {
        this.modifierID = modifierID;
    }

    public String getItemName_ar() {
        return itemName_ar;
    }

    public void setItemName_ar(String itemName_ar) {
        this.itemName_ar = itemName_ar;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc_ar() {
        return desc_ar;
    }

    public void setDesc_ar(String desc_ar) {
        this.desc_ar = desc_ar;
    }

    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice1() {
        return price1;
    }

    public void setPrice1(String price1) {
        this.price1 = price1;
    }

    public String getPrice2() {
        return price2;
    }

    public void setPrice2(String price2) {
        this.price2 = price2;
    }

    public String getPrice3() {
        return price3;
    }

    public void setPrice3(String price3) {
        this.price3 = price3;
    }

    public String getPrice4() {
        return price4;
    }

    public void setPrice4(String price4) {
        this.price4 = price4;
    }

    public String getPrice5() {
        return price5;
    }

    public void setPrice5(String price5) {
        this.price5 = price5;
    }

    public String getPrice6() {
        return price6;
    }

    public void setPrice6(String price6) {
        this.price6 = price6;
    }

    public String getPrice7() {
        return price7;
    }

    public void setPrice7(String price7) {
        this.price7 = price7;
    }

}
