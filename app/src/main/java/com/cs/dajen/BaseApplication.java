package com.cs.dajen;

import android.app.Application;

import com.cs.dajen.widgets.FontsOverride;

/**
 * Created by CS on 26-05-2016.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FontsOverride.setDefaultFont(this, "SERIF", "helveticaneue.ttf");
    }
}
