package com.cs.dajen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cs.dajen.Models.Order;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 03-05-2017.
 */

public class DataBaseHelper extends SQLiteOpenHelper{

    private static final String LOGCAT = null;

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "dajen.db";

    // Contacts table name
    private static final String TABLE_CONTACTS = "OrderTable";

    public DataBaseHelper(Context applicationcontext) {
        super(applicationcontext, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(LOGCAT,"Created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query,query1;
        query = "CREATE TABLE \"OrderTable\" (\"OrderId\" INTEGER PRIMARY KEY  AUTOINCREMENT ,\"CategoryId\" INTEGER, \"ItemId\" INTEGER,\"ItemName\" VARCHAR,\"ItemName_Ar\" VARCHAR,\"ItemDescription_En\" VARCHAR,\"ItemDescription_Ar\" VARCHAR,\"ItemImage\"VARCHAR,\"ItemTypeId\" INTEGER,\"TypeName\"VARCHAR,\"Price\" TEXT DEFAULT 0,\"ModifierId\" TEXT DEFAULT 0,\"ModifierName\"VARCHAR,\"ModifierName_Ar\"VARCHAR,\"MdfrImage\"VARCHAR, \"AdditionalID\" TEXT DEFAULT 0,\"AdditionalName\" VARCHAR,\"AdditionalName_Ar\" VARCHAR,\"AddlImage\"VARCHAR,\"AddlTypeId\"VARCHAR, \"AdditionalPrice\" TEXT DEFAULT 0, \"Qty\" INTEGER DEFAULT 0,\"TotalAmount\" TEXT,\"Comment\" TEXT, \"Custom\" INTEGER DEFAULT 0, \"SinglePrice\" INTEGER DEFAULT 0)";

        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }

    public void insertOrder(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("CategoryId", queryValues.get("CategoryId"));
        values.put("ItemId", queryValues.get("ItemId"));
        values.put("ItemName", queryValues.get("ItemName"));
        values.put("ItemName_Ar", queryValues.get("ItemName_Ar"));
        values.put("ItemDescription_En", queryValues.get("ItemDescription_En"));
        values.put("ItemDescription_Ar", queryValues.get("ItemDescription_Ar"));
        values.put("ItemImage", queryValues.get("ItemImage"));
        values.put("ItemTypeId", queryValues.get("ItemTypeId"));
        values.put("TypeName", queryValues.get("TypeName"));
        values.put("Price", queryValues.get("Price"));
        values.put("ModifierId", queryValues.get("ModifierId"));
        values.put("ModifierName", queryValues.get("ModifierName"));
        values.put("ModifierName_Ar", queryValues.get("ModifierName_Ar"));
        values.put("MdfrImage", queryValues.get("MdfrImage"));
        values.put("AdditionalID", queryValues.get("AdditionalID"));
        values.put("AdditionalName", queryValues.get("AdditionalName"));
        values.put("AdditionalName_Ar", queryValues.get("AdditionalName_Ar"));
        values.put("AddlImage", queryValues.get("AddlImage"));
        values.put("AddlTypeId", queryValues.get("AddlTypeId"));
        values.put("AdditionalPrice", queryValues.get("AdditionalPrice"));
        values.put("Qty", queryValues.get("Qty"));
        values.put("TotalAmount", queryValues.get("TotalAmount"));
        values.put("Comment", queryValues.get("Comment"));
        values.put("Custom", queryValues.get("Custom"));
        values.put("SinglePrice", queryValues.get("SinglePrice"));

        database.insert("OrderTable", null, values);
        database.close();
    }

    public void updateOrder(String qty, String price, String orderId){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("qty", qty);
        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET Qty ="+qty+ ",TotalAmount ="+ price +" where orderId ="+orderId;
        database.execSQL(updateQuery);
    }

    public void updateOrderusingOrderID(String qty, String price, String orderId){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("qty", qty);
        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET Qty ="+qty+ ",TotalAmount ="+ price +" where OrderId ="+orderId;
        database.execSQL(updateQuery);
    }


    public ArrayList<Order> getOrderInfo(){
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setCategoryId(cursor.getString(1));
                sc.setItemId(cursor.getString(2));
                sc.setItemName(cursor.getString(3));
                sc.setItemNameAr(cursor.getString(4));
                sc.setDescription(cursor.getString(5));
                sc.setDescriptionAr(cursor.getString(6));
                sc.setItemImage(cursor.getString(7));
                sc.setItemTypeId(cursor.getString(8));
                sc.setTypeName(cursor.getString(9));
                sc.setItemPrice(cursor.getString(10));
                sc.setModifierId(cursor.getString(11));
                sc.setModifierName(cursor.getString(12));
                sc.setModifierName_Ar(cursor.getString(13));
                sc.setMdfrImage(cursor.getString(14));
                sc.setAdditionalId(cursor.getString(15));
                sc.setAdditionalName(cursor.getString(16));
                sc.setAdditionalNameAr(cursor.getString(17));
                sc.setAddlImage(cursor.getString(18));
                sc.setAddlTypeId(cursor.getString(19));
                sc.setAdditionalPrice(cursor.getString(20));
                sc.setQty(cursor.getString(21));
                sc.setTotalAmount(cursor.getString(22));
                sc.setComment(cursor.getString(23));
                sc.setCustom(cursor.getString(24));
                sc.setSingleprice(cursor.getString(25));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public ArrayList<Order> getItemOrderInfo(String itemId){
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable where ItemId = "+itemId;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setCategoryId(cursor.getString(1));
                sc.setItemId(cursor.getString(2));
                sc.setItemName(cursor.getString(3));
                sc.setItemNameAr(cursor.getString(4));
                sc.setDescription(cursor.getString(5));
                sc.setDescriptionAr(cursor.getString(6));
                sc.setItemImage(cursor.getString(7));
                sc.setItemTypeId(cursor.getString(8));
                sc.setTypeName(cursor.getString(9));
                sc.setItemPrice(cursor.getString(10));
                sc.setModifierId(cursor.getString(11));
                sc.setModifierName(cursor.getString(12));
                sc.setModifierName_Ar(cursor.getString(13));
                sc.setMdfrImage(cursor.getString(14));
                sc.setAdditionalId(cursor.getString(15));
                sc.setAdditionalName(cursor.getString(16));
                sc.setAdditionalNameAr(cursor.getString(17));
                sc.setAddlImage(cursor.getString(18));
                sc.setAddlTypeId(cursor.getString(19));
                sc.setAdditionalPrice(cursor.getString(20));
                sc.setQty(cursor.getString(21));
                sc.setTotalAmount(cursor.getString(22));
                sc.setComment(cursor.getString(23));
                sc.setCustom(cursor.getString(24));
                sc.setSingleprice(cursor.getString(25));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }


    public int getItemOrderCount(String itemId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(Qty) FROM  OrderTable where ItemId="+itemId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }
    public int getItemOrderCountByOrderID(String itemId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(Qty) FROM  OrderTable where OrderId="+itemId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getItemOrderPrice(String itemId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(price) FROM  OrderTable where OrderId="+itemId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getCatOrderCount(String CategoryId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where CategoryId="+CategoryId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getTotalOrderQty(){
        int qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cur = database.rawQuery("SELECT SUM(Qty) FROM OrderTable", null);

            if (cur.moveToFirst()) {
                qty = cur.getInt(0);
            }
        }catch (SQLiteException sqle){
            sqle.printStackTrace();
        }
        return qty;
    }

    public float getTotalOrderPrice(){
        float qty = 0;
        String selectQuery = "SELECT OrderId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT SUM(TotalAmount) FROM OrderTable", null);
        if(cur.moveToFirst())
        {
            qty  = cur.getFloat(0);
        }

        return qty;
    }


    public void deleteItemFromOrder(String orderId){
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where orderId = "+orderId;
        database.execSQL(updateQuery);

    }
    public void deleteItemFromOrderUsingOrderID(String orderId, String Custom){
        SQLiteDatabase database = this.getWritableDatabase();
        String updateQuery = null;
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        if(Custom.equalsIgnoreCase("0")) {
            updateQuery = "DELETE FROM OrderTable where OrderId = " + orderId;
            database.execSQL(updateQuery);
        }
        else{
            updateQuery = "DELETE FROM OrderTable where OrderId = " + orderId;
            database.execSQL(updateQuery);
            int id = Integer.parseInt(orderId);
            id=id+1;
            String updateQuery1 = "DELETE FROM OrderTable where OrderId = " + id;
            database.execSQL(updateQuery1);
        }

    }

    public void deleteOrderTable(){
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable";
        database.execSQL(updateQuery);
    }

    public void updateCommentFromOrderID(String orderId, String comment){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET comment = '"+comment+"' where orderId ="+orderId;
        database.execSQL(updateQuery);
    }
    public void updateCommentFromItemID(String orderId, String comment){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET comment = '"+comment+"' where ItemId ="+orderId;
        database.execSQL(updateQuery);
    }
    public String getCommentFromOrderID(String orderId){
        String comment="";
        String selectQuery = "SELECT OrderId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT comment FROM OrderTable where orderId ="+orderId, null);
        if(cur.moveToFirst())
        {
            comment  = cur.getString(0);
        }

        return comment;
    }
    public String getCommentFromItemID(String orderId){
        String comment="";
        String selectQuery = "SELECT OrderId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT comment FROM OrderTable where ItemId ="+orderId, null);
        if(cur.moveToFirst())
        {
            comment  = cur.getString(0);
        }

        return comment;
    }
}
