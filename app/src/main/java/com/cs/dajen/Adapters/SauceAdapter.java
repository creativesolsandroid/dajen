package com.cs.dajen.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.R;
import com.cs.dajen.activity.BBQBurgerActivity;
import com.cs.dajen.activity.DajenBBQActivity;
import com.cs.dajen.activity.InsideProductActivity;
import com.cs.dajen.activity.TestingMenuDetails;

import java.util.ArrayList;


/**
 * Created by cs android on 15-02-2017.
 */

public class SauceAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Additionals> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    String language, activity;

    public SauceAdapter(Context context, ArrayList<Additionals> orderList, String language, String activity) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.activity = activity;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title,price;
        ImageView mainImage, right;
        RelativeLayout GridLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.extras_list, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.extras_list_ar, null);
            }


            holder.title = (TextView) convertView.findViewById(R.id.extra_name);
            holder.price = (TextView) convertView.findViewById(R.id.extra_price);
            holder.mainImage = (ImageView) convertView.findViewById(R.id.extra_image);
            holder.right = (ImageView) convertView.findViewById(R.id.tick_mark);
            holder.GridLayout = (RelativeLayout) convertView.findViewById(R.id.grid_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Glide.with(context).load(Constants.IMAGE_URL+orderList.get(position).getImages()).into(holder.mainImage);
        if(language.equalsIgnoreCase("En")) {
            holder.title.setText(orderList.get(position).getAdditionalName());
        }
        else{
            holder.title.setText(orderList.get(position).getAdditionalNameAr());
        }
        holder.price.setText(Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice())));

        if(activity.equalsIgnoreCase("inside")) {
            if (InsideProductActivity.sauce_pos.size() > 0 && InsideProductActivity.sauce_pos.contains(Integer.toString(position))) {
                holder.right.setVisibility(View.VISIBLE);
            } else {
                holder.right.setVisibility(View.INVISIBLE);
            }
            InsideProductActivity.footer_price.setText(Constants.decimalFormat.format((((InsideProductActivity.price + InsideProductActivity.cooldrinkprice + InsideProductActivity.sauce_price + InsideProductActivity.bread_price) * InsideProductActivity.quantity))));
        }
        else if(activity.equalsIgnoreCase("testing")) {
            if (TestingMenuDetails.sauce_pos.size() > 0 && TestingMenuDetails.sauce_pos.contains(Integer.toString(position))) {
                holder.right.setVisibility(View.VISIBLE);
            } else {
                holder.right.setVisibility(View.INVISIBLE);
            }
            TestingMenuDetails.footer_price.setText(Constants.decimalFormat.format(((TestingMenuDetails.price + TestingMenuDetails.cooldrinkprice + TestingMenuDetails.sauce_price + TestingMenuDetails.bread_price) * TestingMenuDetails.quantity)) );
        }
        else if(activity.equalsIgnoreCase("dajen")) {
            if (DajenBBQActivity.sauce_pos.size() > 0 && DajenBBQActivity.sauce_pos.contains(Integer.toString(position))) {
                holder.right.setVisibility(View.VISIBLE);
            } else {
                holder.right.setVisibility(View.INVISIBLE);
            }
            DajenBBQActivity.footer_price.setText(Constants.decimalFormat.format((DajenBBQActivity.price + DajenBBQActivity.sauce_price + DajenBBQActivity.bread_price) * DajenBBQActivity.quantity));
        }
        else if(activity.equalsIgnoreCase("burger")) {
            if (BBQBurgerActivity.sauce_pos.size() > 0 && BBQBurgerActivity.sauce_pos.contains(Integer.toString(position))) {
                holder.right.setVisibility(View.VISIBLE);
            } else {
                holder.right.setVisibility(View.INVISIBLE);
            }
            BBQBurgerActivity.footer_price.setText(Constants.decimalFormat.format((BBQBurgerActivity.price + BBQBurgerActivity.sauce_price + BBQBurgerActivity.bread_price) * BBQBurgerActivity.quantity));
        }

        holder.GridLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity.equalsIgnoreCase("inside")) {
                    if (InsideProductActivity.quantity != 0) {
                        String pos = Integer.toString(position);
                        if (InsideProductActivity.sauce_pos != null && !InsideProductActivity.sauce_pos.contains(pos)) {
                            InsideProductActivity.sauce_pos.add(pos);
                            InsideProductActivity.bread_price = InsideProductActivity.bread_price + Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice());
                        } else {
                            InsideProductActivity.bread_price = InsideProductActivity.bread_price - Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice());
                            InsideProductActivity.sauce_pos.remove(pos);
                        }
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, "Please select Item Type first", Toast.LENGTH_SHORT).show();
                    }
                }
                else if(activity.equalsIgnoreCase("testing")) {
                    if (InsideProductActivity.quantity != 0) {
                        String pos = Integer.toString(position);
                        if (TestingMenuDetails.sauce_pos != null && !TestingMenuDetails.sauce_pos.contains(pos)) {
                            TestingMenuDetails.sauce_pos.add(pos);
                            TestingMenuDetails.bread_price = TestingMenuDetails.bread_price + Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice());
                        } else {
                            TestingMenuDetails.bread_price = TestingMenuDetails.bread_price - Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice());
                            TestingMenuDetails.sauce_pos.remove(pos);
                        }
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, "Please select Item Type first", Toast.LENGTH_SHORT).show();
                    }
                }
                else if(activity.equalsIgnoreCase("dajen")) {
                        String pos = Integer.toString(position);
                        if (DajenBBQActivity.sauce_pos != null && !DajenBBQActivity.sauce_pos.contains(pos)) {
                            DajenBBQActivity.sauce_pos.add(pos);
                            DajenBBQActivity.bread_price = DajenBBQActivity.bread_price + Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice());
                        } else {
                            DajenBBQActivity.bread_price = DajenBBQActivity.bread_price - Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice());
                            DajenBBQActivity.sauce_pos.remove(pos);
                        }
                        notifyDataSetChanged();
                }
                else if(activity.equalsIgnoreCase("burger")) {
                    String pos = Integer.toString(position);
                    if (BBQBurgerActivity.sauce_pos != null && !BBQBurgerActivity.sauce_pos.contains(pos)) {
                        BBQBurgerActivity.sauce_pos.add(pos);
                        BBQBurgerActivity.bread_price = BBQBurgerActivity.bread_price + Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice());
                    } else {
                        BBQBurgerActivity.bread_price = BBQBurgerActivity.bread_price - Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice());
                        BBQBurgerActivity.sauce_pos.remove(pos);
                    }
                    notifyDataSetChanged();
                }
            }
        });
        return convertView;
    }
}