package com.cs.dajen.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.ViewOrder;
import com.cs.dajen.R;

import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class ViewOrderAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<ViewOrder> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    int qty, price, pos1;
    String language;

    public ViewOrderAdapter(Context context, ArrayList<ViewOrder> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title, price, qty;
        ImageView itemIcon;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.view_details_row, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.view_details_row_ar, null);
            }

            holder.title = (TextView) convertView.findViewById(R.id.item_name);
            holder.price = (TextView) convertView.findViewById(R.id.item_price);
            holder.qty = (TextView) convertView.findViewById(R.id.item_qty);
            holder.itemIcon = (ImageView) convertView.findViewById(R.id.item_image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        if(orderList.get(position).getAdditionalName().equals("")){
//            holder.additionalsName.setVisibility(View.GONE);
//        }else {
//            holder.additionalsName.setVisibility(View.VISIBLE);
//            holder.additionalsName.setText(orderList.get(position).getAdditionalName());
//        }

        if(language.equalsIgnoreCase("En")){
            holder.title.setText(orderList.get(position).getItemName());
        }
        else{
            holder.title.setText(orderList.get(position).getItemNameAr());
        }
        if(Integer.parseInt(orderList.get(position).getQuantity())<10) {
            holder.qty.setText("0"+orderList.get(position).getQuantity());
        }
        else{
            holder.qty.setText(orderList.get(position).getQuantity());
        }
        float priceTxt = Float.parseFloat(orderList.get(position).getPrice())* Integer.parseInt(orderList.get(position).getQuantity());
        holder.price.setText(""+Constants.decimalFormat.format(priceTxt));
        Glide.with(context).load(Constants.IMAGE_URL+orderList.get(position).getItemImage()).into(holder.itemIcon);

		/*ArrayList<HashMap<String, String>> List = SchoolAlertMain.DBcontroller.getAllAlerts();
		//System.out.println("adapter"+alertsArrayList.get(position).getTime());
		if(List.contains(alertsArrayList.get(position).getAlertId())){
			holder.alertList.setBackgroundColor(Color.GRAY);
		}*/

        return convertView;
    }
}