package com.cs.dajen.Adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.Models.StoreInfo;
import com.cs.dajen.R;

import org.apache.commons.lang3.text.WordUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 15-06-2016.
 */
public class SelectStoresAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<StoreInfo> storesList = new ArrayList<>();
    //    private DataBaseHelper myDbHelper;
    int qty, price;
    String serverTime;
    String language;
    SharedPreferences languagePrefs;
    int logo_width;
    int logo_height;
    Double lat, longi;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    public SelectStoresAdapter(Context context, ArrayList<StoreInfo> storesList, Double lat, Double longi, String serverTime) {
        this.context = context;
        this.storesList = storesList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.lat = lat;
        this.longi = longi;
        this.serverTime = serverTime;
//        this.serverTime = "19/07/2016 01:05 AM";

        languagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
//        myDbHelper = new DataBaseHelper(context);
        Display deviceDisplay = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay();
        int layoutWidth = deviceDisplay.getWidth();// 480
        int layoutHeight = deviceDisplay.getHeight();// 800
        // ::::::::::: setting parameters for Header::::::::::
        logo_width = (int) (layoutWidth * 10 / 40);// 120

    }


    public int getCount() {
        return storesList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView storeName, storeAddress, storeOpenClose, storeTimings;
        ImageView storeImage;
        LinearLayout storeLocation, storeCall, storeShare;
        RelativeLayout storeItemLayout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.stores_list, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.stores_list_ar, null);
            }

            holder.storeName = (TextView) convertView.findViewById(R.id.store_name);
            holder.storeAddress = (TextView) convertView.findViewById(R.id.store_address);
            holder.storeTimings = (TextView) convertView.findViewById(R.id.store_timings);
            holder.storeImage = (ImageView) convertView.findViewById(R.id.store_image);
            holder.storeItemLayout = (RelativeLayout) convertView.findViewById(R.id.store_item);
            holder.storeOpenClose = (TextView) convertView.findViewById(R.id.store_status);
            holder.storeLocation = (LinearLayout) convertView.findViewById(R.id.store_location);
            holder.storeCall = (LinearLayout) convertView.findViewById(R.id.store_call);
            holder.storeShare = (LinearLayout) convertView.findViewById(R.id.store_share);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        RelativeLayout.LayoutParams msgTxtParam = new RelativeLayout.LayoutParams(
                logo_width, ViewGroup.LayoutParams.MATCH_PARENT);
//        holder.storeImage.setLayoutParams(msgTxtParam);
//        if(orderList.get(position).getAdditionalName().equals("")){
//            holder.additionalsName.setVisibility(View.GONE);
//        }else {
//            holder.additionalsName.setVisibility(View.VISIBLE);
        if (language.equalsIgnoreCase("En")) {
            holder.storeName.setText(WordUtils.capitalizeFully(storesList.get(position).getStoreName()));
            holder.storeAddress.setText(WordUtils.capitalizeFully(storesList.get(position).getStoreAddress()));
        } else {
            holder.storeName.setText(WordUtils.capitalizeFully(storesList.get(position).getStoreName_ar()));
            holder.storeAddress.setText(WordUtils.capitalizeFully(storesList.get(position).getStoreAddress_ar()));
        }

        SimpleDateFormat time = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat datetime = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        Date st = null, et = null;

        try {
            st = datetime.parse(storesList.get(position).getStartTime());
            et = datetime.parse(storesList.get(position).getEndTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String starttime = time.format(st);
        String endtime = time.format(et);
        holder.storeTimings.setText(starttime + " to " + endtime);


        if (storesList.get(position).getImageURL().equals("") || storesList.get(position).getImageURL().equals("null")) {
            holder.storeImage.setImageDrawable(context.getResources().getDrawable(R.drawable.store_image));
        } else {
            Glide.with(context).load(Constants.IMAGE_URL + storesList.get(position).getImageURL()).into(holder.storeImage);
        }
//        holder.openHours.setText(storesList.get(position).getStartTime() + " - " + storesList.get(position).getEndTime());

        holder.storeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + storesList.get(position).getLatitude() + "+" + storesList.get(position).getLongitude());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                context.startActivity(mapIntent);
            }
        });

        holder.storeShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "I would love to have Food with you at Dajen in " + storesList.get(position).getStoreName() +
                                ".\n http://maps.google.com/maps?saddr=" + lat + "," + longi + "&daddr=" + storesList.get(position).getLatitude() + "," + storesList.get(position).getLongitude());
//                intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
//                        "");
                context.startActivity(Intent.createChooser(intent, "Share"));
            }
        });

        holder.storeCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        Toast.makeText(context, "Please allow PHONE CALL permission from settings.", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+" + storesList.get(position).getStoreNumber()));
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        context.startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+" + storesList.get(position).getStoreNumber()));
                    context.startActivity(intent);
                }
            }
        });

		/*ArrayList<HashMap<String, String>> List = SchoolAlertMain.DBcontroller.getAllAlerts();
		//System.out.println("adapter"+alertsArrayList.get(position).getTime());
		if(List.contains(alertsArrayList.get(position).getAlertId())){
			holder.alertList.setBackgroundColor(Color.GRAY);
		}*/

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
//        serverTime = dateFormat.format(c.getTime());
        StoreInfo si = storesList.get(position);
        String startTime = si.getStartTime();
        String endTime = si.getEndTime();


//        if (startTime.equals("null") && endTime.equals("null")) {
//            holder.storeOpenClose.setText("CLOSE");
//            holder.storeOpenClose.setTextColor(Color.parseColor("#FF0000"));
//        } else {
//
//            if (endTime.equals("00:00AM")) {
//                holder.storeOpenClose.setText("OPEN");
//                holder.storeOpenClose.setTextColor(Color.parseColor("#FFFFFF"));
//                return convertView;
//            } else if (endTime.equals("12:00AM")) {
//                endTime = "11:59PM";
//            }

        Calendar now = Calendar.getInstance();

        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);


        Date serverDate = null;
        Date end24Date = null;
        Date start24Date = null;
        Date current24Date = null;
        Date dateToday = null;
        Calendar dateStoreClose = Calendar.getInstance();
        try {
            end24Date = dateFormat2.parse(endTime);
            start24Date = dateFormat2.parse(startTime);
            serverDate = dateFormat.parse(serverTime);
            dateToday = dateFormat.parse(serverTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dateStoreClose.setTime(dateToday);
        dateStoreClose.add(Calendar.DATE, 1);
        String current24 = timeFormat1.format(serverDate);
        String end24 = timeFormat1.format(end24Date);
        String start24 = timeFormat1.format(start24Date);
        String startDateString = dateFormat1.format(dateToday);
        String endDateString = dateFormat1.format(dateToday);
        String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
        dateStoreClose.add(Calendar.DATE, -2);
        String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

        Date startDate = null;
        Date endDate = null;

        try {
            end24Date = timeFormat1.parse(end24);
            start24Date = timeFormat1.parse(start24);
            current24Date = timeFormat1.parse(current24);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] parts2 = start24.split(":");
        int startHour = Integer.parseInt(parts2[0]);
        int startMinute = Integer.parseInt(parts2[1]);

        String[] parts = end24.split(":");
        int endHour = Integer.parseInt(parts[0]);
        int endMinute = Integer.parseInt(parts[1]);

        String[] parts1 = current24.split(":");
        int currentHour = Integer.parseInt(parts1[0]);
        int currentMinute = Integer.parseInt(parts1[1]);


//                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");


        if (startTime.contains("AM") && endTime.contains("AM")) {
            if (startHour < endHour) {
                startDateString = startDateString + " " + startTime;
                endDateString = endDateString + "  " + endTime;
                try {
                    startDate = dateFormat2.parse(startDateString);
                    endDate = dateFormat2.parse(endDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (startHour > endHour) {
                if (serverTime.contains("AM")) {
                    if (currentHour > endHour) {
                        startDateString = startDateString + " " + startTime;
                        endDateString = endDateTomorrow + "  " + endTime;
                        try {
                            startDate = dateFormat2.parse(startDateString);
                            endDate = dateFormat2.parse(endDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        startDateString = endDateYesterday + " " + startTime;
                        endDateString = endDateString + "  " + endTime;
                        try {
                            startDate = dateFormat2.parse(startDateString);
                            endDate = dateFormat2.parse(endDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    startDateString = startDateString + " " + startTime;
                    endDateString = endDateTomorrow + "  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (startTime.contains("AM") && endTime.contains("PM")) {
            startDateString = startDateString + " " + startTime;
            endDateString = endDateString + "  " + endTime;
            try {
                startDate = dateFormat2.parse(startDateString);
                endDate = dateFormat2.parse(endDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (startTime.contains("PM") && endTime.contains("AM")) {
            if (serverTime.contains("AM")) {
                if (currentHour <= endHour) {
                    startDateString = endDateYesterday + " " + startTime;
                    endDateString = endDateString + "  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    startDateString = startDateString + " " + startTime;
                    endDateString = endDateTomorrow + "  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                startDateString = startDateString + " " + startTime;
                endDateString = endDateTomorrow + "  " + endTime;
                try {
                    startDate = dateFormat2.parse(startDateString);
                    endDate = dateFormat2.parse(endDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        } else if (startTime.contains("PM") && endTime.contains("PM")) {
            startDateString = startDateString + " " + startTime;
            endDateString = endDateString + "  " + endTime;
        }

        try {
            startDate = dateFormat2.parse(si.getStartTime());
            endDate = dateFormat2.parse(si.getEndTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String serverDateString = null;

        try {
            serverDateString = dateFormat.format(serverDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            serverDate = dateFormat.parse(serverDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.i("TAG DATE", "" + startDate);
        Log.i("TAG DATE1", "" + endDate);
        Log.i("TAG DATE2", "" + serverDate);

        if (serverDate.after(startDate) && serverDate.before(endDate)) {
            Log.i("TAG Visible", "true");
            long diff = endDate.getTime() - serverDate.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
//            if (((int) diffHours) < 1 && ((int) diffMinutes) > 0) {
                holder.storeOpenClose.setText("OPEN");
                holder.storeOpenClose.setTextColor(Color.parseColor("#3EB654"));
                Log.i("TAG1", "Store opens in " + diffMinutes + " minutes");
            return convertView;
        } else {
            long diff = startDate.getTime() - serverDate.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            holder.storeOpenClose.setText("CLOSE");
            holder.storeOpenClose.setTextColor(Color.parseColor("#EF5350"));

            return convertView;
//        }
        }
    }



    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(context, perm));
    }


}