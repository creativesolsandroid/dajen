package com.cs.dajen.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Constants;
import com.cs.dajen.Models.FavouriteOrder;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brahmam on 19/2/16.
 */
public class FavouriteOrderAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<FavouriteOrder> favOrderList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language, response;
    int pos1=0;
    //public ImageLoader imageLoader;

    public FavouriteOrderAdapter(Context context, ArrayList<FavouriteOrder> favOrderList, String language) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView favouriteName,orderDate,totalPrice, itemDetails, remove;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.fav_order_list, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.fav_order_list_ar, null);
            }


            holder.favouriteName = (TextView) convertView
                    .findViewById(R.id.fav_name);
            holder.orderDate = (TextView) convertView
                    .findViewById(R.id.fav_date);
            holder.totalPrice = (TextView) convertView
                    .findViewById(R.id.fav_price);
            holder.remove = (TextView) convertView
                    .findViewById(R.id.remove);
            holder.itemDetails = (TextView) convertView.findViewById(R.id.fav_order);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.favouriteName.setText(favOrderList.get(position).getFavoriteName());
        holder.totalPrice.setText(Constants.decimalFormat.format(Float.parseFloat(favOrderList.get(position).getTotalPrice())));
        if(language.equalsIgnoreCase("En")){
//            holder.storeName.setText(favOrderList.get(position).getStoreName());
            holder.itemDetails.setText(favOrderList.get(position).getItemDetails());
        }else if(language.equalsIgnoreCase("Ar")){
//            holder.storeName.setText(favOrderList.get(position).getStoreName_ar());
            holder.itemDetails.setText(favOrderList.get(position).getItemDetails_ar());
        }
        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat curFormater1 = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(" ");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getOrderDate().replace("  ", " 0"));
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                dateObj = curFormater1.parse(favOrderList.get(position).getOrderDate());
            }catch (ParseException pe){

            }
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.orderDate.setText(date);

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos1 = position;
                new DeleteFavOrder().execute(Constants.DELETE_FAVORITE_ORDER_URL + favOrderList.get(position).getOrderId());
            }
        });
//
//        holder.totalBalance.setText(String.format("%.2f", Float.parseFloat(storesList.get(position).getTotalBalance())));


        return convertView;
    }

    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new MaterialDialog.Builder(context)
                    .title(R.string.app_name)
                    .content("Please Wait...")
                    .progress(true, 0)
                    .widgetColor(context.getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("PUT");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    response = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
                        favOrderList.remove(pos1);
                        notifyDataSetChanged();
                    }
                }

            }else {
                Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }
    }
}