package com.cs.dajen.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.R;
import com.cs.dajen.activity.BBQBurgerActivity;

import java.util.ArrayList;


/**
 * Created by cs android on 15-02-2017.
 */

public class BurgerAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Additionals> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    String language;

    public BurgerAdapter(Context context, ArrayList<Additionals> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title;
        ImageView mainImage,checkBox;
        LinearLayout GridLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.burger_additionals_list, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.burger_additionals_list_ar, null);
            }

            holder.title = (TextView) convertView.findViewById(R.id.name);
            holder.mainImage = (ImageView) convertView.findViewById(R.id.image);
            holder.checkBox = (ImageView) convertView.findViewById(R.id.checkbox);
            holder.GridLayout = (LinearLayout) convertView.findViewById(R.id.grid_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (!BBQBurgerActivity.burger_id.contains("51")) {
            BBQBurgerActivity.burger_id.add("51");
            BBQBurgerActivity.burger_pos.add(Integer.toString(position));
        }
        String pos1 = Integer.toString((orderList.size()-1));

        if (!BBQBurgerActivity.burger_id.contains("57")) {
            BBQBurgerActivity.burger_id.add("57");
            BBQBurgerActivity.burger_pos.add(pos1);
        }

        if(BBQBurgerActivity.burger_id.size()>0 && BBQBurgerActivity.burger_id.contains(orderList.get(position).getAdditionalsId())){
            holder.checkBox.setImageDrawable(context.getResources().getDrawable(R.drawable.burger_selected));
        }
        else{
            holder.checkBox.setImageDrawable(context.getResources().getDrawable(R.drawable.burger_unselected));
        }

        BBQBurgerActivity.footer_price.setText(Constants.decimalFormat.format((BBQBurgerActivity.price + BBQBurgerActivity.sauce_price + BBQBurgerActivity.bread_price)*BBQBurgerActivity.quantity));

        if(language.equalsIgnoreCase("En")) {
            holder.title.setText(orderList.get(position).getAdditionalName());
        }
        else{
            holder.title.setText(orderList.get(position).getAdditionalNameAr());
        }

        Glide.with(context).load(Constants.IMAGE_URL+orderList.get(position).getImages()).into(holder.mainImage);

        holder.GridLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(orderList.get(position).getAdditionalsId().equals("51") ||orderList.get(position).getAdditionalsId().equals("57"))) {
                    String pos = Integer.toString(position);
                    if (BBQBurgerActivity.burger_id != null && !BBQBurgerActivity.burger_id.contains(orderList.get(position).getAdditionalsId())) {
                        BBQBurgerActivity.burger_pos.add(pos);
                        BBQBurgerActivity.burger_id.add(orderList.get(position).getAdditionalsId());
                    } else {
                        BBQBurgerActivity.burger_pos.remove(pos);
                        BBQBurgerActivity.burger_id.remove(orderList.get(position).getAdditionalsId());
                    }
                    notifyDataSetChanged();
                }
            }
        });

        return convertView;
    }
}