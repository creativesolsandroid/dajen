package com.cs.dajen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.Order;
import com.cs.dajen.R;
import com.cs.dajen.activity.CheckoutActivity;
import com.cs.dajen.activity.CommentActivity;
import com.cs.dajen.activity.FooterActivity;
import com.cs.dajen.activity.MenuActivity;

import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class CheckoutAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    int qty;
    float price;
    String language;
    boolean imageRefresh = true;
    float vat=5;

    public CheckoutAdapter(Context context, ArrayList<Order> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title, totalPrice, qty1, itemDesc, itemType;
        ImageView itemIcon, plusBtn, minusBtn, itemComment;
        //ImageButton plus;
//        LinearLayout additionalsLayout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.checkout_list, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.checkout_list_ar, null);
            }

            holder.title = (TextView) convertView.findViewById(R.id.checkout_title);
            holder.qty1 = (TextView) convertView.findViewById(R.id.checkout_qty);
            holder.plusBtn = (ImageView) convertView.findViewById(R.id.checkout_plus);
            holder.minusBtn = (ImageView) convertView.findViewById(R.id.checkout_minus);
            holder.totalPrice = (TextView) convertView.findViewById(R.id.checkout_total_price);
//            holder.additionals = (TextView) convertView.findViewById(R.id.additionals);

            holder.itemComment = (ImageView) convertView.findViewById(R.id.checkout_comment);
            holder.itemType = (TextView) convertView.findViewById(R.id.checkout_type);
            holder.itemIcon = (ImageView) convertView.findViewById(R.id.checkout_image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        float priceTxt = Float.parseFloat(orderList.get(position).getItemPrice())* Integer.parseInt(orderList.get(position).getQty());
        holder.totalPrice.setText(""+Constants.decimalFormat.format(priceTxt));
        holder.qty1.setText(orderList.get(position).getQty());

        if((orderList.get(position).getItemId().contains("54")) || (orderList.get(position).getItemId().equals("55")) || (orderList.get(position).getItemId().equals("56")) || (orderList.get(position).getItemId().equals("57"))) {
            if (language.equalsIgnoreCase("En")) {
                holder.title.setText(orderList.get(position).getItemName());
                holder.itemType.setText("" + orderList.get(position).getAdditionalName() + "\n" + Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getItemPrice())));
            } else {
                holder.title.setText(orderList.get(position).getItemNameAr());
                holder.itemType.setText("" + orderList.get(position).getAdditionalNameAr() + "\n" + Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getItemPrice())));
            }
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                holder.title.setText(orderList.get(position).getItemName());
                holder.itemType.setText("" + orderList.get(position).getTypeName() + "\n" + Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getItemPrice())));
            } else {
                holder.title.setText(orderList.get(position).getItemNameAr());
                holder.itemType.setText("" + orderList.get(position).getTypeName() + "\n" + Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getItemPrice())));
            }
        }

//        if(imageRefresh) {
            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(position).getItemImage()).into(holder.itemIcon);
//        }
//        try {
//            String addls = null;
//            if(language.equalsIgnoreCase("En")){
//                addls = orderList.get(position).getAdditionalId();
//            }
//            else{
//                addls = orderList.get(position).getAdditionalNameAr();
//            }
//
//            if(!addls.equals("0")) {
//                String addlsQty = orderList.get(position).getSingleprice();
//                String[] additionals = addls.split(",");
//                String[] additionalsQty = addlsQty.split(",");
//                String finalStr = "";
//                for (int i = 0; i < additionals.length; i++) {
////                    Log.i("TAG","final "+finalStr);
//                    if (finalStr!=null && finalStr.equals("")) {
//                        if(language.equalsIgnoreCase("En")) {
//                            if (!(additionals[i].equalsIgnoreCase("Medium") || additionals[i].equalsIgnoreCase("Large"))) {
//                                finalStr = additionals[i];
//                            }
//                        }
//                        else{
//                            if (!(additionals[i].equalsIgnoreCase("وسط") || additionals[i].equalsIgnoreCase("كبير"))) {
//                                finalStr = additionals[i];
//                            }
//                        }
//                    } else {
//                        finalStr = finalStr + " , " + additionals[i];
//                    }
//                }
//
//               Log.i("TAG","final str "+finalStr);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        holder.plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qty = Integer.parseInt(orderList.get(position).getQty()) + 1;
                price = Float.parseFloat(orderList.get(position).getItemPrice()) + (Float.parseFloat(orderList.get(position).getTotalAmount()));

                myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getOrderId());

                orderList = myDbHelper.getOrderInfo();
                imageRefresh = false;
                notifyDataSetChanged();

                CheckoutActivity.amount.setText(""+Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
                float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                CheckoutActivity.vatAmount.setText(""+Constants.decimalFormat.format(tax));
                CheckoutActivity.netTotal.setText(""+Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()+tax));
                CheckoutActivity.footer_amt.setText("" + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()+tax));
                CheckoutActivity.footer_qty.setText("" + myDbHelper.getTotalOrderQty());
                FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
            }
        });

        holder.minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    qty = Integer.parseInt(orderList.get(position).getQty()) - 1;
                if(qty==0){
                        myDbHelper.deleteItemFromOrder(orderList.get(position).getOrderId());
                }
                else {
                        price = (Float.parseFloat(orderList.get(position).getTotalAmount())) - Float.parseFloat(orderList.get(position).getItemPrice());
                        myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getOrderId());
                }

                orderList = myDbHelper.getOrderInfo();

                CheckoutActivity.amount.setText(""+Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
                float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                CheckoutActivity.vatAmount.setText(""+Constants.decimalFormat.format(tax));
                CheckoutActivity.netTotal.setText(""+Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()+tax));
                CheckoutActivity.footer_amt.setText("" + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()+tax));
                CheckoutActivity.footer_qty.setText("" + myDbHelper.getTotalOrderQty());
                FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());

                if(myDbHelper.getTotalOrderQty()==0){
                    Intent intent = new Intent(context, MenuActivity.class);
                    intent.putExtra("startWith",2);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);
//                    context.finish();
                }
                imageRefresh = false;
                notifyDataSetChanged();
            }
        });

        holder.itemComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderList = myDbHelper.getOrderInfo();
                Intent intent = new Intent(context, CommentActivity.class);
                if(language.equalsIgnoreCase("En")) {
                    intent.putExtra("name",orderList.get(position).getItemName());
                }else if(language.equalsIgnoreCase("Ar")){
                    intent.putExtra("name",orderList.get(position).getItemNameAr());
                }
                intent.putExtra("orderId", orderList.get(position).getOrderId());
                intent.putExtra("comment", orderList.get(position).getComment());
                intent.putExtra("image", orderList.get(position).getItemImage());
                intent.putExtra("screen", "checkout");
                context.startActivity(intent);
            }
        });

        return convertView;
    }
}