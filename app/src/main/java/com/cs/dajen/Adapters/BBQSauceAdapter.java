package com.cs.dajen.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.R;
import com.cs.dajen.activity.BBQProductDetails;

import java.util.ArrayList;


/**
 * Created by cs android on 15-02-2017.
 */

public class BBQSauceAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Additionals> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    String language;

    public BBQSauceAdapter(Context context, ArrayList<Additionals> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title,price,name2, qty;
        ImageView mainImage, right, plus, minus;
        RelativeLayout GridLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.bbq_sauce_list, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.bbq_sauce_list_ar, null);
            }


            holder.title = (TextView) convertView.findViewById(R.id.extra_name1);
            holder.name2 = (TextView) convertView.findViewById(R.id.extra_name2);
            holder.price = (TextView) convertView.findViewById(R.id.extra_price);
            holder.mainImage = (ImageView) convertView.findViewById(R.id.extra_image);
            holder.right = (ImageView) convertView.findViewById(R.id.tick_mark);
            holder.GridLayout = (RelativeLayout) convertView.findViewById(R.id.grid_layout);
            holder.qty = (TextView) convertView.findViewById(R.id.stick_count);
            holder.plus = (ImageView) convertView.findViewById(R.id.stick_plus);
            holder.minus = (ImageView) convertView.findViewById(R.id.stick_minus);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(BBQProductDetails.sauceQty!=null){
            int count = 0;
            for(int i=0; i<BBQProductDetails.sauceQty.size(); i++){
                if(BBQProductDetails.sauceQty.get(i).equals(orderList.get(position).getAdditionalName())){
                    count = count + 1;
                }
            }
            if(count>0) {
                String pos = Integer.toString(position);
                if (!BBQProductDetails.saucePos.contains(pos)) {
                    BBQProductDetails.saucePos.add(pos);
                }
                holder.qty.setText("" + count);
                holder.qty.setAlpha(1);
            }
            else{
                String pos = Integer.toString(position);
                if (BBQProductDetails.saucePos.contains(pos)) {
                    BBQProductDetails.saucePos.remove(pos);
                }
                holder.qty.setText("0");
                holder.qty.setAlpha(0.5f);
            }
        }
        else{
            holder.qty.setText("0");
            holder.qty.setAlpha(0.5f);
        }

        if(language.equalsIgnoreCase("En")) {
            String name = orderList.get(position).getAdditionalName();
            String[] nameList = name.split(" ");
            String name1Str = null, name2Str = null;
            if (nameList.length > 1) {
                name1Str = nameList[0];
                for (int i = 1; i < nameList.length; i++) {
                    if (i == 1) {
                        name2Str = nameList[i];
                    } else {
                        name2Str = name2Str + " " + nameList[i];
                    }
                }
                holder.title.setText(name1Str);
                holder.name2.setText(name2Str);
            } else {
                holder.title.setText(nameList[0]);
                holder.name2.setText("");
            }
        }
        else{
            String name = orderList.get(position).getAdditionalNameAr();
            String[] nameList = name.split(" ");
            String name1Str = null, name2Str = null;
            if (nameList.length > 1) {
                name1Str = nameList[0];
                for (int i = 1; i < nameList.length; i++) {
                    if (i == 1) {
                        name2Str = nameList[i];
                    } else {
                        name2Str = name2Str + " " + nameList[i];
                    }
                }
                holder.title.setText(name1Str);
                holder.name2.setText(name2Str);
            } else {
                holder.title.setText(nameList[0]);
                holder.name2.setText("");
            }
        }

        BBQProductDetails.footerPrice.setText(""+Constants.decimalFormat.format((BBQProductDetails.price+BBQProductDetails.saucePrice)*BBQProductDetails.quantity));

        holder.price.setText(Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice())));

        if(BBQProductDetails.refreshCount_sauce==0) {
            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(position).getImages()).into(holder.mainImage);
        }

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(BBQProductDetails.sauceCount<9) {
                    BBQProductDetails.sauceCount = BBQProductDetails.sauceCount +1;
                    BBQProductDetails.sauceQty.add(orderList.get(position).getAdditionalName());
                    BBQProductDetails.saucePrice = BBQProductDetails.saucePrice + Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice());
                    BBQProductDetails.refreshCount = 1;
                    notifyDataSetChanged();
                }
                else{
                    Toast.makeText(context,"You can only add 9 sauces", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Integer.parseInt(holder.qty.getText().toString())>0) {
                    BBQProductDetails.sauceCount = BBQProductDetails.sauceCount -1;
                    BBQProductDetails.sauceQty.remove(orderList.get(position).getAdditionalName());
                    BBQProductDetails.saucePrice = BBQProductDetails.saucePrice - Float.parseFloat(orderList.get(position).getAdditionalPriceList().get(0).getPrice());
                    BBQProductDetails.refreshCount = 1;
                    notifyDataSetChanged();
                }
            }
        });

        return convertView;
    }
}