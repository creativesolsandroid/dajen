package com.cs.dajen.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.R;
import com.cs.dajen.activity.InsideProductActivity;

import java.util.ArrayList;


/**
 * Created by cs android on 15-02-2017.
 */

public class PastaAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Additionals> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    String language, activity;

    public PastaAdapter(Context context, ArrayList<Additionals> orderList, String language, String activity) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.activity = activity;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title;
        ImageView mainImage, right;
        RelativeLayout GridLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.salad_list, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.salad_list, null);
            }


            holder.title = (TextView) convertView.findViewById(R.id.salad_name);
            holder.mainImage = (ImageView) convertView.findViewById(R.id.salad_image);
            holder.right = (ImageView) convertView.findViewById(R.id.salad_tick);
            holder.GridLayout = (RelativeLayout) convertView.findViewById(R.id.salad_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Glide.with(context).load(Constants.IMAGE_URL+orderList.get(position).getImages()).into(holder.mainImage);
        if(language.equalsIgnoreCase("En")) {
            holder.title.setText(orderList.get(position).getAdditionalName());
        }
        else{
            holder.title.setText(orderList.get(position).getAdditionalNameAr());
        }

        if(!InsideProductActivity.isPastaLoaded) {
            InsideProductActivity.pasta_pos.add("0");
            InsideProductActivity.isPastaLoaded = true;
        }
        if (InsideProductActivity.pasta_pos.size() > 0 && InsideProductActivity.pasta_pos.contains(Integer.toString(position))) {
            holder.right.setVisibility(View.VISIBLE);
        } else {
            holder.right.setVisibility(View.INVISIBLE);
        }

        InsideProductActivity.footer_price.setText(Constants.decimalFormat.format(((InsideProductActivity.price + InsideProductActivity.cooldrinkprice + InsideProductActivity.sauce_price + InsideProductActivity.bread_price) * InsideProductActivity.quantity)));

        holder.GridLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String pos = Integer.toString(position);
                if (InsideProductActivity.pasta_pos != null && !InsideProductActivity.pasta_pos.contains(pos)) {
                    InsideProductActivity.pasta_pos.add(pos);

                } else {
                    InsideProductActivity.pasta_pos.remove(pos);
                }

                notifyDataSetChanged();
            }
        });
        return convertView;
    }
}