package com.cs.dajen.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cs.dajen.Constants;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.R;
import com.cs.dajen.activity.SaladActivity;

import java.util.ArrayList;


/**
 * Created by cs android on 15-02-2017.
 */

public class CustomSaladAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Additionals> orderList = new ArrayList<>();
    String language;
    int remaining_count;
    Activity parentActivity;
    AlertDialog customDialog;

    public CustomSaladAdapter(Context context, ArrayList<Additionals> orderList, String language, Activity parentActivity) {
        this.context = context;
        this.orderList = orderList;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title;
        ImageView mainImage, right;
        RelativeLayout itemLayout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.salad_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.title = (TextView) convertView.findViewById(R.id.salad_name);
            holder.mainImage = (ImageView) convertView.findViewById(R.id.salad_image);
            holder.right = (ImageView) convertView.findViewById(R.id.salad_tick);
            holder.itemLayout = (RelativeLayout) convertView.findViewById(R.id.salad_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(language.equalsIgnoreCase("En")) {
            holder.title.setText(orderList.get(position).getAdditionalName());
        }
        else{
            holder.title.setText(orderList.get(position).getAdditionalNameAr());
        }

         Glide.with(context).load(Constants.IMAGE_URL + orderList.get(position).getImages()).placeholder(R.drawable.icon_xhdpi).into(holder.mainImage);

//        if(SaladActivity.addl_ids!=null && SaladActivity.addl_ids.size()>0){
            if(SaladActivity.addl_ids.contains(orderList.get(position).getAdditionalsId())){
                holder.right.setVisibility(View.VISIBLE);
            }
            else{
                holder.right.setVisibility(View.INVISIBLE);
            }

            if(SaladActivity.addlRefresh) {
                SaladActivity.additionals.removeAllViews();
                if (SaladActivity.addlImages != null && SaladActivity.addlImages.size() > 0) {
                    SaladActivity.additionals.setVisibility(View.VISIBLE);
                    SaladActivity.empty_bowl.setVisibility(View.INVISIBLE);
                    for (int i = 0; i < SaladActivity.addlImages.size(); i++) {
                        View view = inflater.inflate(R.layout.custom_salad_image, null);
                        ImageView imageView = (ImageView) view.findViewById(R.id.imageLayout);
                        Glide.with(context).load(Constants.IMAGE_URL + SaladActivity.addlImages.get(i)).diskCacheStrategy( DiskCacheStrategy.ALL ).into(imageView);
                        SaladActivity.additionals.addView(view);
//                        if(SaladActivity.addlImages.size()==i){
                            SaladActivity.addlRefresh = false;
//                        }
                    }
                }
                else{
                    if(SaladActivity.isCustom) {
                        SaladActivity.empty_bowl.setImageDrawable(context.getResources().getDrawable(R.drawable.empty_bowl));
                        SaladActivity.additionals.setVisibility(View.INVISIBLE);
                        SaladActivity.empty_bowl.setVisibility(View.VISIBLE);
                    }
                }
            }
//        }

        remaining_count = SaladActivity.additionals_count - SaladActivity.addl_ids.size();
        if(remaining_count<10){
            SaladActivity.grid_count.setText((SaladActivity.total_additionals - remaining_count)+"/"+SaladActivity.total_additionals);
        }
        else{
            SaladActivity.grid_count.setText((SaladActivity.total_additionals - remaining_count)+"/"+SaladActivity.total_additionals);
        }

        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View view1 = inflater.inflate(R.layout.custom_salad_image, null);
                ImageView imageView = (ImageView) view1.findViewById(R.id.imageLayout);

                if(SaladActivity.addl_ids.contains(orderList.get(position).getAdditionalsId())){
                    SaladActivity.addl_ids.remove(orderList.get(position).getAdditionalsId());
                    SaladActivity.addl_pos.remove(Integer.toString(position));
                    SaladActivity.addlImages.remove(orderList.get(position).getImages());
                    Log.i("TAG","refresh2 "+SaladActivity.addlRefresh);
                    SaladActivity.addlRefresh = true;
//                SaladActivity.refreshCount=1;

                }
                else {
                    SaladActivity.addlRefresh = true;
                    if(remaining_count!=0) {
                        SaladActivity.addl_ids.add((orderList.get(position).getAdditionalsId()));
                        SaladActivity.addl_pos.add(Integer.toString(position));
                        SaladActivity.addlImages.add(orderList.get(position).getImages());

//                        Glide.with(context).load(Constants.IMAGE_URL + orderList.get(position).getImages()).into(imageView);
                    }
                    else{
                        String msg;
//                        if(SaladActivity.total_additionals==5) {
//                            if(language.equalsIgnoreCase("En")) {
//                                msg = context.getResources().getString(R.string.max_5_salads);
//                            }
//                            else{
//                                msg = context.getResources().getString(R.string.max_5_salads_ar);
//                            }
//                        }
//                        else{
                            if(language.equalsIgnoreCase("En")) {
                                msg = context.getResources().getString(R.string.max_10_salads);
                            }
                            else{
                                msg = context.getResources().getString(R.string.max_10_salads_ar);
                            }
//                        }

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(parentActivity, android.R.style.Theme_Material_Light_Dialog));

                            alertDialogBuilder.setTitle(R.string.app_name);

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage(msg)
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
                }
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}