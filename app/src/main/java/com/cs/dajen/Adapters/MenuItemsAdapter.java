package com.cs.dajen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.MenuItems;
import com.cs.dajen.R;
import com.cs.dajen.activity.BBQBurgerActivity;
import com.cs.dajen.activity.DajenBBQActivity;
import com.cs.dajen.activity.InsideProductActivity;
import com.cs.dajen.activity.SaladActivity;

import java.util.ArrayList;

/**
 * Created by CS on 27-04-2017.
 */

public class MenuItemsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<MenuItems> orderList = new ArrayList<>();
    String language;
    int count = 0;
    long sec1;
    private DataBaseHelper myDbHelper;

    public MenuItemsAdapter(Context context, ArrayList<MenuItems> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

    }

    public int getCount() {
        return orderList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView grid_title, grid_desc, grid_price, grid_count;
        ImageView grid_image;
        RelativeLayout item_layout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.product_itemslist, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.product_itemslist_ar, null);
            }

            myDbHelper = new DataBaseHelper(context);

            holder.grid_title = (TextView) convertView.findViewById(R.id.grid_title);
            holder.grid_desc = (TextView) convertView.findViewById(R.id.grid_desc);
            holder.grid_price = (TextView) convertView.findViewById(R.id.grid_price);
            holder.grid_count = (TextView) convertView.findViewById(R.id.grid_count);
            holder.grid_image = (ImageView) convertView.findViewById(R.id.grid_image);
            holder.item_layout = (RelativeLayout) convertView.findViewById(R.id.item_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Glide.with(context).load(Constants.IMAGE_URL + orderList.get(position).getImage()).into(holder.grid_image);
        if(language.equalsIgnoreCase("En")) {
            holder.grid_title.setText("" + orderList.get(position).getItemName());
            holder.grid_desc.setText("" + orderList.get(position).getDesc());

        }
        else{
            holder.grid_title.setText("" + orderList.get(position).getItemName_ar());
            holder.grid_desc.setText("" + orderList.get(position).getDesc_ar());
        }

        if(myDbHelper.getItemOrderCount(orderList.get(position).getItemId())>0){
            holder.grid_count.setText(""+myDbHelper.getItemOrderCount(orderList.get(position).getItemId()));
        }
        else{
            holder.grid_count.setText("");
        }

        holder.item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(orderList.get(position).getCatId().equals("5")) {
                    Intent produtIntent = new Intent(context, SaladActivity.class);
                    produtIntent.putExtra("position", position);
                    produtIntent.putExtra("arraylist", orderList);
                    context.startActivity(produtIntent);
                }
                else if(orderList.get(position).getCatId().equals("12")) {
                    Intent produtIntent = new Intent(context, DajenBBQActivity.class);
                    produtIntent.putExtra("position", position);
                    produtIntent.putExtra("arraylist", orderList);
                    context.startActivity(produtIntent);
                }
                else if(orderList.get(position).getCatId().equals("13")) {
                    Intent produtIntent = new Intent(context, BBQBurgerActivity.class);
                    produtIntent.putExtra("position", position);
                    produtIntent.putExtra("arraylist", orderList);
                    context.startActivity(produtIntent);
                }
                else{
                    Intent produtIntent = new Intent(context, InsideProductActivity.class);
                    produtIntent.putExtra("position", position);
                    produtIntent.putExtra("arraylist", orderList);
                    context.startActivity(produtIntent);
                }
            }
        });
        if(!orderList.get(position).getPrice1().equals("-1")){
            holder.grid_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getPrice1())));
        }

        if(!orderList.get(position).getPrice2().equals("-1")){
            holder.grid_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getPrice2())));
        }

        if(!orderList.get(position).getPrice3().equals("-1")){
            holder.grid_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getPrice3())));
        }

        if(!orderList.get(position).getPrice4().equals("-1")){
            holder.grid_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getPrice4())));
        }

        if(!orderList.get(position).getPrice5().equals("-1")){
            holder.grid_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getPrice5())));
        }

        if(!orderList.get(position).getPrice6().equals("-1")){
            holder.grid_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getPrice6())));
        }

        if(!orderList.get(position).getPrice7().equals("-1")){
            holder.grid_price.setText(""+Constants.decimalFormat.format(Float.parseFloat(orderList.get(position).getPrice7())));
        }

        if(orderList.get(position).getTwoCount() || holder.grid_price.getText().toString().equals("0.00")){
            holder.grid_price.setVisibility(View.INVISIBLE);
        }
        else{
            holder.grid_price.setVisibility(View.VISIBLE);
        }

        return convertView;
    }
}
