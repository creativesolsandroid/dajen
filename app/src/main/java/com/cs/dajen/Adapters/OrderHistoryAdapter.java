package com.cs.dajen.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.dajen.Constants;
import com.cs.dajen.Models.OrderHistory;
import com.cs.dajen.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by SKT on 20-02-2016.
 */
public class OrderHistoryAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<OrderHistory> favOrderList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    Map<Integer, View> views = new HashMap<Integer, View>();
    //public ImageLoader imageLoader;

    public OrderHistoryAdapter(Context context, ArrayList<OrderHistory> favOrderList, String language) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        // menu type count
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if(favOrderList.get(position).getOrderStatus().equals("Rejected") ||favOrderList.get(position).getOrderStatus().equals("Cancel") ||favOrderList.get(position).getOrderStatus().equals("Close")){
            type = 0;
        }
        else{
            type = 1;
        }
        // current menu type
        return type;
    }

    public static class ViewHolder {
        TextView orderNumber,orderDate,totalPrice, itemDetails, storeName,deliver_to,deliver_from,deliveryAddress;
        ImageView progress;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {

            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.order_history_list, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.order_history_list_ar, null);
            }


            holder.orderNumber = (TextView) convertView
                    .findViewById(R.id.order_number);
            holder.orderDate = (TextView) convertView
                    .findViewById(R.id.order_date);
            holder.totalPrice = (TextView) convertView
                    .findViewById(R.id.history_amount);
            holder.deliveryAddress = (TextView) convertView
                    .findViewById(R.id.deliver_address);
            holder.storeName = (TextView) convertView
                    .findViewById(R.id.store_name);
            holder.deliver_to = (TextView) convertView
                    .findViewById(R.id.deliver_to);
            holder.deliver_from = (TextView) convertView
                    .findViewById(R.id.deliver_from);

            holder.itemDetails = (TextView) convertView.findViewById(R.id.history_items);
            holder.progress = (ImageView) convertView.findViewById(R.id.order_step);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.orderNumber.setText(favOrderList.get(position).getInvoiceNo());
        holder.totalPrice.setText(Constants.decimalFormat.format(Float.parseFloat(favOrderList.get(position).getTotalPrice())));
        if(language.equalsIgnoreCase("En")){
            holder.storeName.setText(favOrderList.get(position).getStoreName());
            holder.itemDetails.setText(favOrderList.get(position).getItemDetails());
        }else if(language.equalsIgnoreCase("Ar")){
            holder.storeName.setText(favOrderList.get(position).getStoreName_ar());
            holder.itemDetails.setText(favOrderList.get(position).getItemDetails_ar());
        }

        if(favOrderList.get(position).getOrderType().equals("Delivery")){
            holder.deliver_to.setVisibility(View.VISIBLE);
            holder.deliveryAddress.setVisibility(View.VISIBLE);
            holder.deliveryAddress.setText(favOrderList.get(position).getUserAddress());
            if(language.equalsIgnoreCase("En")) {
                holder.deliver_from.setText(context.getResources().getString(R.string.deliver_from));
            }
            else{
                holder.deliver_from.setText(context.getResources().getString(R.string.deliver_from_ar));
            }
        }
        else if(favOrderList.get(position).getOrderType().equals("PickUp")){
            holder.deliver_to.setVisibility(View.GONE);
            holder.deliveryAddress.setVisibility(View.GONE);
            if(language.equalsIgnoreCase("En")) {
                holder.deliver_from.setText(context.getResources().getString(R.string.pickup_from));
            }
            else{
                holder.deliver_from.setText(context.getResources().getString(R.string.pickup_from_ar));
            }
        }
        else if(favOrderList.get(position).getOrderType().equals("DineIn")){
            holder.deliver_to.setVisibility(View.GONE);
            holder.deliveryAddress.setVisibility(View.GONE);
            if(language.equalsIgnoreCase("En")) {
                holder.deliver_from.setText(context.getResources().getString(R.string.dinein_from));
            }
            else{
                holder.deliver_from.setText(context.getResources().getString(R.string.dinein_from_ar));
            }
        }

        SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getOrderDate());
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                dateObj = curFormater1.parse(favOrderList.get(position).getOrderDate());
            }catch (ParseException pe){

            }

        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.orderDate.setText(date);

        if(language.equalsIgnoreCase("En")) {
            if (!favOrderList.get(position).getOrderType().equalsIgnoreCase("Delivery")) {
                if (favOrderList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.pickup_step4);
                } else if (favOrderList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.pickup_step2);
                } else if (favOrderList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.pickup_step3);
                } else if (favOrderList.get(position).getOrderStatus().equals("Rejected") || favOrderList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.pickup_step_cancel);
                } else {
                    holder.progress.setImageResource(R.drawable.pickup_step1);
                }
            } else {
                if (favOrderList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.delivery_step5);
                } else if (favOrderList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.delivery_step2);
                } else if (favOrderList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.delivery_step3);
                } else if (favOrderList.get(position).getOrderStatus().equals("OnTheWay")) {
                    holder.progress.setImageResource(R.drawable.delivery_step4);
                } else if (favOrderList.get(position).getOrderStatus().equals("Rejected") || favOrderList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.delivery_step_cancel);
                } else {
                    holder.progress.setImageResource(R.drawable.delivery_step1);
                }
            }
        }else if(language.equalsIgnoreCase("Ar")){
            if (!favOrderList.get(position).getOrderType().equalsIgnoreCase("Delivery")) {
                if (favOrderList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.pickup_step4);
                } else if (favOrderList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.pickup_step2);
                } else if (favOrderList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.pickup_step3);
                } else if (favOrderList.get(position).getOrderStatus().equals("Rejected") || favOrderList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.pickup_step_cancel);
                } else {
                    holder.progress.setImageResource(R.drawable.pickup_step1);
                }
            } else {
                if (favOrderList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.delivery_step5);
                } else if (favOrderList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.delivery_step2);
                } else if (favOrderList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.delivery_step3);
                } else if (favOrderList.get(position).getOrderStatus().equals("OnTheWay")) {
                    holder.progress.setImageResource(R.drawable.delivery_step4);
                } else if (favOrderList.get(position).getOrderStatus().equals("Rejected") || favOrderList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.delivery_step_cancel);
                } else {
                    holder.progress.setImageResource(R.drawable.delivery_step1);
                }
            }
        }
//
//        holder.totalBalance.setText(String.format("%.2f", Float.parseFloat(storesList.get(position).getTotalBalance())));

        views.put(position, convertView);
        return convertView;
    }
}