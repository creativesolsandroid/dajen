package com.cs.dajen.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.PagerAdapter;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.R;
import com.cs.dajen.activity.InsideProductActivity;
import com.cs.dajen.activity.SaladActivity;

import static com.cs.dajen.Constants.ChickenMenu;

/**
 * Created by cs android on 23-02-2017.
 */

public class BannersAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ImageView Banner;
    TextView name;
    LinearLayout layout;
    Context context;
    Activity parentActivity;

    Integer[] bannerList;
    String[] itemNames;
    String language;
    private DataBaseHelper myDbHelper;
    SharedPreferences userPrefs;
    int pos = 0, catId = 1;

    public BannersAdapter(Context context, Integer[] bannerList, String[] itemNames, String language, Activity parentActivity) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.offerBanner = false;
        this.context = context;
        this.language = language;
        this.bannerList = bannerList;
        this.itemNames = itemNames;
        this.parentActivity = parentActivity;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = null;
        if(language.equalsIgnoreCase("En")) {
            itemView = mLayoutInflater.inflate(R.layout.banners_list, container, false);
        }
        else{
            itemView = mLayoutInflater.inflate(R.layout.banners_list_ar, container, false);
        }

        Banner=(ImageView)itemView.findViewById(R.id.banner);
        name=(TextView) itemView.findViewById(R.id.name);
        layout=(LinearLayout) itemView.findViewById(R.id.layout);

        myDbHelper = new DataBaseHelper(context.getApplicationContext());
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        Banner.setImageResource(bannerList[position]);
        name.setText(itemNames[position]);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(position == 0){
                    pos = 3;
                    Constants.Itemid = 1;
                }
                else if(position == 1){
                    pos = 7;
                    Constants.Itemid = 1;
                }
                else if(position == 2){
                    pos = 25;
                    Constants.Itemid = 2;
                }
                else if(position == 3){
                    pos = 78;
                    Constants.Itemid = 5;
                }

                if(myDbHelper.getTotalOrderQty() > 0) {
                    if(userPrefs.getString("menu","").equals("bbq")){

                        String msg,name,yes,no;
                        if(language.equalsIgnoreCase("En")) {
                            msg = "You have " + myDbHelper.getTotalOrderQty() + " BBQ item(s) in your bag, by this action all items get clear. Do you want to continue with Main Menu?";
                            name = context.getResources().getString(R.string.app_name);
                            yes = context.getResources().getString(R.string.yes);
                            no = context.getResources().getString(R.string.no);
                        }
                        else{
                            msg = "لديك" + myDbHelper.getTotalOrderQty() + "طلبات من القائمة الرئيسية  في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع قائمة ال BBQ ؟";
                            name = context.getResources().getString(R.string.dajen_ar);
                            yes = context.getResources().getString(R.string.yes_ar);
                            no = context.getResources().getString(R.string.no_ar);
                        }
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(parentActivity, android.R.style.Theme_Material_Light_Dialog));

                        alertDialogBuilder.setTitle(name);

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(msg)
                                .setCancelable(false)
                                .setNegativeButton(no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                        myDbHelper.deleteOrderTable();
                                        Constants.MENU_TYPE = "main";
                                        if(Constants.Itemid != 5) {
                                            Intent produtIntent = new Intent(context, InsideProductActivity.class);
                                            produtIntent.putExtra("position", pos);
                                            produtIntent.putExtra("arraylist", ChickenMenu);
                                            produtIntent.putExtra("screen", "chicken");
                                            ((Activity)context).startActivity(produtIntent);
                                            ((Activity)context).finish();
                                        }
                                        else{
                                            Intent produtIntent = new Intent(context, SaladActivity.class);
                                            produtIntent.putExtra("position", pos);
                                            produtIntent.putExtra("arraylist", ChickenMenu);
                                            produtIntent.putExtra("screen", "chicken");
                                            ((Activity)context).startActivity(produtIntent);
                                            ((Activity)context).finish();
                                        }
                                    }
                                });
                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
                    else {
                        Constants.MENU_TYPE = "main";
                        if(Constants.Itemid != 5) {
                            Intent produtIntent = new Intent(context, InsideProductActivity.class);
                            produtIntent.putExtra("position", pos);
                            produtIntent.putExtra("arraylist", ChickenMenu);
                            produtIntent.putExtra("screen", "chicken");
                            ((Activity)context).startActivity(produtIntent);
                            ((Activity)context).finish();
                        }
                        else{
                            Intent produtIntent = new Intent(context, SaladActivity.class);
                            produtIntent.putExtra("position", pos);
                            produtIntent.putExtra("arraylist", ChickenMenu);
                            produtIntent.putExtra("screen", "chicken");
                            ((Activity)context).startActivity(produtIntent);
                            ((Activity)context).finish();
                        }
                    }
                }
                else {
                    Constants.MENU_TYPE = "main";
                    if(Constants.Itemid != 5) {
                        Intent produtIntent = new Intent(context, InsideProductActivity.class);
                        produtIntent.putExtra("position", pos);
                        produtIntent.putExtra("arraylist", ChickenMenu);
                        produtIntent.putExtra("screen", "chicken");
                        ((Activity)context).startActivity(produtIntent);
                        ((Activity)context).finish();
                    }
                    else{
                        Intent produtIntent = new Intent(context, SaladActivity.class);
                        produtIntent.putExtra("position", pos);
                        produtIntent.putExtra("arraylist", ChickenMenu);
                        produtIntent.putExtra("screen", "chicken");
                        ((Activity)context).startActivity(produtIntent);
                        ((Activity)context).finish();
                    }
                }
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
