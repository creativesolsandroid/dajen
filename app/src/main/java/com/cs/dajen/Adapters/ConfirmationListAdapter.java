package com.cs.dajen.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.Order;
import com.cs.dajen.R;

import java.util.ArrayList;

/**
 * Created by CS on 04-07-2016.
 */
public class ConfirmationListAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    int qty;
    float price;
    String language, size;
    int pos = 0;

    public ConfirmationListAdapter(Context context, ArrayList<Order> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView itemName, itemPrice;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.confirmation_list, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.confirmation_list_ar, null);
            }

            holder.itemName = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemPrice = (TextView) convertView.findViewById(R.id.item_quantity);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(language.equalsIgnoreCase("En")) {
            holder.itemName.setText(orderList.get(position).getItemName());
        }
        else{
            holder.itemName.setText(orderList.get(position).getItemNameAr());
        }
        if(Integer.parseInt(orderList.get(position).getQty())<10) {
            holder.itemPrice.setText("0"+orderList.get(position).getQty());
        }
        else{
            holder.itemPrice.setText(orderList.get(position).getQty());
        }
        return convertView;
    }
}

